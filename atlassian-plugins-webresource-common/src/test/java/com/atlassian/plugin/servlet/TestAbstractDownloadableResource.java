package com.atlassian.plugin.servlet;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.util.CapturingHttpServletResponse;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.collect.ImmutableMap;
import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Map;

/**
 * A test for AbstractDownloadableResource
 */
public class TestAbstractDownloadableResource extends TestCase {
    private static final String MINIFIED_CONTENT = "minified content";
    private static final String PLAIN_CONTENT = "plain content";
    private static final String NEVER_MINIFIED_CONTENT = "never minified content";
    private static final Map<String, String> EMPTY_PARAMS = ImmutableMap.of();

    private class MinifiedFileServingDownloadableResource extends AbstractDownloadableResource {
        public MinifiedFileServingDownloadableResource(final ResourceLocation resourceLocation) {
            super(null, resourceLocation, null);
        }

        @Override
        protected String getLocation() {
            return "somecode.js";
        }

        @Override
        public String getContentType() {
            return "minified/content";
        }

        @Override
        protected InputStream getResourceAsStream(final String resourceLocation) {
            if (resourceLocation.contains("-min.")) {
                assertEquals("somecode-min.js", resourceLocation);
                return newStream(MINIFIED_CONTENT);
            }
            assertEquals("somecode.js", resourceLocation);
            return newStream(PLAIN_CONTENT);
        }

        private InputStream newStream(final String s) {
            return new ByteArrayInputStream(s.getBytes());
        }
    }

    private class MyDownloadableResource extends AbstractDownloadableResource {
        private String passedResourceLocation;

        public MyDownloadableResource(final ResourceLocation resourceLocation, final boolean disableMinification) {
            super(null, resourceLocation, "", disableMinification);
        }

        @Override
        protected InputStream getResourceAsStream(final String resourceLocation) {
            passedResourceLocation = resourceLocation;
            return newStream(resourceLocation);
        }

        private InputStream newStream(final String s) {
            return new ByteArrayInputStream(s.getBytes());
        }
    }

    private class NotMinifiedFileServingDownloadableResouce extends AbstractDownloadableResource {
        public NotMinifiedFileServingDownloadableResouce() {
            super(null, new ResourceLocation("/flintstone/fred.jpg", "fred.jpg", "stuff", "stuff", "stuff", EMPTY_PARAMS), null);
        }

        @Override
        protected String getLocation() {
            return "somemorecode.js";
        }

        @Override
        public String getContentType() {
            return "plain/content";
        }

        @Override
        protected InputStream getResourceAsStream(final String resourceLocation) {
            if (resourceLocation.contains("-min.")) {
                assertEquals("somemorecode-min.js", resourceLocation);
                return null;
            }
            assertEquals("somemorecode.js", resourceLocation);
            return newStream(PLAIN_CONTENT);
        }

        private InputStream newStream(final String s) {
            return new ByteArrayInputStream(s.getBytes());
        }

    }

    private class NeverMinifiedFileServingDownloadableResource extends AbstractDownloadableResource {
        public NeverMinifiedFileServingDownloadableResource() {
            super(null, new ResourceLocation("/flintstone/fred.jpg", "fred.jpg", "stuff", "stuff", "stuff", EMPTY_PARAMS), null);
        }

        @Override
        protected String getLocation() {
            return "neverminified.js";
        }

        @Override
        public String getContentType() {
            return "neverminified/content";
        }

        @Override
        protected InputStream getResourceAsStream(final String resourceLocation) {
            if (resourceLocation.contains("-min.")) {
                fail("it should never ask for this");
            }
            assertEquals("neverminified.js", resourceLocation);
            return newStream(NEVER_MINIFIED_CONTENT);
        }

        private InputStream newStream(final String s) {
            return new ByteArrayInputStream(s.getBytes());
        }
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        System.setProperty("atlassian.webresource.disable.minification", "false");
        System.setProperty("atlassian.dev.mode", "false");
    }

    public void testMinificationStrategyWrongFileType() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.jpg", "fred.jpg", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred.jpg", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyCss() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.css", "fred.css", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred-min.css", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyWithTwoMinsInName() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone-min./fred-min.js", "fred-min.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone-min./fred-min.js", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyAlreadyMinimised() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred-min.js", "fred-min.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred-min.js", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyNotMinimisedAndEnabled() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.js", "fred.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred-min.js", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyNotMinimisedAndDisabled() throws Exception {
        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.js", "fred.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, true);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred.js", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyNotMinimisedAndSystemDisabled() throws Exception {
        System.setProperty("atlassian.webresource.disable.minification", "true");

        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.js", "fred.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred.js", myDownloadableResource.passedResourceLocation);
    }

    public void testMinificationStrategyNotMinimisedAndSystemEnabled() throws Exception {
        System.setProperty("atlassian.webresource.disable.minification", "false");

        final ResourceLocation resourceLocation = new ResourceLocation("/flintstone/fred.js", "fred.js", "stuff", "stuff", "stuff", EMPTY_PARAMS);
        final MyDownloadableResource myDownloadableResource = new MyDownloadableResource(resourceLocation, false);
        myDownloadableResource.streamResource(new ByteArrayOutputStream());

        assertEquals("/flintstone/fred-min.js", myDownloadableResource.passedResourceLocation);
    }

    public void testWithMinifiedStrategyInPlay() throws DownloadException {
        // it should ask for -min files first and in this case get content back
        final MinifiedFileServingDownloadableResource minifiedFileServingDownloadableResource = new MinifiedFileServingDownloadableResource(new ResourceLocation("/flintstone/fred.jpg", "fred.jpg", "stuff", "stuff", "stuff", EMPTY_PARAMS));
        assertContent(minifiedFileServingDownloadableResource, MINIFIED_CONTENT);

        // it should ask for -min files first but get null and hence move on to the plain old content case.
        final NotMinifiedFileServingDownloadableResouce notMinifiedFileServingDownloadableResource = new NotMinifiedFileServingDownloadableResouce();
        assertContent(notMinifiedFileServingDownloadableResource, PLAIN_CONTENT);

    }

    public void testWhenSystemPropertyIsSet() throws DownloadException {
        verifySystemPropertyRespected("atlassian.webresource.disable.minification");
    }

    public void testWhenDevModeSystemPropertyIsSet() throws DownloadException {
        verifySystemPropertyRespected(PluginUtils.ATLASSIAN_DEV_MODE);
    }

    private void verifySystemPropertyRespected(String sysprop)
            throws DownloadException {
        try {
            System.setProperty(sysprop, "true");

            // now in this case it must never ask for minified files.  This class used will assert that.
            final NeverMinifiedFileServingDownloadableResource neverMinifiedFileServingDownloadableResource = new NeverMinifiedFileServingDownloadableResource();
            assertContent(neverMinifiedFileServingDownloadableResource, NEVER_MINIFIED_CONTENT);

            final MinifiedFileServingDownloadableResource minifiedFileServingDownloadableResource = new MinifiedFileServingDownloadableResource(new ResourceLocation("/flintstone/fred.jpg", "fred.jpg", "stuff", "stuff", "stuff", EMPTY_PARAMS));
            assertContent(minifiedFileServingDownloadableResource, PLAIN_CONTENT);

            // it should ask for -min files first but get null and hence move on to the plain old content case.
            final NotMinifiedFileServingDownloadableResouce notMinifiedFileServingDownloadableResource = new NotMinifiedFileServingDownloadableResouce();
            assertContent(notMinifiedFileServingDownloadableResource, PLAIN_CONTENT);

            System.setProperty(sysprop, "false");

            // it should ask for -min files first and in this case get content back
            assertContent(minifiedFileServingDownloadableResource, MINIFIED_CONTENT);

            // it should ask for -min files first but get null and hence move on to the plain old content case.
            assertContent(notMinifiedFileServingDownloadableResource, PLAIN_CONTENT);

            //
            // now this test is wierd but hey.  If I call back on a never minified resource class object it should
            // throw an assertion exception that it doesnt expect it.  This proves that it odes indeed get called
            // with a -min version of itself
            try {
                assertContent(neverMinifiedFileServingDownloadableResource, "doesnt matter");

                fail("This should have barfed in NeverMinifiedFileServingDownloadableResource");
            } catch (final AssertionFailedError expected) {
                // this is expected since the test class asserts tgat a -min file should never be called on it.
                // and hence by inference the atlassian.webresource.disable.minification property is not taking effect
            }
        } finally {
            // reset for this test
            System.setProperty(sysprop, "false");
        }
    }

    private void assertContent(final AbstractDownloadableResource downloadableResource, final String content) throws DownloadException {
        final CapturingHttpServletResponse httpServletResponse = new CapturingHttpServletResponse();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            downloadableResource.serveResource(null, httpServletResponse);
        } catch (final DownloadException e) {
            throw new RuntimeException(e);
        }
        downloadableResource.streamResource(baos);

        assertEquals(content, httpServletResponse.toString());
        assertEquals(content, baos.toString());
    }
}
