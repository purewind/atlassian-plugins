package com.atlassian.plugin.elements;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ResourceLocationTest {
    @Test
    public void paramsAreCopied() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("a", "original");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        params.put("a", "modified");

        assertEquals("original", rl.getParams().get("a"));
    }

    @Test
    public void paramsAreImmutable() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("a", "original");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);

        try {
            rl.getParams().put("a", "modified");
        } catch (UnsupportedOperationException e) {
            // Okay
        }

        assertEquals("original", rl.getParams().get("a"));
    }

    @Test(expected = NullPointerException.class)
    public void constructionWithNullParamsFails() {
        new ResourceLocation("", "", "", "", "", null);
    }

    @Test
    public void constructionWithNullValuesSucceeds() {
        Map<String, String> params = Collections.<String, String>singletonMap("key", null);

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertNull(rl.getParameter("key"));
    }

    @Test
    public void constructionWithNullValuesOmitsParam() {
        Map<String, String> params = Collections.<String, String>singletonMap("key", null);

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(Collections.emptyMap(), rl.getParams());
    }

    @Test
    public void constructionWithMixedValuesIncludesNonNullParam() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("null", null);
        params.put("notnull", "value");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(ImmutableMap.of("notnull", "value"), rl.getParams());
    }

    @Test
    public void constructionWithNullKeyOmitsParam() {
        Map<String, String> params = Collections.<String, String>singletonMap(null, "value");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(Collections.emptyMap(), rl.getParams());
    }
}
