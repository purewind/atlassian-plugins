package com.atlassian.plugin.instrumentation;

import com.atlassian.instrumentation.InstrumentRegistry;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.plugin.test.Matchers.notPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestPluginSystemInstrumentationDisabled {

    @Test
    public void instrumentationNotCreated() {
        final Optional<InstrumentRegistry> instrumentRegistry = PluginSystemInstrumentation.instance().getInstrumentRegistry();
        assertThat(instrumentRegistry, notPresent());
    }

    @Test
    public void pullTimerReturnsEmpty() {
        final Timer timer = PluginSystemInstrumentation.instance().pullTimer("sometimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), notPresent());
    }

    @Test
    public void pullSingleTimerReturnsEmpty() {
        final Timer timer = PluginSystemInstrumentation.instance().pullSingleTimer("somesingletimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), notPresent());
    }
}
