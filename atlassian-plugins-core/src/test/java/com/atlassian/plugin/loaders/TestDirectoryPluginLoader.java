package com.atlassian.plugin.loaders;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockBear;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.PADDINGTON_JAR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestDirectoryPluginLoader {
    private static final List<PluginFactory> DEFAULT_PLUGIN_FACTORIES = ImmutableList.<PluginFactory>of(
            new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME));

    private PluginEventManager pluginEventManager;
    private DirectoryPluginLoader loader;
    private DefaultModuleDescriptorFactory moduleDescriptorFactory;
    private File pluginsDirectory;
    private File pluginsTestDir;

    public void createFillAndCleanTempPluginDirectory() throws IOException {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories = DirectoryPluginLoaderUtils.createFillAndCleanTempPluginDirectory();
        pluginsDirectory = directories.pluginsDirectory;
        pluginsTestDir = directories.pluginsTestDir;
    }

    @Before
    public void setUp() throws Exception {
        moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        pluginEventManager = new DefaultPluginEventManager();
        createFillAndCleanTempPluginDirectory();
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteDirectory(pluginsTestDir);
    }

    @Test
    public void testAtlassianPlugin() throws Exception {
        addTestModuleDecriptors();
        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(2, Iterables.size(plugins));

        for (final Plugin plugin : plugins) {
            assertTrue(plugin.getName().equals("Test Class Loaded Plugin") || plugin.getName().equals("Test Class Loaded Plugin 2"));

            if (plugin.getName().equals("Test Class Loaded Plugin")) // asserts for first plugin
            {
                assertEquals("Test Class Loaded Plugin", plugin.getName());
                assertEquals("test.atlassian.plugin.classloaded", plugin.getKey());
                assertEquals(1, plugin.getModuleDescriptors().size());
                final MockAnimalModuleDescriptor paddingtonDescriptor = (MockAnimalModuleDescriptor) plugin.getModuleDescriptor("paddington");
                paddingtonDescriptor.enabled();
                assertEquals("Paddington Bear", paddingtonDescriptor.getName());
                final MockBear paddington = (MockBear) paddingtonDescriptor.getModule();
                assertEquals("com.atlassian.plugin.mock.MockPaddington", paddington.getClass().getName());
            } else if (plugin.getName().equals("Test Class Loaded Plugin 2")) // asserts for second plugin
            {
                assertEquals("Test Class Loaded Plugin 2", plugin.getName());
                assertEquals("test.atlassian.plugin.classloaded2", plugin.getKey());
                assertEquals(1, plugin.getModuleDescriptors().size());
                final MockAnimalModuleDescriptor poohDescriptor = (MockAnimalModuleDescriptor) plugin.getModuleDescriptor("pooh");
                poohDescriptor.enabled();
                assertEquals("Pooh Bear", poohDescriptor.getName());
                final MockBear pooh = (MockBear) poohDescriptor.getModule();
                assertEquals("com.atlassian.plugin.mock.MockPooh", pooh.getClass().getName());
            } else {
                fail("What plugin name?!");
            }
        }
    }

    private void addTestModuleDecriptors() {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
    }

    @Test
    public void testSupportsAdditionAndRemoval() {
        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);
        assertTrue(loader.supportsAddition());
        assertTrue(loader.supportsRemoval());
    }

    @Test
    public void testNoFoundPlugins() throws PluginParseException {
        addTestModuleDecriptors();
        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);
        Iterable<Plugin> col = loader.loadFoundPlugins(moduleDescriptorFactory);
        assertFalse(Iterables.isEmpty(col));

        col = loader.loadFoundPlugins(moduleDescriptorFactory);
        assertTrue(Iterables.isEmpty(col));
    }

    @Test
    public void testFoundPlugin() throws PluginParseException, IOException {
        //delete paddington for the timebeing
        final File paddington = new File(pluginsTestDir + File.separator + PADDINGTON_JAR);
        paddington.delete();

        addTestModuleDecriptors();
        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);
        loader.loadAllPlugins(moduleDescriptorFactory);

        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);

        Iterable<Plugin> col = loader.loadFoundPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(col));
        // next time we shouldn't find any new plugins
        col = loader.loadFoundPlugins(moduleDescriptorFactory);
        assertEquals(0, Iterables.size(col));
    }

    @Test
    public void testRemovePlugin() throws PluginException, IOException {
        addTestModuleDecriptors();
        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        //duplicate the paddington plugin before removing the original
        //the duplicate will be used to restore the deleted original after the test

        final Iterator<Plugin> iter = plugins.iterator();

        Plugin paddingtonPlugin = null;

        while (iter.hasNext()) {
            final Plugin plugin = iter.next();

            if (plugin.getName().equals("Test Class Loaded Plugin")) {
                paddingtonPlugin = plugin;
                break;
            }
        }

        if (paddingtonPlugin == null) {
            fail("Can't find test plugin 1 (paddington)");
        }

        loader.removePlugin(paddingtonPlugin);
    }

    @Test
    public void testInvalidPluginHandled() throws IOException, PluginParseException {
        createJarFile("evilplugin.jar", PluginAccessor.Descriptor.FILENAME, pluginsTestDir.getAbsolutePath());

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals("evil jar wasn't loaded, but other plugins were", pluginsTestDir.list(new FilenameFilter() {

            public boolean accept(final File directory, final String fileName) {
                return fileName.endsWith(".jar");
            }
        }).length, Iterables.size(plugins));

        assertEquals(1, Iterables.size(Iterables.filter(plugins, new Predicate<Plugin>() {
            @Override
            public boolean apply(final Plugin input) {
                return input instanceof UnloadablePlugin;
            }
        })));
    }

    @Test
    public void testInstallPluginTwice() throws URISyntaxException, IOException, PluginParseException, InterruptedException {
        FileUtils.cleanDirectory(pluginsTestDir);
        final File plugin = new File(pluginsTestDir, "some-plugin.jar");
        new PluginJarBuilder("plugin").addPluginInformation("some.key", "My name", "1.0", 1).addResource("foo.txt", "foo").build().renameTo(plugin);

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(plugins));
        assertNotNull((plugins.iterator().next()).getResource("foo.txt"));
        assertNull((plugins.iterator().next()).getResource("bar.txt"));

        Thread.currentThread();
        // sleep to ensure the new plugin is picked up
        Thread.sleep(1000);

        plugin.delete(); //delete the old plugin artifact to make windows happy
        new PluginJarBuilder("plugin").addPluginInformation("some.key", "My name", "1.0", 1).addResource("bar.txt", "bar").build().renameTo(plugin);
        plugins = loader.loadFoundPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(plugins));
        assertNull((plugins.iterator().next()).getResource("foo.txt"));
        assertNotNull((plugins.iterator().next()).getResource("bar.txt"));
        assertTrue(plugin.exists());

    }

    @Test
    public void testMixedFactories() throws URISyntaxException, IOException, PluginParseException, InterruptedException {
        FileUtils.cleanDirectory(pluginsTestDir);
        final File plugin = new File(pluginsTestDir, "some-plugin.jar");
        new PluginJarBuilder("plugin").addPluginInformation("some.key", "My name", "1.0", 1).addResource("foo.txt", "foo").build().renameTo(plugin);
        FileUtils.writeStringToFile(new File(pluginsTestDir, "foo.xml"), "<atlassian-plugin key=\"jim\"></atlassian-plugin>");

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertEquals(2, Iterables.size(plugins));
    }

    @Test
    public void testUnknownPluginArtifact() throws URISyntaxException, IOException, PluginParseException, InterruptedException {
        FileUtils.cleanDirectory(pluginsTestDir);
        FileUtils.writeStringToFile(new File(pluginsTestDir, "foo.bob"), "<an>");

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(plugins));
        assertTrue(plugins.iterator().next() instanceof UnloadablePlugin);
    }

    @Test
    public void testPluginWithModuleDescriptorWithNoKey() throws Exception, IOException, PluginParseException, InterruptedException {
        FileUtils.cleanDirectory(pluginsTestDir);
        new PluginJarBuilder("first")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='test.plugin'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <object/>",
                        "</atlassian-plugin>")
                .build(pluginsTestDir);

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(plugins));
        assertTrue(plugins.iterator().next() instanceof UnloadablePlugin);
        assertEquals("test.plugin", plugins.iterator().next().getKey());
    }

    @Test
    public void testPluginWithBadDescriptor() throws Exception, IOException, PluginParseException, InterruptedException {
        FileUtils.cleanDirectory(pluginsTestDir);
        File pluginJar = new PluginJarBuilder("first")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-pluasdfasdf")
                .build(pluginsTestDir);

        loader = new DirectoryPluginLoader(pluginsTestDir, DEFAULT_PLUGIN_FACTORIES, pluginEventManager);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertEquals(1, Iterables.size(plugins));
        assertTrue(plugins.iterator().next() instanceof UnloadablePlugin);
        assertEquals(pluginJar.getName(), plugins.iterator().next().getKey());
    }

    private void createJarFile(final String jarname, final String jarEntry, final String saveDir) throws IOException {
        final OutputStream os = new FileOutputStream(saveDir + File.separator + jarname);
        final JarOutputStream plugin1 = new JarOutputStream(os);
        final JarEntry jarEntry1 = new JarEntry(jarEntry);

        plugin1.putNextEntry(jarEntry1);
        plugin1.closeEntry();
        plugin1.flush();
        plugin1.close();
    }

}
