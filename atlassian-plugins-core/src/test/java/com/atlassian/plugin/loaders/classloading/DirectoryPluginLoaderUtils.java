package com.atlassian.plugin.loaders.classloading;

import com.atlassian.plugin.test.PluginTestUtils;
import com.atlassian.plugin.util.ClassLoaderUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class DirectoryPluginLoaderUtils {
    private static final String TEST_PLUGIN_DIRECTORY = "ap-plugins";
    public static final String PADDINGTON_JAR = "paddington-test-plugin.jar";
    public static final String POOH_JAR = "pooh-test-plugin.jar";

    /**
     * Copies the test plugins to a new temporary directory and returns that directory.
     */
    public static File copyTestPluginsToTempDirectory() throws IOException {
        File directory = PluginTestUtils.createTempDirectory(DirectoryPluginLoaderUtils.class);
        FileUtils.copyDirectory(getTestPluginsDirectory(), directory);

        // Clean up version control files in case we copied them by mistake.
        FileUtils.deleteDirectory(new File(directory, "CVS"));
        FileUtils.deleteDirectory(new File(directory, ".svn"));

        return directory;
    }

    /**
     * Returns the directory on the classpath where the test plugins live.
     */
    public static File getTestPluginsDirectory() {
        URL url = ClassLoaderUtils.getResource(TEST_PLUGIN_DIRECTORY, DirectoryPluginLoaderUtils.class);
        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            // Shouldn't happen, but fallback to getFile if the above fails. getFile breaks for paths with spaces
            // and other special characters because they get URL encoded
            return new File(url.getFile());
        }
    }

    public static ScannerDirectories createFillAndCleanTempPluginDirectory() throws IOException {
        File pluginsDirectory = getTestPluginsDirectory();
        File pluginsTestDir = copyTestPluginsToTempDirectory();
        return new ScannerDirectories(pluginsDirectory, pluginsTestDir);
    }

    public static class ScannerDirectories {
        public final File pluginsDirectory;
        public final File pluginsTestDir;

        public ScannerDirectories(File pluginsDirectory, File pluginsTestDir) {
            this.pluginsDirectory = pluginsDirectory;
            this.pluginsTestDir = pluginsTestDir;
        }
    }
}
