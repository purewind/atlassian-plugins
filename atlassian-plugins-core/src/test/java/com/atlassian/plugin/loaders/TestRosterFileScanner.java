package com.atlassian.plugin.loaders;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.test.PluginTestUtils;
import com.google.common.base.Function;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.atlassian.plugin.loaders.classloading.TestDeploymentUnit.deploymentUnitWithPath;
import static com.google.common.collect.Lists.transform;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestRosterFileScanner {
    private File temporaryDirectory;
    private File rosterFile;

    private RosterFileScanner rosterFileScanner;

    @Before
    public void setUp() throws Exception {
        temporaryDirectory = PluginTestUtils.createTempDirectory(RosterFileScanner.class);
        rosterFile = new File(temporaryDirectory, "rosterFile.list");
        rosterFileScanner = new RosterFileScanner(rosterFile);
    }

    @After
    public void tearDown() throws Exception {
        deleteQuietly(temporaryDirectory);
    }

    @Test
    public void scanOfMissingFileIsEmpty() {
        assertThat(rosterFileScanner.scan(), empty());
    }

    @Test
    public void scanAndGetDeploymentUnitsWithAbsolutePaths() throws Exception {
        final List<File> expectedFiles = writeRosterFile(rosterFile, "/absoluteSimple", "/absolute/compound");

        // First scan should yield expected deployment units
        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedFiles));
        // A get should also yield expected deployment units
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedFiles));
    }

    @Test
    public void scanAndGetDeploymentUnitsWithRelativePaths() throws Exception {
        final List<File> expectedFiles = writeRosterFile(rosterFile, "relativeSimple", "relative/simple");

        // First scan should yield expected deployment units
        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedFiles));
        // A get should also yield expected deployment units
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedFiles));
    }

    @Test
    public void getDeploymentUnitsIsEmptyBeforeScan() {
        assertThat(rosterFileScanner.getDeploymentUnits(), empty());
    }

    @Test
    public void scanAfterResetReturnsDeploymentUnits() throws Exception {
        final List<File> expectedFiles = writeRosterFile(rosterFile, "/one", "/two");

        // First scan should yield expected deployment units
        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedFiles));
        // A get should also yield expected deployment units
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedFiles));
        // Subsequent scan should yield no new deployment units
        assertThat(rosterFileScanner.scan(), empty());

        rosterFileScanner.reset();

        // After reset, a get should see nothing
        assertThat(rosterFileScanner.getDeploymentUnits(), empty());
        // A new scan should yield the expected deployment units
        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedFiles));
        // At which point get should see them also
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedFiles));
    }

    @Test
    public void scanAfterFileUpdateYieldsNewUnits() throws Exception {
        final List<File> expectedBefore = writeRosterFile(rosterFile, "/one", "/two", "/three");
        // We need the file modified time to change when we update it, so just backdate the first copy
        backdateFile(rosterFile);

        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedBefore));
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedBefore));

        final List<File> expectedGetAfter = writeRosterFile(rosterFile, "/three", "/four", "/five");
        // This is a tiny bit brittle, but doing better is quite a pain - we know only 1 element is common with before
        final List<File> expectedScanAfter = expectedGetAfter.subList(1, expectedGetAfter.size());

        assertThat(rosterFileScanner.scan(), containsDeploymentUnitsFor(expectedScanAfter));
        assertThat(rosterFileScanner.getDeploymentUnits(), containsDeploymentUnitsFor(expectedGetAfter));
    }

    @Test
    public void removeDoesNotFail() {
        final DeploymentUnit deploymentUnit = new DeploymentUnit(new File("removed"));
        rosterFileScanner.remove(deploymentUnit);
    }

    @Test
    public void isKnownRosterFileFormatTrueForList() {
        assertThat(RosterFileScanner.isKnownRosterFileFormat(new File("some.list")), is(true));
    }

    @Test
    public void isKnownRosterFileFormatFalseForUnsuffixed() {
        assertThat(RosterFileScanner.isKnownRosterFileFormat(new File("unsuffixed")), is(false));
    }

    public static List<File> writeRosterFile(final File rosterFile, final String... paths)
            throws IOException {
        final List<String> listOfPaths = Arrays.asList(paths);
        FileUtils.writeLines(rosterFile, listOfPaths);
        return transform(listOfPaths, new Function<String, File>() {
            @Override
            public File apply(final String path) {
                // Infer whether the path is absolute or not. A quick check is good enough for tests where we control the input
                // data, and has the added advantage of keeping us independent of the implementation of this feature.
                final boolean absolute = '/' == path.charAt(0);
                return absolute ? new File(path) : new File(rosterFile.getParentFile(), path);
            }
        });
    }

    public static void backdateFile(final File file) throws Exception {
        if (!file.setLastModified(file.lastModified() - 2000)) {
            throw new Exception("Unable to backdate file '" + file + "'");
        }
    }

    private static Matcher<Iterable<? extends DeploymentUnit>> containsDeploymentUnitsFor(final List<File> files) {
        final List<Matcher<DeploymentUnit>> matchers = transform(files, new Function<File, Matcher<DeploymentUnit>>() {
            @Override
            public Matcher<DeploymentUnit> apply(final File file) {
                return deploymentUnitWithPath(equalTo(file));
            }
        });

        // I don't seem able to get the right overload of containsInAnyOrder without the unchecked cast
        //noinspection unchecked
        return containsInAnyOrder((Collection) matchers);
    }
}
