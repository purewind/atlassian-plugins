package com.atlassian.plugin.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestEnumUtils {
    private final String PROPERTY_NAME = TestEnumUtils.class.getName() + ".propertyName";

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(PROPERTY_NAME);

    private enum TestEnum {
        ALPHA,
        BETA,
        GAMMA,
        DELTA
    }

    private TestEnum lookupValue() {
        return EnumUtils.enumValueFromProperty(PROPERTY_NAME, TestEnum.values(), TestEnum.BETA);
    }

    @Test
    public void checkDefault() {
        assertThat(lookupValue(), is(TestEnum.BETA));
    }

    @Test
    public void valueTracksPropertyValue() {
        System.setProperty(PROPERTY_NAME, "alpha");
        assertThat(lookupValue(), is(TestEnum.ALPHA));
        System.setProperty(PROPERTY_NAME, "GAMMA");
        assertThat(lookupValue(), is(TestEnum.GAMMA));
        System.setProperty(PROPERTY_NAME, "DelTa");
        assertThat(lookupValue(), is(TestEnum.DELTA));
    }
}
