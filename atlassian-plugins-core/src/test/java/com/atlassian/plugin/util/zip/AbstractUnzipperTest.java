package com.atlassian.plugin.util.zip;

import org.junit.Test;

import static com.atlassian.plugin.util.zip.AbstractUnzipper.normaliseAndVerify;
import static org.junit.Assert.assertEquals;

public class AbstractUnzipperTest {
    @Test
    public void testNormaliseAndVerifySimple() {
        assertEquals("simple", normaliseAndVerify("simple"));
    }

    @Test
    public void testNormaliseAndVerifySubdir() {
        assertEquals("subdir/file", normaliseAndVerify("subdir/file"));
    }

    @Test
    public void testNormaliseAndVerifyRelativeSimple() {
        assertEquals("test-relative.txt", normaliseAndVerify("something/../test-relative.txt"));
    }

    @Test
    public void testNormaliseAndVerifyRelativeComplex() {
        assertEquals("test-relative.txt", normaliseAndVerify("./something/.././test-relative.txt"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNormaliseAndVerifyIllegalRelative() {
        normaliseAndVerify("something/../../test-relative.txt");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNormaliseAndVerifyIllegalRelativeTrailingDots() {
        normaliseAndVerify("something/../test-relative.txt/..");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNormaliseAndVerifyIllegalRelativeLeadingSlash() {
        normaliseAndVerify("/something/../../test-relative.txt");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNormaliseAndVerifyIllegalRelativeLeadingDots() {
        normaliseAndVerify("../test-relative.txt");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNormaliseAndVerifyIllegalRelativeLeadingSeriesOfDots() {
        normaliseAndVerify("./../test-relative.txt");
    }
}
