package com.atlassian.plugin.util.zip;

import com.atlassian.plugin.test.PluginTestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUrlUnzipper {
    private File basedir;
    private File destdir;
    private File zip;
    private File sourcedir;
    private File source1;

    @Before
    public void setUp() throws Exception {
        basedir = PluginTestUtils.createTempDirectory(TestUrlUnzipper.class);
        FileUtils.cleanDirectory(basedir);
        destdir = new File(basedir, "dest");
        destdir.mkdir();
        zip = new File(basedir, "test.zip");
        sourcedir = new File(basedir, "source");
        sourcedir.mkdir();
        source1 = new File(sourcedir, "source1.jar");
        FileUtils.writeStringToFile(source1, "source1");
        source1.setLastModified(source1.lastModified() - 100000);
    }

    @Test
    public void testConditionalUnzip() throws IOException, InterruptedException {
        zip(sourcedir, zip);
        UrlUnzipper unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        unzipper.conditionalUnzip();

        assertEquals(1, destdir.listFiles().length);
        File dest1 = destdir.listFiles()[0];
        assertEquals(source1.lastModified(), dest1.lastModified());
        assertEquals("source1", FileUtils.readFileToString(dest1));

        FileUtils.writeStringToFile(source1, "source1-modified");
        zip(sourcedir, zip);
        unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        unzipper.conditionalUnzip();

        assertEquals(1, destdir.listFiles().length);
        dest1 = destdir.listFiles()[0];
        assertEquals(source1.lastModified(), dest1.lastModified());
        assertEquals("source1-modified", FileUtils.readFileToString(dest1));

    }

    @Test
    public void testConditionalUnzipCleansZipDirectory() throws IOException, InterruptedException {
        File f = new File(destdir, "bogusfile.txt");
        zip(sourcedir, zip);
        UrlUnzipper unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        unzipper.conditionalUnzip();
        assertEquals(1, destdir.listFiles().length); // there's only one..
        assertEquals("source1.jar", destdir.listFiles()[0].getName()); // ..and it's not randomfile.txt
    }

    @Test
    public void testConditionalUnzipWithNoUnzipIfNoFileMod() throws IOException, InterruptedException {
        zip(sourcedir, zip);
        UrlUnzipper unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        unzipper.conditionalUnzip();

        assertEquals(1, destdir.listFiles().length);
        File dest1 = destdir.listFiles()[0];
        assertEquals(source1.lastModified(), dest1.lastModified());
        assertEquals("source1", FileUtils.readFileToString(dest1));

        long ts = source1.lastModified();
        FileUtils.writeStringToFile(source1, "source1-modified");
        source1.setLastModified(ts);
        zip(sourcedir, zip);
        unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        unzipper.conditionalUnzip();

        assertEquals(1, destdir.listFiles().length);
        dest1 = destdir.listFiles()[0];
        assertEquals(source1.lastModified(), dest1.lastModified());
        assertEquals("source1", FileUtils.readFileToString(dest1));
    }

    @Test
    public void testSaveEntryLegalRelativePath() throws IOException {
        zip(sourcedir, zip);
        final UrlUnzipper unzipper = new UrlUnzipper(zip.toURI().toURL(), destdir);
        final ZipEntry entry = mock(ZipEntry.class);

        when(entry.getName()).thenReturn("something/.././test-relative.txt");
        when(entry.isDirectory()).thenReturn(false);
        when(entry.getTime()).thenReturn(source1.lastModified());
        unzipper.saveEntry(new FileInputStream(source1), entry);

        assertEquals(1, destdir.listFiles().length);
        final File dest1 = destdir.listFiles()[0];
        assertEquals("test-relative.txt", dest1.getName());
        assertEquals(source1.lastModified(), dest1.lastModified());
        assertEquals("source1", FileUtils.readFileToString(dest1));
    }

    private void zip(File basedir, File destfile) throws IOException {
        ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(destfile));
        for (File child : basedir.listFiles()) {
            // Zip files have a time granularity of 2 seconds.
            long ZIP_FILE_GRAIN = 2000;
            // Since we want to check that the unzipper restores modification times from the zip, we
            // ensure all our files have exactly representable modification times.
            child.setLastModified((child.lastModified() / ZIP_FILE_GRAIN) * ZIP_FILE_GRAIN);

            ZipEntry entry = new ZipEntry(child.getName());
            entry.setTime(child.lastModified());
            zout.putNextEntry(entry);
            FileInputStream input = new FileInputStream(child);
            IOUtils.copy(input, zout);
            input.close();
        }
        zout.close();
    }
}
