package com.atlassian.plugin.util;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.MockUnusedModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.dom4j.Element;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;

import static com.atlassian.plugin.util.PluginUtils.ATLASSIAN_DEV_MODE;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class TestPluginUtils {
    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(ATLASSIAN_DEV_MODE);

    @Test
    public void testPluginWithNoModuleDoesNotRequireRestartInDevMode() {
        System.setProperty(ATLASSIAN_DEV_MODE, "true");
        final Plugin plugin = mockPluginWithModuleDescriptors();

        assertFalse(PluginUtils.doesPluginRequireRestart(plugin));
    }

    @Test
    public void testPluginWithNoModuleDoesNotRequireRestartNoDevMode() {
        final Plugin plugin = mockPluginWithModuleDescriptors();

        assertFalse(PluginUtils.doesPluginRequireRestart(plugin));
    }

    @Test
    public void testPluginWithModuleRequiringRestartDoesNotRequireRestartInDevMode() {
        System.setProperty(ATLASSIAN_DEV_MODE, "true");
        final Plugin plugin = mockPluginWithModuleDescriptors(new DynamicModuleDescriptor(), new RequiresRestartModuleDescriptor());

        assertFalse(PluginUtils.doesPluginRequireRestart(plugin));
    }

    @Test
    public void testPluginWithModuleRequiringRestartDoesRequireRestartNoDevMode() {
        final Plugin plugin = mockPluginWithModuleDescriptors(new DynamicModuleDescriptor(), new RequiresRestartModuleDescriptor());

        assertTrue(PluginUtils.doesPluginRequireRestart(plugin));
    }

    @Test
    public void testPluginWithNoModuleRequiringRestartDoesNotRequireRestartNoDevMode() {
        final Plugin plugin = mockPluginWithModuleDescriptors(new DynamicModuleDescriptor());

        assertFalse(PluginUtils.doesPluginRequireRestart(plugin));
    }

    private Plugin mockPluginWithModuleDescriptors(final ModuleDescriptor<?>... descriptors) {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getModuleDescriptors()).thenReturn(Lists.newArrayList(descriptors));
        return plugin;
    }

    @Test
    public void testModuleElementAppliesWithNoInformation() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeJira() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("jira"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBamboo() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("bamboo"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeJiraBamboo() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("jira,bamboo"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBambooConfluence() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("bamboo,confluence"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBambooCrowd() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("bamboo,crowd"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBambooCrowdAndBlank() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("bamboo,,crowd, ,"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBambooConfluenceAndBlank() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement("bamboo,, , confluence, "), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithApplicationAttributeBlank() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(" "), applications(), InstallationMode.REMOTE));
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(" ,"), applications(), InstallationMode.REMOTE));
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(" ,, "), applications(), InstallationMode.REMOTE));
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(" ,, ,,,"), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJira() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none()))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementBamboo() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("bamboo", Option.<InstallationMode>none()))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraBamboo() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none()), newMockRestrictElement("bamboo", Option.<InstallationMode>none()))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraConfluence() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none()), newMockRestrictElement("confluence", Option.<InstallationMode>none()))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraAndVersionInRange() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none(), "[4.0,)"))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraAndVersionOutOfRange() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none(), "(,5.0)"))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraAndVersionsInRange() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none(), "(,4.0)", "5.0"))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementJiraAndVersionsOutOfRange() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none(), "(,4.0]", "(5.0,)"))), applications(), InstallationMode.REMOTE));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementMatchingInstallMode() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.some(InstallationMode.LOCAL)))), applications(), InstallationMode.LOCAL));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementNonMatchingInstallMode() {
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.some(InstallationMode.REMOTE)))), applications(), InstallationMode.LOCAL));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementInstallModeUnspecified() {
        assertTrue(PluginUtils.doesModuleElementApplyToApplication(
                newMockModuleElement(null, Lists.newArrayList(newMockRestrictElement("jira", Option.<InstallationMode>none()))), applications(), InstallationMode.LOCAL));
    }

    @Test
    public void testModuleElementAppliesWithRestrictElementInstallModeSpecifiedOnDifferentAppVersion() {
        final List<Element> restricts = Lists.newArrayList(
                newMockRestrictElement("jira", Option.some(InstallationMode.LOCAL), "5.0"),
                newMockRestrictElement("jira", Option.some(InstallationMode.REMOTE), "6.0"));

        assertTrue(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, restricts), applications(), InstallationMode.LOCAL));
        assertFalse(PluginUtils.doesModuleElementApplyToApplication(newMockModuleElement(null, restricts), applications(), InstallationMode.REMOTE));
    }

    private Element newMockRestrictElement(final String app, final Option<InstallationMode> modeOption, final String... versions) {
        final Element restrict = mock(Element.class);
        when(restrict.attributeValue("application")).thenReturn(app);
        if (modeOption.isDefined()) {
            when(restrict.attributeValue("mode")).thenReturn(modeOption.get().toString().toLowerCase());
        }
        if (versions != null && versions.length == 1) {
            when(restrict.attributeValue("version")).thenReturn(versions[0]);
        } else if (versions != null) {
            when(restrict.elements("version")).thenReturn(Lists.transform(newArrayList(versions), new Function<String, Element>() {
                @Override
                public Element apply(final String version) {
                    final Element versionE = mock(Element.class);
                    when(versionE.getText()).thenReturn(version);
                    return versionE;
                }
            }));
        }

        return restrict;
    }

    private HashSet<Application> applications() {
        return newHashSet(jira50(), confluence40());
    }

    private Application confluence40() {
        return newApplication("confluence", "4.0");
    }

    private Application jira50() {
        return newApplication("jira", "5.0");
    }

    private Element newMockModuleElement(final String app) {
        return newMockModuleElement(app, Lists.<Element>newArrayList());
    }

    private Element newMockModuleElement(final String app, final List<Element> restricts) {
        final Element element = mock(Element.class);
        when(element.attributeValue("application")).thenReturn(app);
        when(element.elements("restrict")).thenReturn(restricts);
        return element;
    }

    private Application newApplication(final String name, final String version) {
        final Application app = mock(Application.class);
        when(app.getKey()).thenReturn(name);
        when(app.getVersion()).thenReturn(version);
        return app;
    }

    static class DynamicModuleDescriptor extends MockUnusedModuleDescriptor {
    }

    @RequiresRestart
    static class RequiresRestartModuleDescriptor extends MockUnusedModuleDescriptor {
    }
}
