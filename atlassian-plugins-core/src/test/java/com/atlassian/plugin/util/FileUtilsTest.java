package com.atlassian.plugin.util;

import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertNotNull;

public class FileUtilsTest {
    @Test
    public void testToFileWhereUrlContainsSpaces() throws MalformedURLException {
        URL url = new URL("file:/C:/Program Files (X86)/atlassian/jira-home/plugins/bundled-plugins.zip");
        final File file = FileUtils.toFile(url);
        assertNotNull(file);
    }
}
