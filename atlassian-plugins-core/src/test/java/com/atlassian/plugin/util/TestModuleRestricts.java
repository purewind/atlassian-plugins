package com.atlassian.plugin.util;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.InstallationMode;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.dom4j.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class TestModuleRestricts {
    @Mock
    private Element moduleElement;

    @Test
    public void parseModuleWithNoRestrictInformation() {
        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);
        assertTrue(Iterables.isEmpty(restricts.restricts));
    }

    @Test
    public void parseModuleWithApplicationAsAttribute() {
        when(moduleElement.attributeValue("application")).thenReturn("my-app");
        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(1, Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
    }

    @Test
    public void parseModuleWithApplicationAsRestrictElement() {
        final List<Element> restrictElements = mockRestrictElements("my-app");
        when(moduleElement.elements("restrict")).thenReturn(restrictElements);


        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
    }

    @Test
    public void parseModuleWithMultipleApplicationAsRestrictElement() {
        final List<Element> restrictElements = mockRestrictElements("my-app", "my-app2");
        when(moduleElement.elements("restrict")).thenReturn(restrictElements);

        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
        assertEquals(Option.<InstallationMode>none(), Iterables.get(restricts.restricts, 0).mode);
        assertEquals("my-app2", Iterables.get(restricts.restricts, 1).application);
        assertEquals(Option.<InstallationMode>none(), Iterables.get(restricts.restricts, 1).mode);
    }

    @Test
    public void parseModuleWithApplicationAsRestrictElementWithInstallationModeAttribute() {
        final Element restrictElement = mockRestrictElement("my-app");
        when(restrictElement.attributeValue("mode")).thenReturn("local");
        final List<Element> restrictElements = newArrayList(restrictElement);

        when(moduleElement.elements("restrict")).thenReturn(restrictElements);

        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
        assertEquals(Option.option(InstallationMode.LOCAL), Iterables.get(restricts.restricts, 0).mode);
    }

    @Test
    public void parseModuleWithApplicationAsRestrictElementWithVersionRangeAttribute() {
        final Element restrictElement = mockRestrictElement("my-app");
        when(restrictElement.attributeValue("version")).thenReturn("[2.0]");
        final List<Element> restrictElements = newArrayList(restrictElement);

        when(moduleElement.elements("restrict")).thenReturn(restrictElements);

        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
        assertEquals(VersionRange.single("2.0"), Iterables.get(restricts.restricts, 0).version);
    }

    @Test
    public void parseModuleWithApplicationAsRestrictElementWithVersionRangeElement() {
        final Element restrictElement = mockRestrictElement("my-app");
        final Element versionElement = mockVersionElement("[2.0]");
        when(restrictElement.elements("version")).thenReturn(newArrayList(versionElement));
        final List<Element> restrictElements = newArrayList(restrictElement);

        when(moduleElement.elements("restrict")).thenReturn(restrictElements);

        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
        assertEquals(VersionRange.empty().or(VersionRange.single("2.0")), Iterables.get(restricts.restricts, 0).version);
    }

    @Test
    public void parseModuleWithApplicationAsRestrictElementWithVersionRangeElements() {
        final Element restrictElement = mockRestrictElement("my-app");
        final Element versionElement = mockVersionElement("[2.0]");
        final Element versionElement2 = mockVersionElement("[3.0,4.0)");
        when(restrictElement.elements("version")).thenReturn(newArrayList(versionElement, versionElement2));
        final List<Element> restrictElements = newArrayList(restrictElement);

        when(moduleElement.elements("restrict")).thenReturn(restrictElements);

        final ModuleRestricts restricts = ModuleRestricts.parse(moduleElement);

        assertEquals(restrictElements.size(), Iterables.size(restricts.restricts));
        assertEquals("my-app", Iterables.get(restricts.restricts, 0).application);
        assertEquals(VersionRange.empty().or(VersionRange.single("2.0")).or(VersionRange.include("3.0").exclude("4.0")), Iterables.get(restricts.restricts, 0).version);
    }

    private List<Element> mockRestrictElements(String... appNames) {
        return newArrayList(transform(newArrayList(appNames), new Function<String, Element>() {
            @Override
            public Element apply(String appName) {
                return mockRestrictElement(appName);
            }
        }));
    }

    private Element mockRestrictElement(String appName) {
        final Element restrictElement = mock(Element.class);
        when(restrictElement.attributeValue("application")).thenReturn(appName);
        return restrictElement;
    }

    private Element mockVersionElement(String range) {
        final Element versionElement = mock(Element.class);
        when(versionElement.getText()).thenReturn(range);
        return versionElement;
    }
}
