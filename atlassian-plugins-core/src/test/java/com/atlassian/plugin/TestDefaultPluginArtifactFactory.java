package com.atlassian.plugin;

import com.atlassian.plugin.test.PluginJarBuilder;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestDefaultPluginArtifactFactory extends TestCase {
    File testDir;
    File spaceTestDir;
    File file;

    public void setUp() throws IOException {
        testDir = makeTempDir("test");
        spaceTestDir = makeTempDir("space test");
        file = new File(testDir, "some.jar");
    }

    private File makeTempDir(String prefix)
            throws IOException {
        File tempDir = File.createTempFile(prefix, "");
        tempDir.delete();
        tempDir.mkdir();

        return tempDir;
    }

    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(testDir);
        FileUtils.deleteDirectory(spaceTestDir);

        testDir = null;
        spaceTestDir = null;
    }

    public void testCreate() throws IOException {
        doCreationTestInDirectory(testDir);
    }

    public void testCreateWithSpaceInArtifactPath() throws IOException {
        doCreationTestInDirectory(spaceTestDir);
    }

    private void doCreationTestInDirectory(File directory) throws IOException {
        File xmlFile = new File(directory, "foo.xml");
        FileUtils.writeStringToFile(xmlFile, "<xml/>");
        File jarFile = new PluginJarBuilder("jar").build(directory);

        DefaultPluginArtifactFactory factory = new DefaultPluginArtifactFactory();

        PluginArtifact jarArt = factory.create(jarFile.toURI());
        assertNotNull(jarArt);
        assertTrue(jarArt instanceof JarPluginArtifact);

        PluginArtifact xmlArt = factory.create(xmlFile.toURI());
        assertNotNull(xmlArt);
        assertTrue(xmlArt instanceof XmlPluginArtifact);

        try {
            factory.create(new File(testDir, "bob.jim").toURI());
            fail("Should have thrown exception");
        } catch (IllegalArgumentException ex) {
            // test passed
        }
    }

    public void testReferenceModeForbidReferenceDoesNotAllowReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory(FORBID_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        verifyAllowsReference(pluginArtifact, false);
        assertThat(pluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    public void testReferenceModePermitReferenceAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory(PERMIT_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        verifyAllowsReference(pluginArtifact, true);
        assertThat(pluginArtifact.getReferenceMode(), is(PERMIT_REFERENCE));
    }

    public void testDefaultDoesNotAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory();
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        verifyAllowsReference(pluginArtifact, false);
        assertThat(pluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    public void testLegacyReferenceModeForbidReferenceDoesNotAllowReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory(
                PluginArtifact.AllowsReference.ReferenceMode.FORBID_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        verifyAllowsReference(pluginArtifact, false);
    }

    public void testLegacyReferenceModePermitReferenceAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory(
                PluginArtifact.AllowsReference.ReferenceMode.PERMIT_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        verifyAllowsReference(pluginArtifact, true);
    }

    private void verifyAllowsReference(PluginArtifact pluginArtifact, boolean allowsReference) {
        // Modern interface via getReferenceMode
        assertThat(pluginArtifact.getReferenceMode().allowsReference(), is(allowsReference));
        // Legacy support
        // The interface is always provided now
        assertTrue(pluginArtifact instanceof PluginArtifact.AllowsReference);
        // And the query should report what we specified
        assertEquals(((PluginArtifact.AllowsReference) pluginArtifact).allowsReference(), allowsReference);
        // And the convenience interface should likewise report this
        assertEquals(PluginArtifact.AllowsReference.Default.allowsReference(pluginArtifact), allowsReference);
    }
}
