package com.atlassian.plugin.factories;

import com.atlassian.plugin.MockApplication;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.XmlDynamicPlugin;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXmlDynamicPluginFactory {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private XmlDynamicPluginFactory xmlDynamicPluginFactory;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;
    @Mock
    private Element module;
    @Mock
    private PluginArtifact pluginArtifact;

    @Before
    public void before() throws Exception {
        xmlDynamicPluginFactory = new XmlDynamicPluginFactory(new MockApplication().setKey("foo"));
    }

    @Test
    public void createBadXml() {
        when(pluginArtifact.toFile()).thenReturn(new File("sadfasdf"));

        expectedException.expect(PluginParseException.class);

        xmlDynamicPluginFactory.create(pluginArtifact, moduleDescriptorFactory);
    }

    @Test
    public void createModule() {
        final XmlDynamicPlugin plugin = mock(XmlDynamicPlugin.class);

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("XmlDynamicPlugin"));

        xmlDynamicPluginFactory.createModule(plugin, module, moduleDescriptorFactory);
    }

    @Test
    public void creatModuleIncorrectPluginType() {
        final Plugin plugin = mock(Plugin.class);

        assertThat(xmlDynamicPluginFactory.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }
}
