package com.atlassian.plugin.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.util.VersionStringComparator;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class TestAbstractPlugin {
    @Test
    public void compareToSortsByKey() {
        final Plugin alpha = createAbstractPlugin("alpha");
        final Plugin beta = createAbstractPlugin("beta");

        // beta should be after alpha
        assertThat(alpha, lessThan(beta));
        assertThat(beta, greaterThan(alpha));
    }

    @Test
    public void compareToSortsMilestonesBeforeNumericVersions() {
        final Plugin milestone = createAbstractPlugin("foo", "1.2.m2");
        final Plugin numeric = createAbstractPlugin("foo", "1.2.1");

        // numeric v1.2.1 should be after milestone v1.2.m2
        assertThat(milestone, lessThan(numeric));
        assertThat(numeric, greaterThan(milestone));
    }

    @Test
    public void compareToSortsByVersion() {
        final Plugin lateVersion = createAbstractPlugin("foo", "3.4.1");
        final Plugin earlyVersion = createAbstractPlugin("foo", "3.1.4");

        // lateVersion v3.4.1 should be after earlyVersion v3.1.4
        assertThat(earlyVersion, lessThan(lateVersion));
        assertThat(lateVersion, greaterThan(earlyVersion));
    }

    @Test
    public void compareToEqualWhenKeyAndVersionEqual() {
        final Plugin firstPlugin = createAbstractPlugin("foo", "3.1.4");
        final Plugin secondPlugin = createAbstractPlugin("foo", "3.1.4");

        // Plugins are "equal" in order
        assertThat(firstPlugin, comparesEqualTo(secondPlugin));
        assertThat(secondPlugin, comparesEqualTo(firstPlugin));
    }

    @Test
    public void compareToTreatsNullPluginInformationAsVersionZero() {
        final Plugin nullPluginInformation = createAbstractPlugin("foo");
        nullPluginInformation.setPluginInformation(null);
        final Plugin defaultPluginInformation = createAbstractPlugin("foo");

        // p2 has default version (== "0.0")
        assertThat(defaultPluginInformation.getPluginInformation().getVersion(), is("0.0"));
        // compareTo() will "clean up" nullPluginInformation to use version "0" which is considered equal to "0.0"
        assertThat(nullPluginInformation, comparesEqualTo(defaultPluginInformation));
        assertThat(defaultPluginInformation, comparesEqualTo(nullPluginInformation));
    }

    @Test
    public void compareToSortsInvalidVersionBeforeValidVersion() throws Exception {
        final String invalidVersion = "@$%^#";
        assertThat(invalidVersion, not(validVersion()));
        final String validVersion = "3.2";
        assertThat(validVersion, validVersion());

        final Plugin invalidVersionPlugin = createAbstractPlugin("foo", invalidVersion);
        final Plugin validVersionPlugin = createAbstractPlugin("foo", validVersion);

        // The valid version should be after the invalid version
        assertThat(invalidVersionPlugin, lessThan(validVersionPlugin));
        assertThat(validVersionPlugin, greaterThan(invalidVersionPlugin));
    }

    @Test
    public void compareToEqualWhenBothVersionsInvalid() throws Exception {
        final Plugin firstInvalidPlugin = createAbstractPlugin("foo", "@$%^#");
        final Plugin secondInvalidPlugin = createAbstractPlugin("foo", "!!");

        assertThat(firstInvalidPlugin.getPluginInformation().getVersion(), not(validVersion()));
        assertThat(secondInvalidPlugin.getPluginInformation().getVersion(), not(validVersion()));

        // The plugins should sort equally
        assertThat(firstInvalidPlugin, comparesEqualTo(secondInvalidPlugin));
        assertThat(secondInvalidPlugin, comparesEqualTo(firstInvalidPlugin));
    }

    @Test
    public void compareToSortsNullKeyBeforeNonNullKey() {
        final Plugin nullKey = createAbstractPlugin();
        final Plugin nonNullKey = createAbstractPlugin("foo");

        // null should be before "foo"
        assertThat(nullKey.getKey(), nullValue());
        assertThat(nullKey, lessThan(nonNullKey));
        assertThat(nonNullKey, greaterThan(nullKey));
    }

    @Test
    public void compareToEqualWhenBothKeysNull() {
        final Plugin firstNullPlugin = createAbstractPlugin();
        final Plugin secondNullPlugin = createAbstractPlugin();

        assertThat(firstNullPlugin.getKey(), nullValue());
        assertThat(secondNullPlugin.getKey(), nullValue());
        assertThat(firstNullPlugin, comparesEqualTo(secondNullPlugin));
        assertThat(secondNullPlugin, comparesEqualTo(firstNullPlugin));
    }

    @Test
    public void getNameDefaultsToKey() {
        final AbstractPlugin plugin = createAbstractPlugin("foo");
        assertThat(plugin.getName(), is("foo"));
    }

    @Test
    public void getNameReturnsSetName() {
        final Plugin plugin = createAbstractPlugin("key");
        plugin.setI18nNameKey("i18n");
        plugin.setName("name");
        assertThat(plugin.getName(), is("name"));
    }

    @Test
    public void getNameReturnsBlankIfI18nNameKeySpecified() {
        final Plugin plugin = createAbstractPlugin("foo");
        plugin.setI18nNameKey("i18n");
        assertThat(plugin.getName(), equalToIgnoringWhiteSpace(""));
    }

    @Test(timeout = 5000)
    public void fastAsynchronousEnableIsNotLost() {
        final boolean[] enableThreadDidSet = new boolean[1];
        // This is approximately what OsgiPlugin does - enableInternal moves to ENABLING, starting a background thread does
        // the actual enable. Meanwhile, the main thread polls the state and returns. We use some synchronization to ensure
        // we hit the race that we are interested in.
        final Plugin plugin = new ConcretePlugin() {
            final private Thread enableThread = new Thread() {
                @Override
                public void run() {
                    synchronized (enableThreadDidSet) {
                        // This models what OsgiPlugin#onPluginContainerRefresh does, and then releases the main thread
                        enableThreadDidSet[0] = compareAndSetPluginState(PluginState.ENABLING, PluginState.ENABLED);
                        enableThreadDidSet.notify();
                    }
                }
            };

            private PluginState slowly(final PluginState state) {
                try {
                    // This wait releases the monitor on enableThreadDidSet so that the background thread can progress to
                    // do its state manipulation, and also blocks us until it notifies us post that change so that we
                    // ensure we hit the race condition of interest.
                    enableThreadDidSet.wait();
                } catch (final InterruptedException interruptedException) {
                    throw new RuntimeException(interruptedException);
                }
                return state;
            }

            @Override
            protected PluginState enableInternal() {
                final PluginState state = PluginState.ENABLING;
                setPluginState(state);
                // Synchronize before starting the background thread so we can block it until we get to the critical
                // window when returning from enableInternal
                synchronized (enableThreadDidSet) {
                    enableThread.start();
                    return slowly(PluginState.PENDING);
                }
            }
        };
        plugin.enable();
        // If the background thread wasn't the one that performed the enable, the test is broken
        assertThat(enableThreadDidSet[0], is(true));
        // We should have ended up enabled
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
    }

    @Test(timeout = 5000)
    public void slowAsynchronousEnableIsNotLost() throws Exception {
        final boolean[] enableThreadDidSet = new boolean[1];
        // This is approximately what OsgiPlugin does - enableInternal moves to ENABLING, starting a background thread does
        // the actual enable. Meanwhile, the main thread polls the state and returns. We use some synchronization to ensure
        // we hit the race that we are interested in.
        final Plugin plugin = new ConcretePlugin() {
            final private Thread enableThread = new Thread() {
                @Override
                public void run() {
                    synchronized (enableThreadDidSet) {
                        // This models what OsgiPlugin#onPluginContainerRefresh does, and then releases the main thread
                        enableThreadDidSet[0] = compareAndSetPluginState(PluginState.ENABLING, PluginState.ENABLED);
                        enableThreadDidSet.notify();
                    }
                }
            };

            @Override
            protected PluginState enableInternal() {
                final PluginState state = PluginState.ENABLING;
                setPluginState(state);
                enableThread.start();
                return PluginState.PENDING;
            }
        };
        // Synchronize before enabling the plugin so we can block the background thread to arrange the timing we need to
        // check the correct transition from ENABLING to ENABLED. It's fine to synchronize here IntelliJ, the enableThread
        // field of the inner class also has a run method which synchronizes on this.
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (enableThreadDidSet) {
            plugin.enable();
            // Until the background thread finishes executing, the plugin is still ENABLING
            assertThat(plugin.getPluginState(), is(PluginState.ENABLING));
            // This wait releases the monitor on enableThreadDidSet so that the background thread can progress to
            // do its state manipulation, and also blocks us until it notifies us post that change so that we
            // can check the compareAndSetPluginState result and the PluginState itself.
            enableThreadDidSet.wait();
        }
        // If the background thread wasn't the one that performed the enable, the test is broken
        assertThat(enableThreadDidSet[0], is(true));
        // We should have ended up enabled
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
    }


    @Test
    public void enableTimesAreInitiallyNull() {
        final AbstractPlugin plugin = createAbstractPlugin();
        assertThat(plugin.getDateEnabling(), nullValue());
        assertThat(plugin.getDateEnabled(), nullValue());
    }

    @Test
    public void simpleEnableSetsBothEnableTimes()
            throws InterruptedException {
        final AbstractPlugin plugin = createAbstractPlugin();

        final Date before = ensureTimePasses();

        plugin.enable();
        final Date enabling = plugin.getDateEnabling();
        final Date enabled = plugin.getDateEnabled();

        final Date after = ensureTimePasses();


        assertThat(enabling, notNullValue());
        assertThat(enabled, notNullValue());
        assertThat(before, lessThan(enabling));
        assertThat(enabling, lessThanOrEqualTo(enabled));
        assertThat(enabled, lessThan(after));
    }

    @Test
    public void slowEnableSetsEnableTimesSeparately()
            throws InterruptedException {
        final SlowAbstractPlugin plugin = new SlowAbstractPlugin();

        final Date before = ensureTimePasses();

        plugin.enable();
        assertThat(plugin.getPluginState(), is(PluginState.ENABLING));
        assertThat(plugin.getDateEnabled(), nullValue());
        final Date enabling = plugin.getDateEnabling();

        final Date middle = ensureTimePasses();

        final boolean finishedEnable = plugin.finishEnable();
        assertThat(finishedEnable, is(true));
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
        assertThat(plugin.getDateEnabling(), equalTo(enabling));
        final Date enabled = plugin.getDateEnabled();

        final Date after = ensureTimePasses();

        assertThat(enabling, notNullValue());
        assertThat(enabled, notNullValue());
        assertThat(before, lessThan(enabling));
        assertThat(enabling, lessThan(middle));
        assertThat(middle, lessThan(enabled));
        assertThat(enabled, lessThan(after));
    }

    @Test
    public void secondEnableClearsEnablingTime()
            throws InterruptedException {
        final SlowAbstractPlugin plugin = new SlowAbstractPlugin();

        plugin.enable();

        ensureTimePasses();

        assertThat(plugin.getDateEnabled(), nullValue());
        final Date firstEnabling = plugin.getDateEnabling();
        plugin.finishEnable();

        ensureTimePasses();

        assertThat(plugin.getDateEnabling(), equalTo(firstEnabling));
        final Date firstEnabled = plugin.getDateEnabled();
        plugin.disable();

        ensureTimePasses();

        assertThat(plugin.getDateEnabling(), equalTo(firstEnabling));
        assertThat(plugin.getDateEnabled(), equalTo(firstEnabled));
        plugin.enable();

        ensureTimePasses();

        assertThat(plugin.getDateEnabled(), nullValue());
        final Date secondEnabling = plugin.getDateEnabling();
        plugin.finishEnable();

        ensureTimePasses();

        assertThat(plugin.getDateEnabling(), equalTo(secondEnabling));
        final Date secondEnabled = plugin.getDateEnabled();

        assertThat(firstEnabling, notNullValue());
        assertThat(firstEnabled, notNullValue());
        assertThat(secondEnabling, notNullValue());
        assertThat(secondEnabled, notNullValue());
        assertThat(firstEnabling, lessThan(firstEnabled));
        assertThat(firstEnabled, lessThan(secondEnabling));
        assertThat(secondEnabling, lessThan(secondEnabled));
    }

    @Test
    public void dynamicModuleDescriptors() {
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);

        final AbstractPlugin abstractPlugin = createAbstractPlugin();

        assertThat(abstractPlugin.getModuleDescriptors(), empty());
        assertThat(abstractPlugin.getDynamicModuleDescriptors(), emptyIterable());

        abstractPlugin.addDynamicModuleDescriptor(moduleDescriptor);

        assertThat(abstractPlugin.getModuleDescriptors(), Matchers.contains(moduleDescriptor));
        assertThat(abstractPlugin.getDynamicModuleDescriptors(), Matchers.contains(moduleDescriptor));

        abstractPlugin.removeDynamicModuleDescriptor(moduleDescriptor);

        assertThat(abstractPlugin.getModuleDescriptors(), empty());
        assertThat(abstractPlugin.getDynamicModuleDescriptors(), emptyIterable());
    }

    private AbstractPlugin createAbstractPlugin(final String key, final String version) {
        final AbstractPlugin plugin = createAbstractPlugin(key);
        plugin.getPluginInformation().setVersion(version);
        return plugin;
    }

    private AbstractPlugin createAbstractPlugin(final String key) {
        final AbstractPlugin plugin = createAbstractPlugin();
        plugin.setKey(key);
        return plugin;
    }

    private AbstractPlugin createAbstractPlugin() {
        return new ConcretePlugin();
    }

    /**
     * Minimal concrete subclass of {@link AbstractPlugin} for testing.
     */
    private static class ConcretePlugin extends AbstractPlugin {
        public ConcretePlugin() {
            super(null);
        }

        @Override
        public boolean isUninstallable() {
            return false;
        }

        @Override
        public boolean isDeleteable() {
            return false;
        }

        @Override
        public boolean isDynamicallyLoaded() {
            return false;
        }

        @Override
        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
            return null;
        }

        @Override
        public ClassLoader getClassLoader() {
            return null;
        }

        @Override
        public URL getResource(final String path) {
            return null;
        }

        @Override
        public InputStream getResourceAsStream(final String name) {
            return null;
        }
    }

    /**
     * A test fixture that mimics the OsgiPlugin usage of AbstractPlugin.
     */
    private class SlowAbstractPlugin extends ConcretePlugin {
        @Override
        public PluginState enableInternal() {
            return PluginState.ENABLING;
        }

        public boolean finishEnable() {
            return compareAndSetPluginState(PluginState.ENABLING, PluginState.ENABLED);
        }
    }

    /**
     * Ensure time passes during a test so timestamps can be reliably compared.
     *
     * @return a date which is strictly between the time before and the time after this method was
     * called.
     */
    private Date ensureTimePasses()
            throws InterruptedException {
        final long before = System.currentTimeMillis();
        while (before >= System.currentTimeMillis()) {
            Thread.sleep(2);
        }
        final Date date = new Date();
        while (date.getTime() >= System.currentTimeMillis()) {
            Thread.sleep(2);
        }
        return date;
    }

    private static final Matcher<String> VALID_VERSION_MATCHER = new TypeSafeMatcher<String>() {
        @Override
        protected boolean matchesSafely(final String version) {
            return VersionStringComparator.isValidVersionString(version);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("valid version string");
        }
    };

    private Matcher<String> validVersion() {
        return VALID_VERSION_MATCHER;
    }

    // These methods test the plugin compareTo() function, which compares plugins based on their version numbers.
    @Test
    public void testComparePluginNewer() {

        final Plugin p1 = createPluginWithVersion("1.1");
        final Plugin p2 = createPluginWithVersion("1.0");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.10");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.01");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.0.1");
        p2.getPluginInformation().setVersion("1.0");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.1.1");
        assertTrue(p1.compareTo(p2) == 1);
    }

    @Test
    public void testComparePluginOlder() {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.1");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.10");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.01");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.0");
        p2.getPluginInformation().setVersion("1.0.1");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.1.1");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == -1);
    }

    @Test
    public void testComparePluginEqual() {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.0");
        assertTrue(p1.compareTo(p2) == 0);

        p1.getPluginInformation().setVersion("1.1.0.0");
        p2.getPluginInformation().setVersion("1.1");
        assertTrue(p1.compareTo(p2) == 0);

        p1.getPluginInformation().setVersion(" 1 . 1 ");
        p2.getPluginInformation().setVersion("1.1");
        assertTrue(p1.compareTo(p2) == 0);
    }

    // If we can't understand the version of a plugin, then take the new one.
    @Test
    public void testComparePluginNoVersion() {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("#$%");
        assertEquals(1, p1.compareTo(p2));

        p1.getPluginInformation().setVersion("#$%");
        p2.getPluginInformation().setVersion("1.0");
        assertEquals(-1, p1.compareTo(p2));
    }

    @Test
    public void testComparePluginBadPlugin() {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.0");

        // Compare against something with a different key
        p2.setKey("bad.key");
        assertTrue(p1.compareTo(p2) != 0);
    }

    public static Plugin createPluginWithVersion(final String version) {
        final Plugin p = new ConcretePlugin();
        p.setKey("test.default.plugin");
        final PluginInformation pInfo = p.getPluginInformation();
        pInfo.setVersion(version);
        return p;
    }
}
