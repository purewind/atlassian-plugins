package com.atlassian.plugin.predicate;

import com.atlassian.plugin.Plugin;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginKeyPatternsPredicate {
    private final List<String> parts = Arrays.asList("com\\.atlassian\\.end", "com\\.atlassian\\.part\\..*");

    @Test
    public void naiveIncludeExamples() {
        final PluginPredicate predicate = new PluginKeyPatternsPredicate(PluginKeyPatternsPredicate.MatchType.MATCHES_ANY, parts);
        assertThat(predicate.matches(pluginWithKey("com.atlassian.end")), is(true));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.ene")), is(false));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.enda")), is(false));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part")), is(false));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part.a")), is(true));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part.b")), is(true));
    }

    @Test
    public void naiveExcludeExamples() {
        final PluginPredicate predicate = new PluginKeyPatternsPredicate(PluginKeyPatternsPredicate.MatchType.MATCHES_NONE, parts);
        assertThat(predicate.matches(pluginWithKey("com.atlassian.end")), is(false));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.ene")), is(true));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.enda")), is(true));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part")), is(true));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part.a")), is(false));
        assertThat(predicate.matches(pluginWithKey("com.atlassian.part.b")), is(false));
    }

    private Plugin pluginWithKey(final String key) {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn(key);
        return plugin;
    }
}
