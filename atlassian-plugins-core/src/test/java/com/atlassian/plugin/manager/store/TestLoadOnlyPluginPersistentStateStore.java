package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestLoadOnlyPluginPersistentStateStore {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void defaultConstructedReadOnlyStoreHasEmptyState() {
        final PluginPersistentStateStore store = new LoadOnlyPluginPersistentStateStore();
        final PluginPersistentState state = store.load();
        assertThat(state.getMap().isEmpty(), is(true));
    }

    @Test
    public void defaultConstructedReadOnlyStoreStateIsDefaulted() {
        final Plugin enabledByDefaultPlugin = mock(Plugin.class);
        when(enabledByDefaultPlugin.isEnabledByDefault()).thenReturn(true);
        final Plugin disabledByDefaultPlugin = mock(Plugin.class);
        when(disabledByDefaultPlugin.isEnabledByDefault()).thenReturn(false);

        final PluginPersistentStateStore store = new LoadOnlyPluginPersistentStateStore();
        final PluginPersistentState state = store.load();
        assertThat(state.isEnabled(enabledByDefaultPlugin), is(true));
        assertThat(state.isEnabled(disabledByDefaultPlugin), is(false));
    }

    @Test
    public void saveAlwaysThrows() {
        final PluginPersistentStateStore store = new LoadOnlyPluginPersistentStateStore();
        final PluginPersistentState state = mock(PluginPersistentState.class);

        expectedException.expect(IllegalStateException.class);
        store.save(state);
    }

    @Test
    public void readOnlyStoreGivenStateReturnsThatState() {
        final PluginPersistentState state = mock(PluginPersistentState.class);
        final PluginPersistentStateStore store = new LoadOnlyPluginPersistentStateStore(state);
        assertThat(store.load(), is(state));
    }
}
