package com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkResumingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.loaders.DiscardablePluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.manager.store.DelegatingPluginPersistentStateStore;
import com.atlassian.plugin.manager.store.LoadOnlyPluginPersistentStateStore;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.metadata.ClasspathFilePluginMetadata;
import com.atlassian.plugin.predicate.PluginPredicate;
import com.atlassian.plugin.test.CapturedLogging;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.plugin.manager.DefaultPluginManager.getLateStartupEnableRetryProperty;
import static com.atlassian.plugin.manager.DefaultPluginManager.getMinimumPluginVersionsFileProperty;
import static com.atlassian.plugin.manager.DefaultPluginManager.getStartupOverrideFileProperty;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.FailureMode.FAIL_TO_ENABLE;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.doAnswerPluginStateChangeWhen;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockFailingModuleDescriptor;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPlugin;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginLoaderForPlugins;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginPersistentStateStore;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginWithVersion;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginsSortOrder;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockStateChangePlugin;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockStaticPlugin;
import static com.atlassian.plugin.test.CapturedLogging.didLogInfo;
import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;
import static com.atlassian.plugin.test.PluginTestUtils.createTempDirectory;
import static com.google.common.collect.ImmutableList.copyOf;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.writeLines;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultPluginManagerLifecycle {
    @Rule
    public CapturedLogging capturedLogging = new CapturedLogging(DefaultPluginManager.class);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(
            getStartupOverrideFileProperty(),
            getLateStartupEnableRetryProperty(),
            getMinimumPluginVersionsFileProperty());

    private File temporaryDirectory;
    private File startupOverrideFile;

    /**
     * the object being tested
     */
    private SplitStartupPluginSystemLifecycle manager;

    private PluginEventManager pluginEventManager;

    private ModuleDescriptorFactory descriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());

    private DefaultPluginManager newDefaultPluginManager(PluginLoader... pluginLoaders) {
        DefaultPluginManager dpm = DefaultPluginManager.newBuilder().
                withPluginLoaders(copyOf(pluginLoaders))
                .withModuleDescriptorFactory(descriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withStore(new MemoryPluginPersistentStateStore())
                .withVerifyRequiredPlugins(true)
                .build();
        manager = dpm;
        return dpm;
    }

    @Before
    public void setUp() throws Exception {
        pluginEventManager = new DefaultPluginEventManager();
        temporaryDirectory = createTempDirectory(TestDefaultPluginManager.class);
        startupOverrideFile = new File(temporaryDirectory, "startupOverride.properties");
    }

    @After
    public void tearDown() throws Exception {
        deleteQuietly(temporaryDirectory);
    }


    @Test
    public void delayedPluginsCanBeDisabled() {
        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";

        Wrapper wrapper = new Wrapper(earlyKey, laterKey).invoke(true);
        DefaultPluginManager defaultPluginManager = wrapper.getDefaultPluginManager();
        Plugin laterPlugin = wrapper.getLaterPlugin();

        defaultPluginManager.earlyStartup();
        defaultPluginManager.disablePlugin(laterKey);

        defaultPluginManager.lateStartup();
        verify(laterPlugin, never()).enable();
    }

    @Test
    public void delayedPluginsCanBeEnabled() {
        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";

        Wrapper wrapper = new Wrapper(earlyKey, laterKey).invoke(false);
        DefaultPluginManager defaultPluginManager = wrapper.getDefaultPluginManager();
        Plugin laterPlugin = wrapper.getLaterPlugin();
        defaultPluginManager.earlyStartup();
        defaultPluginManager.enablePlugin(laterKey);

        defaultPluginManager.lateStartup();
        verify(laterPlugin).enable();
    }

    private class Wrapper {
        private final String earlyKey;
        private final String laterKey;
        private Plugin laterPlugin;
        private DefaultPluginManager defaultPluginManager;

        public Wrapper(String earlyKey, String laterKey) {
            this.earlyKey = earlyKey;
            this.laterKey = laterKey;
        }

        public Plugin getLaterPlugin() {
            return laterPlugin;
        }

        public DefaultPluginManager getDefaultPluginManager() {
            return defaultPluginManager;
        }

        public Wrapper invoke(final boolean isLatePluginEnabledByDefault) {
            final PluginPersistentStateStore pluginPersistentStateStore = mock(
                    PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
            when(pluginPersistentStateStore.load().getPluginRestartState(earlyKey)).thenReturn(PluginRestartState.NONE);
            when(pluginPersistentStateStore.load().getPluginRestartState(laterKey)).thenReturn(PluginRestartState.NONE);
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                    final PluginPersistentState pluginState = (PluginPersistentState) invocationOnMock.getArguments()[0];
                    when(pluginPersistentStateStore.load()).thenReturn(pluginState);
                    return null;
                }
            }).when(pluginPersistentStateStore).save(isA(PluginPersistentState.class));

            PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
            Plugin earlyPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
            laterPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
            when(earlyPlugin.getKey()).thenReturn(earlyKey);
            when(laterPlugin.getKey()).thenReturn(laterKey);
            when(earlyPlugin.isEnabledByDefault()).thenReturn(true);
            when(laterPlugin.isEnabledByDefault()).thenReturn(isLatePluginEnabledByDefault);
            List<Plugin> bothPlugins = Arrays.asList(earlyPlugin, laterPlugin);
            when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(bothPlugins);
            List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

            ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

            PluginEventManager pluginEventManager = mock(PluginEventManager.class);

            PluginPredicate pluginPredicate = mock(PluginPredicate.class);
            when(pluginPredicate.matches(earlyPlugin)).thenReturn(false);
            when(pluginPredicate.matches(laterPlugin)).thenReturn(true);

            defaultPluginManager = new DefaultPluginManager(
                    pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);
            return this;
        }
    }

    @Test
    public void scanForNewPluginsNotAllowedBeforeLateStartup() {
        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.<Plugin>asList());
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager);

        defaultPluginManager.earlyStartup();

        expectedException.expect(IllegalStateException.class);
        defaultPluginManager.scanForNewPlugins();
    }

    @Test
    public void scanForNewPluginsDuringLateStartup() {
        final String pluginKey = "plugin-key";
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(plugin.getKey()).thenReturn(pluginKey);
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);

        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().isEnabled(plugin)).thenReturn(true);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(Arrays.asList(plugin));
        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Arrays.<Plugin>asList());
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        when(pluginPredicate.matches(plugin)).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        // Set up to do the equivalent of what connect is doing in ACDEV-1276, namely responding to plugin enable with
        // a reentrant plugin installation.
        final Answer<Object> scanForNewPlugins = new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                defaultPluginManager.scanForNewPlugins();
                return null;
            }
        };
        doAnswer(scanForNewPlugins).when(pluginEventManager).broadcast(isA(PluginEnabledEvent.class));

        defaultPluginManager.earlyStartup();
        defaultPluginManager.lateStartup();
    }


    @Test
    public void earlyStartupDoesNotSavePluginPersistentState() {
        // Make a family of plugins for each possible persistent state
        final String pluginKeyPrefix = "pluginWithRestartState_";
        final ImmutableList.Builder<Plugin> pluginListBuilder = ImmutableList.<Plugin>builder();
        final PluginPersistentState.Builder pluginPersistentStateBuilder = PluginPersistentState.Builder.create();
        for (final PluginRestartState pluginRestartState : PluginRestartState.values()) {
            final String pluginKey = pluginKeyPrefix + pluginRestartState.toString();
            final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
            when(plugin.getKey()).thenReturn(pluginKey);
            pluginListBuilder.add(plugin);
            pluginPersistentStateBuilder.setPluginRestartState(pluginKey, pluginRestartState);

        }
        final PluginPersistentStateStore pluginPersistentStateStore = mock(PluginPersistentStateStore.class);
        when(pluginPersistentStateStore.load()).thenReturn(pluginPersistentStateBuilder.toState());
        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(pluginListBuilder.build());
        final List<PluginLoader> pluginLoaders = ImmutableList.of(pluginLoader);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        when(pluginPredicate.matches(any(Plugin.class))).thenReturn(false);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();
        verify(pluginPersistentStateStore, never()).save(any(PluginPersistentState.class));
    }

    @Test
    public void lateStartupRemovesPluginsMarkedForRemoval() {
        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";
        final Plugin earlyPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final Plugin laterPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(earlyPlugin.getKey()).thenReturn(earlyKey);
        when(laterPlugin.getKey()).thenReturn(laterKey);

        // Since we're interested in the final computed state, we use a real PluginPersistentStateStore here
        final PluginPersistentStateStore pluginPersistentStateStore = new MemoryPluginPersistentStateStore();
        pluginPersistentStateStore.save(PluginPersistentState.Builder.create()
                .setEnabled(earlyPlugin, !earlyPlugin.isEnabledByDefault())
                .setEnabled(laterPlugin, !laterPlugin.isEnabledByDefault())
                .setPluginRestartState(earlyKey, PluginRestartState.REMOVE)
                .setPluginRestartState(laterKey, PluginRestartState.REMOVE)
                .toState());

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        final List<Plugin> bothPlugins = Arrays.asList(earlyPlugin, laterPlugin);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(bothPlugins);
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        when(pluginPredicate.matches(earlyPlugin)).thenReturn(false);
        when(pluginPredicate.matches(laterPlugin)).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();
        verify(pluginLoader, never()).removePlugin(any(Plugin.class));
        // We can't verify pluginPersistentStateStore.save, because it's not a Mock

        defaultPluginManager.lateStartup();
        verify(pluginLoader).removePlugin(earlyPlugin);
        verify(pluginLoader).removePlugin(laterPlugin);

        final PluginPersistentState pluginPersistentState = pluginPersistentStateStore.load();
        // Enablement should have fallen back to default state
        assertThat(pluginPersistentState.isEnabled(earlyPlugin), is(earlyPlugin.isEnabledByDefault()));
        assertThat(pluginPersistentState.isEnabled(laterPlugin), is(laterPlugin.isEnabledByDefault()));
        // Restart state should have fallen back to default state
        assertThat(pluginPersistentState.getPluginRestartState(earlyKey), is(PluginRestartState.NONE));
        assertThat(pluginPersistentState.getPluginRestartState(laterKey), is(PluginRestartState.NONE));
        // This final test is arguably a bit brittle, but i think it's valid given the documentation for getMap
        assertThat(pluginPersistentState.getMap().size(), is(0));
    }

    @Test
    public void exampleUsingPersistentStateDelegation() {
        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";
        final Plugin earlyPlugin = mock(Plugin.class, RETURNS_MOCKS);
        final Plugin laterPlugin = mock(Plugin.class, RETURNS_MOCKS);
        when(earlyPlugin.getKey()).thenReturn(earlyKey);
        when(laterPlugin.getKey()).thenReturn(laterKey);
        when(earlyPlugin.isEnabledByDefault()).thenReturn(true);
        when(laterPlugin.isEnabledByDefault()).thenReturn(true);

        // This is an example of using a DelegatingPluginPersistentStateStore to manage persistent state
        final boolean tenanted[] = {false};
        final PluginPersistentStateStore warmStore = new LoadOnlyPluginPersistentStateStore();
        final PluginPersistentStateStore tenantedStore = new MemoryPluginPersistentStateStore();
        final PluginPersistentStateStore pluginPersistentStateStore = new DelegatingPluginPersistentStateStore() {
            @Override
            public PluginPersistentStateStore getDelegate() {
                return !tenanted[0] ? warmStore : tenantedStore;
            }
        };

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        final List<Plugin> bothPlugins = Arrays.asList(earlyPlugin, laterPlugin);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(bothPlugins);
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        when(pluginPredicate.matches(earlyPlugin)).thenReturn(false);
        when(pluginPredicate.matches(laterPlugin)).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();

        // Become tenanted
        tenanted[0] = true;

        defaultPluginManager.lateStartup();

        when(earlyPlugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(laterPlugin.getPluginState()).thenReturn(PluginState.ENABLED);

        // Change some things which get persisted
        defaultPluginManager.disablePlugin(earlyKey);
        defaultPluginManager.disablePlugin(laterKey);
        // And check they persist
        final PluginPersistentState pluginPersistentState = tenantedStore.load();
        assertThat(pluginPersistentState.isEnabled(earlyPlugin), is(false));
        assertThat(pluginPersistentState.isEnabled(laterPlugin), is(false));
        // Check that we actually persisted something, which is a good check the test isn't broken.
        assertThat(pluginPersistentState.getMap().size(), is(2));
    }

    @Test
    public void lateStartupDoesntRetryEnableWhenNotRequested() {
        validateLateStartupRetryEnable(false);
    }

    @Test
    public void lateStartupDoesRetryEnableWhenRequested() {
        validateLateStartupRetryEnable(true);
    }

    private void validateLateStartupRetryEnable(final boolean allowEnableRetry) {
        // This functionality is enabled only via a system property
        System.setProperty(getLateStartupEnableRetryProperty(), Boolean.toString(allowEnableRetry));
        // An ordinary plugin which enables as expected when installed
        final Plugin enablesFinePlugin = mockStateChangePlugin("enablesFine", pluginEventManager);
        // A plugin which does not enable when enabled()d, that is, enable() to fails first time
        final Plugin firstEnableFailsPlugin = mockPlugin("firstEnableFails");
        doAnswerPluginStateChangeWhen(firstEnableFailsPlugin, PluginState.INSTALLED, pluginEventManager).install();

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        mockPluginsSortOrder(enablesFinePlugin, firstEnableFailsPlugin);

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(enablesFinePlugin, firstEnableFailsPlugin);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.earlyStartup();

        verify(enablesFinePlugin).enable();
        verify(firstEnableFailsPlugin).enable();

        // Adjust the plugin so it will enable correctly now
        doAnswerPluginStateChangeWhen(firstEnableFailsPlugin, PluginState.ENABLED, pluginEventManager).enable();

        defaultPluginManager.lateStartup();

        // For the plugin which enabled fine, enable has not been called again
        verify(enablesFinePlugin).enable();

        // The other enable is left to the calling test method

        if (allowEnableRetry) {
            // Retry is enabled, so the plugin which didn't enable during earlyStartup should have had enable reattempted,
            // so it has now been called twice
            verify(firstEnableFailsPlugin, times(2)).enable();
            final String pluginString = firstEnableFailsPlugin.toString();
            assertThat(capturedLogging, didLogWarn("Failed to enable", "fallback", "lateStartup", pluginString));
            assertThat(capturedLogging, didLogWarn("fallback enabled", "lateStartup", pluginString));
        } else {
            // Retry is disabled, so still only enabled once
            verify(firstEnableFailsPlugin, times(1)).enable();
            // Currently there is not warning in this case. We might add a warning later, but if so it should not mention
            // "fallback" or "lateStartup": these are the strings i'm mooting as the smoking gun for this problem in the logs.
            assertThat(capturedLogging, not(didLogWarn("fallback", "lateStartup")));
        }
    }

    @Test
    public void startupOverrideFileOverridesPluginInformationAndDelayPredicate() throws Exception {
        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final String earlyOverrideNoInfoKey = "earlyOverrideNoInfoKey";
        final String lateOverrideNoInfoKey = "lateOverrideNoInfoKey";
        final Plugin earlyOverrideNoInfoPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final Plugin lateOverrideNoInfoPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(earlyOverrideNoInfoPlugin.getKey()).thenReturn(earlyOverrideNoInfoKey);
        when(lateOverrideNoInfoPlugin.getKey()).thenReturn(lateOverrideNoInfoKey);

        final String earlyOverrideLateInfoKey = "earlyOverrideLateInfoKey";
        final String lateOverrideEarlyInfoKey = "lateOverrideEarlyInfoKey";
        final Plugin earlyOverrideLateInfoPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final Plugin lateOverrideEarlyInfoPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(earlyOverrideLateInfoPlugin.getKey()).thenReturn(earlyOverrideLateInfoKey);
        when(earlyOverrideLateInfoPlugin.getPluginInformation().getStartup()).thenReturn("late");
        when(lateOverrideEarlyInfoPlugin.getKey()).thenReturn(lateOverrideEarlyInfoKey);
        when(lateOverrideEarlyInfoPlugin.getPluginInformation().getStartup()).thenReturn("early");

        final List<String> overrideContents = Arrays.asList(
                earlyOverrideNoInfoKey + "=early",
                lateOverrideNoInfoKey + "=late",
                earlyOverrideLateInfoKey + "=early",
                lateOverrideEarlyInfoKey + "=late"
        );
        writeLines(startupOverrideFile, overrideContents);
        System.setProperty(getStartupOverrideFileProperty(), startupOverrideFile.getPath());

        // Set the plugin sort order
        mockPluginsSortOrder(
                earlyOverrideLateInfoPlugin, earlyOverrideNoInfoPlugin, lateOverrideEarlyInfoPlugin, lateOverrideNoInfoPlugin);

        final List<Plugin> allPlugins = Arrays.asList(
                earlyOverrideNoInfoPlugin, lateOverrideNoInfoPlugin, earlyOverrideLateInfoPlugin, lateOverrideEarlyInfoPlugin);

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(allPlugins);
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        // This predicate is inverted when there is nothing in the information, so that the override is doing something different.
        // It is inverted from the information version when it is present, since we know that is taking effect (from its test),
        // and we want to ensure we're changing that!
        when(pluginPredicate.matches(earlyOverrideNoInfoPlugin)).thenReturn(true);
        when(pluginPredicate.matches(lateOverrideNoInfoPlugin)).thenReturn(false);
        when(pluginPredicate.matches(earlyOverrideLateInfoPlugin)).thenReturn(false);
        when(pluginPredicate.matches(lateOverrideEarlyInfoPlugin)).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();
        verify(earlyOverrideNoInfoPlugin).install();
        verify(earlyOverrideLateInfoPlugin).install();
        verify(lateOverrideNoInfoPlugin, never()).install();
        verify(lateOverrideEarlyInfoPlugin, never()).install();

        defaultPluginManager.lateStartup();
        verify(lateOverrideNoInfoPlugin).install();
        verify(lateOverrideEarlyInfoPlugin).install();
    }

    @Test
    public void earlyLateStartupEvents() {
        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.<Plugin>asList());
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final SplitStartupPluginSystemLifecycle splitStartupPluginSystemLifecycle = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager);

        splitStartupPluginSystemLifecycle.earlyStartup();
        final ArgumentCaptor<Object> earlyEvents = ArgumentCaptor.forClass(Object.class);
        verify(pluginEventManager, times(2)).broadcast(earlyEvents.capture());
        assertThat(earlyEvents.getAllValues(), contains(
                instanceOf(PluginFrameworkStartingEvent.class), instanceOf(PluginFrameworkDelayedEvent.class)));

        // reset() is a bit naughty, but we want to check events are sent when expected
        reset(pluginEventManager);

        splitStartupPluginSystemLifecycle.lateStartup();
        final ArgumentCaptor<Object> laterEvents = ArgumentCaptor.forClass(Object.class);
        verify(pluginEventManager, times(2)).broadcast(laterEvents.capture());
        assertThat(laterEvents.getAllValues(), contains(
                instanceOf(PluginFrameworkResumingEvent.class), instanceOf(PluginFrameworkStartedEvent.class)));
    }

    @Test
    public void startupElementInPluginInformationOverridesDelayPredicate() {
        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";
        final Plugin earlyPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final Plugin laterPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(earlyPlugin.getKey()).thenReturn(earlyKey);
        when(earlyPlugin.getPluginInformation().getStartup()).thenReturn("early");
        when(laterPlugin.getKey()).thenReturn(laterKey);
        when(laterPlugin.getPluginInformation().getStartup()).thenReturn("late");

        // We need to compareTo to work for plugins that participate in the same addPlugins. Without this, this test can fail for
        // opaque reasons (the install of laterPlugin is dropped as a duplicate). While this currently only happens when the code
        // is broken, it feels brittle enough to guard against.
        when(earlyPlugin.compareTo(laterPlugin)).thenReturn(-1);
        when(laterPlugin.compareTo(earlyPlugin)).thenReturn(1);

        final PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        final List<Plugin> bothPlugins = Arrays.asList(earlyPlugin, laterPlugin);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(bothPlugins);
        final List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        // This predicate is inverted from what you expect, because we want to check the PluginInformation is respected
        when(pluginPredicate.matches(earlyPlugin)).thenReturn(true);
        when(pluginPredicate.matches(laterPlugin)).thenReturn(false);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();
        verify(earlyPlugin).install();
        verify(laterPlugin, never()).install();

        defaultPluginManager.lateStartup();
        verify(laterPlugin).install();
    }

    @Test
    public void delayedPluginsAreDelayed() {
        final String earlyKey = "earlyKey";
        final String laterKey = "laterKey";

        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().getPluginRestartState(earlyKey)).thenReturn(PluginRestartState.NONE);
        when(pluginPersistentStateStore.load().getPluginRestartState(laterKey)).thenReturn(PluginRestartState.NONE);

        PluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        Plugin earlyPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        Plugin laterPlugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(earlyPlugin.getKey()).thenReturn(earlyKey);
        when(laterPlugin.getKey()).thenReturn(laterKey);
        List<Plugin> bothPlugins = Arrays.asList(earlyPlugin, laterPlugin);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(bothPlugins);
        List<PluginLoader> pluginLoaders = Arrays.asList(pluginLoader);

        ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        PluginPredicate pluginPredicate = mock(PluginPredicate.class);
        when(pluginPredicate.matches(earlyPlugin)).thenReturn(false);
        when(pluginPredicate.matches(laterPlugin)).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager, pluginPredicate);

        defaultPluginManager.earlyStartup();
        assertThat(defaultPluginManager.getPlugin(earlyKey), is(earlyPlugin));
        assertThat(defaultPluginManager.getPlugin(laterKey), nullValue());

        defaultPluginManager.lateStartup();
        assertThat(defaultPluginManager.getPlugin(earlyKey), is(earlyPlugin));
        assertThat(defaultPluginManager.getPlugin(laterKey), is(laterPlugin));
    }

    public static class ThingsAreWrongListener {
        private volatile boolean called = false;

        @PluginEventListener
        public void onFrameworkShutdown(final PluginFrameworkShutdownEvent event) {
            called = true;
            throw new NullPointerException("AAAH!");
        }

        public boolean isCalled() {
            return called;
        }
    }

    @Test
    public void testShutdownHandlesException() {
        final ThingsAreWrongListener listener = new ThingsAreWrongListener();
        pluginEventManager.register(listener);

        manager = newDefaultPluginManager();
        manager.init();
        try {
            //this should not throw an exception
            manager.shutdown();
        } catch (Exception e) {
            fail("Should not have thrown an exception!");
        }
        assertTrue(listener.isCalled());
    }

    @Test
    public void testCannotInitTwice() throws PluginParseException {
        manager = newDefaultPluginManager();
        manager.init();
        try {
            manager.init();
            fail("IllegalStateException expected");
        } catch (final IllegalStateException expected) {
        }
    }

    @Test
    public void testCannotShutdownTwice() throws PluginParseException {
        manager = newDefaultPluginManager();
        manager.init();
        manager.shutdown();
        try {
            manager.shutdown();
            fail("IllegalStateException expected");
        } catch (final IllegalStateException expected) {
        }
    }

    private void writeToFile(String file, String line) throws IOException, URISyntaxException {
        final String resourceName = ClasspathFilePluginMetadata.class.getPackage().getName().replace(".", "/") + "/" + file;
        URL resource = getClass().getClassLoader().getResource(resourceName);

        FileOutputStream fout = new FileOutputStream(new File(resource.toURI()), false);
        fout.write(line.getBytes(), 0, line.length());
        fout.close();

    }

    @Test
    public void testExceptionOnRequiredPluginNotEnabling() throws Exception {
        try {
            writeToFile("application-required-modules.txt", "foo.required:bar");
            writeToFile("application-required-plugins.txt", "foo.required");

            final PluginLoader mockPluginLoader = mock(PluginLoader.class);
            final ModuleDescriptor<Object> badModuleDescriptor = mockFailingModuleDescriptor("foo.required:bar", FAIL_TO_ENABLE);

            final AbstractModuleDescriptor goodModuleDescriptor = mock(AbstractModuleDescriptor.class);
            when(goodModuleDescriptor.getKey()).thenReturn("baz");
            when(goodModuleDescriptor.getCompleteKey()).thenReturn("foo.required:baz");

            Plugin plugin = mockStaticPlugin("foo.required", goodModuleDescriptor, badModuleDescriptor);

            when(mockPluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

            manager = newDefaultPluginManager(mockPluginLoader);
            try {
                manager.init();
            } catch (PluginException e) {
                // expected
                assertEquals("Unable to validate required plugins or modules", e.getMessage());
                return;
            }
            fail("A PluginException is expected when trying to initialize the plugins system with required plugins that do not load.");
        } finally {
            // remove references from required files
            writeToFile("application-required-modules.txt", "");
            writeToFile("application-required-plugins.txt", "");
        }
    }

    @Test
    public void minimumPluginVersionIsEnforced() throws Exception {
        // This test tries to install four plugins
        // - alpha has a minimum version specified which is less than its version, and it should be installed
        // - beta has a minimum version specified which is equal to its version, and it should be installed
        // - gamma has a minimum version specified which is greater than its version, and it should not be installed
        // - delta has no minimum version specified, and it should be installed
        // - epsilon has a garbage minimum version specified, and it should be installed
        final String alphaKey = "alpha";
        final String betaKey = "beta";
        final String gammaKey = "gamma";
        final String deltaKey = "delta";
        final String epsilonKey = "epsilon";
        final Plugin alphaPlugin = mockPluginWithVersion(alphaKey, "1.2");
        final Plugin betaPlugin = mockPluginWithVersion(betaKey, "2.3");
        final Plugin gammaPlugin = mockPluginWithVersion(gammaKey, "3.4");
        final Plugin deltaPlugin = mockPluginWithVersion(deltaKey, "5.6");
        final Plugin epsilonPlugin = mockPluginWithVersion(epsilonKey, "7.8");

        final Plugin[] allPlugins = {alphaPlugin, betaPlugin, gammaPlugin, deltaPlugin, epsilonPlugin};

        // We need to compareTo to work for plugins that participate in the same addPlugins, so set a good global order
        mockPluginsSortOrder(allPlugins);
        final DiscardablePluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(ImmutableList.copyOf(allPlugins));
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        manager = new DefaultPluginManager(
                mockPluginPersistentStateStore(),
                ImmutableList.<PluginLoader>of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager
        );

        final File minimumPluginVersionsFile = new File(temporaryDirectory, "mininumPluginVersions.properties");
        final String badEpsilonVersion = "n0V3r$Ion";
        final String largeGammaVersion = "4.5";
        final List<String> minimumPluginVersionsContents = Arrays.asList(
                alphaKey + "=0.1", // less than the version of the plugin
                betaKey + "=2.3", // equal to the version of the plugin
                gammaKey + "=" + largeGammaVersion, // greater than the version of the plugin
                // deltaKey is not here, because it has no specified minimum version...
                epsilonKey + "=" + badEpsilonVersion // garbage version
        );
        writeLines(minimumPluginVersionsFile, minimumPluginVersionsContents);
        System.setProperty(getMinimumPluginVersionsFileProperty(), minimumPluginVersionsFile.getPath());

        manager.init();
        verify(alphaPlugin).install();
        verify(betaPlugin).install();
        verify(gammaPlugin, never()).install();
        // Since gammaPlugin wasn't installed, it should be discarded so the loader can reclaim resources
        verify(pluginLoader).discardPlugin(gammaPlugin);
        verify(deltaPlugin).install();
        verify(epsilonPlugin).install();

        assertThat(capturedLogging, didLogInfo("Unacceptable plugin", gammaPlugin.toString(), largeGammaVersion));
        assertThat(capturedLogging, didLogWarn("Cannot compare minimum version", epsilonPlugin.toString(), badEpsilonVersion));
    }
}
