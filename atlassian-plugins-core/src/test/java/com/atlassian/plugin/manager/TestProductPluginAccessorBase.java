package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class TestProductPluginAccessorBase {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginRegistry.ReadOnly pluginRegistry;
    @Mock
    private PluginPersistentStateStore pluginsPersistentStateStore;
    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;
    @Mock
    private PluginEventManager pluginEventManager;

    @Test
    public void classLoderIsPluginsClassLoader() {
        ProductPluginAccessorBase pluginAccessor = new ProductPluginAccessorBase(pluginRegistry,
                pluginsPersistentStateStore, moduleDescriptorFactory, pluginEventManager);

        // needs to be a PluginsClassLoader to work properly with DPM
        assertThat(pluginAccessor.getClassLoader(), is(instanceOf(PluginsClassLoader.class)));
    }
}
