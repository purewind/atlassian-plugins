package com.atlassian.plugin.predicate;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * A {@link PluginPredicate} that matches enabled plugins.
 */
public class EnabledPluginPredicate implements PluginPredicate {
    private final Set<Plugin> pluginsBeingEnabled;

    public EnabledPluginPredicate(final Set<Plugin> pluginsBeingEnabled) {
        this.pluginsBeingEnabled = pluginsBeingEnabled;
    }

    public boolean matches(@Nonnull final Plugin plugin) {
        return PluginState.ENABLED.equals(plugin.getPluginState()) && !pluginsBeingEnabled.contains(plugin);
    }
}
