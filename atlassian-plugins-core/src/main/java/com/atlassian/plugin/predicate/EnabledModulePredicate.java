package com.atlassian.plugin.predicate;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;

/**
 * A {@link ModuleDescriptorPredicate} that matches enabled modules.
 */
public class EnabledModulePredicate<T> implements ModuleDescriptorPredicate<T> {
    public EnabledModulePredicate() {
    }

    /**
     * @deprecated Since 3.1.0, use {@link #EnabledModulePredicate()} instead.
     */
    @Deprecated
    public EnabledModulePredicate(final PluginAccessor pluginAccessor) {
    }

    public boolean matches(final ModuleDescriptor<? extends T> moduleDescriptor) {
        return moduleDescriptor.isEnabled() && !moduleDescriptor.isBroken();
    }
}
