package com.atlassian.plugin.predicate;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.util.RegularExpressions;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A plugin predicate which matches regular expressions against plugin keys.
 */
@ExperimentalApi
public class PluginKeyPatternsPredicate implements PluginPredicate {
    public enum MatchType {
        /**
         * Specifies a PluginPredicate which matches if any one of a given list of
         * included regular expressions is matched.
         */
        MATCHES_ANY {
            /**
             * Obtain a regular expression which matches any of the provided parts.
             */
            public String buildRegularExpression(final Collection<String> parts) {
                return RegularExpressions.anyOf(parts);
            }

            /**
             * This MatchType matches if the Matcher does match, as we want to match if any of the
             * inclusions provided to {@link #buildRegularExpression} matches.
             */
            public boolean processMatcher(final Matcher matcher) {
                return matcher.matches();
            }
        },

        /**
         * Specifies a PluginPredicate which matches if none of a given list of
         * excluded regular expressions is matched.
         */
        MATCHES_NONE {
            /**
             * Obtain a regular expression which matches any of the provided parts.
             */
            public String buildRegularExpression(final Collection<String> parts) {
                return RegularExpressions.anyOf(parts);
            }

            /**
             * This MatchType matches if the Matcher does not match, as we want to match only when
             * none of the exclusions provided to {@link #buildRegularExpression} matches.
             */
            public boolean processMatcher(final Matcher matcher) {
                return !matcher.matches();
            }
        };

        public abstract String buildRegularExpression(final Collection<String> parts);

        public abstract boolean processMatcher(final Matcher matcher);
    }

    private final MatchType matchType;
    private final Pattern pattern;

    public PluginKeyPatternsPredicate(final MatchType matchType, final Collection<String> parts) {
        this.matchType = matchType;
        this.pattern = Pattern.compile(matchType.buildRegularExpression(parts));
    }

    public boolean matches(final Plugin plugin) {
        return matchType.processMatcher(pattern.matcher(plugin.getKey()));
    }
}
