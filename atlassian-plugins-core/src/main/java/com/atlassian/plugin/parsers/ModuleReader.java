package com.atlassian.plugin.parsers;

import org.dom4j.CharacterData;
import org.dom4j.Element;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Reads module information from a plugin descriptor.
 *
 * @since 3.0.0
 */
public final class ModuleReader {
    private final Element module;

    public ModuleReader(Element module) {
        this.module = checkNotNull(module);
        checkState(!(module instanceof CharacterData), "Module elements cannot be text nodes!");
    }

    public String getType() {
        return module.getName(); // note that's the xml element name, not the module name
    }
}
