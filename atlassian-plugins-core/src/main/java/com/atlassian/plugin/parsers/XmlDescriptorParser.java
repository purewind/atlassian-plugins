package com.atlassian.plugin.parsers;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptor;
import com.atlassian.plugin.impl.UnloadablePluginFactory;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.descriptors.UnloadableModuleDescriptorFactory.createUnloadableModuleDescriptor;
import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.newModuleDescriptor;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * Provides access to the descriptor information retrieved from an XML InputStream.
 * <p/>
 * Uses the dom4j {@link SAXReader} to parse the XML stream into a document
 * when the parser is constructed.
 *
 * @see XmlDescriptorParserFactory
 */
public class XmlDescriptorParser implements DescriptorParser {
    private static final Logger log = LoggerFactory.getLogger(XmlDescriptorParser.class);

    private final PluginDescriptorReader descriptorReader;

    /**
     * Constructs a parser with an already-constructed document
     *
     * @param source       the source document
     * @param applications the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 2.2.0
     */
    public XmlDescriptorParser(final Document source, final Set<Application> applications) throws PluginParseException {
        this.descriptorReader = new PluginDescriptorReader(checkNotNull(source, "XML descriptor source document cannot be null"), checkNotNull(applications));
    }

    /**
     * Constructs a parser with a stream of an XML document for a specific application
     *
     * @param source       The descriptor stream
     * @param applications the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 2.2.0
     */
    public XmlDescriptorParser(final InputStream source, final Set<Application> applications) throws PluginParseException {
        this(createDocument(checkNotNull(source, "XML descriptor source cannot be null")), applications);
    }

    /**
     * Constructs a parser with a stream of an XML document for a specific application
     *
     * @param source              The descriptor stream
     * @param supplementalSources a collection of streams containing supplemental ModuleDescriptors
     * @param applications        the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 3.2.16
     */
    public XmlDescriptorParser(final InputStream source, final Iterable<InputStream> supplementalSources, final Set<Application> applications) throws PluginParseException {
        checkNotNull(source, "XML descriptor source cannot be null");
        checkNotNull(supplementalSources, "Supplemental XML descriptors cannot be null");
        Document mainDescriptor = createDocument(source);
        final Iterable<Document> supplementalDocs = Iterables.transform(supplementalSources, new Function<InputStream, Document>() {
            @Override
            public Document apply(@Nullable InputStream source) {
                return createDocument(source);
            }
        });
        mainDescriptor = mergeDocuments(mainDescriptor, supplementalDocs);
        descriptorReader = new PluginDescriptorReader(mainDescriptor, checkNotNull(applications));
    }

    protected static Document createDocument(final InputStream source) throws PluginParseException {
        final SAXReader reader = new SAXReader();
        reader.setMergeAdjacentText(true);
        try {
            return reader.read(source);
        } catch (final DocumentException e) {
            throw new PluginParseException("Cannot parse XML plugin descriptor", e);
        }
    }

    protected static Document mergeDocuments(final Document mainDocument, final Iterable<Document> supplementalDocuments) {
        Element mainRootElement = mainDocument.getRootElement();
        for (Document supplementalDocument : supplementalDocuments) {
            Element supplementaryRoot = supplementalDocument.getRootElement();
            for (Iterator<Node> iter = supplementaryRoot.content().iterator(); iter.hasNext(); ) {
                Node node = iter.next();
                iter.remove();
                mainRootElement.add(node);
            }
        }
        return mainDocument;
    }

    protected Document getDocument() {
        return descriptorReader.getDescriptor();
    }

    public Plugin configurePlugin(final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin) throws PluginParseException {
        plugin.setName(descriptorReader.getPluginName());
        plugin.setKey(getKey());
        plugin.setPluginsVersion(getPluginsVersion());
        plugin.setSystemPlugin(isSystemPlugin());
        plugin.setI18nNameKey(descriptorReader.getI18nPluginNameKey().getOrElse(plugin.getI18nNameKey()));

        if (plugin.getKey().indexOf(":") > 0) {
            throw new PluginParseException("Plugin keys cannot contain ':'. Key is '" + plugin.getKey() + "'");
        }

        plugin.setEnabledByDefault(descriptorReader.isEnabledByDefault());
        plugin.setResources(descriptorReader.getResources());
        plugin.setPluginInformation(createPluginInformation());

        for (Element module : descriptorReader.getModules(plugin.getInstallationMode())) {
            final ModuleDescriptor<?> moduleDescriptor = createModuleDescriptor(plugin, module, moduleDescriptorFactory);

            // If we're not loading the module descriptor, null is returned, so we skip it
            if (moduleDescriptor == null) {
                continue;
            }

            if (plugin.getModuleDescriptor(moduleDescriptor.getKey()) != null) {
                throw new PluginParseException("Found duplicate key '" + moduleDescriptor.getKey() + "' within plugin '" + plugin.getKey() + "'");
            }

            plugin.addModuleDescriptor(moduleDescriptor);

            // If we have any unloadable modules, also create an unloadable plugin, which will make it clear that there was a problem
            if (moduleDescriptor instanceof UnloadableModuleDescriptor) {
                log.error("There were errors loading the plugin '" + plugin.getName() + "'. The plugin has been disabled.");
                return UnloadablePluginFactory.createUnloadablePlugin(plugin);
            }
        }
        return plugin;
    }

    @Override
    public ModuleDescriptor<?> addModule(final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin, final Element module) {
        return XmlDescriptorParserUtils.addModule(moduleDescriptorFactory, plugin, module);
    }

    protected ModuleDescriptor<?> createModuleDescriptor(final Plugin plugin, final Element element, final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        final String name = element.getName();

        final ModuleDescriptor<?> moduleDescriptor = newModuleDescriptor(plugin, element, moduleDescriptorFactory);

        // When the module descriptor has been excluded, null is returned (PLUG-5)
        if (moduleDescriptor == null) {
            log.info("The module '{}' in plugin '{}' is in the list of excluded module descriptors, so not enabling.", name, plugin.getName());
            return null;
        }

        // Once we have the module descriptor, create it using the given information
        try {
            moduleDescriptor.init(plugin, element);
        }
        // If it fails, return a dummy module that contains the error
        catch (final Exception e) {
            final UnloadableModuleDescriptor descriptor = createUnloadableModuleDescriptor(plugin, element, e, moduleDescriptorFactory);

            log.error("There were problems loading the module '{}'. The module and its plugin have been disabled.", name);
            log.error(descriptor.getErrorText(), e);

            return descriptor;
        }

        return moduleDescriptor;
    }

    protected PluginInformation createPluginInformation() {
        final PluginInformationReader pluginInformationReader = descriptorReader.getPluginInformationReader();

        final PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setDescription(pluginInformationReader.getDescription().getOrElse(pluginInfo.getDescription()));
        pluginInfo.setDescriptionKey(pluginInformationReader.getDescriptionKey().getOrElse(pluginInfo.getDescriptionKey()));
        pluginInfo.setVersion(pluginInformationReader.getVersion().getOrElse(pluginInfo.getVersion()));
        pluginInfo.setVendorName(pluginInformationReader.getVendorName().getOrElse(pluginInfo.getVendorName()));
        pluginInfo.setVendorUrl(pluginInformationReader.getVendorUrl().getOrElse(pluginInfo.getVendorUrl()));
        for (Map.Entry<String, String> param : pluginInformationReader.getParameters().entrySet()) {
            pluginInfo.addParameter(param.getKey(), param.getValue());
        }
        pluginInfo.setMinVersion(pluginInformationReader.getMinVersion().getOrElse(pluginInfo.getMinVersion()));
        pluginInfo.setMaxVersion(pluginInformationReader.getMaxVersion().getOrElse(pluginInfo.getMaxVersion()));
        pluginInfo.setMinJavaVersion(pluginInformationReader.getMinJavaVersion().getOrElse(pluginInfo.getMinJavaVersion()));
        pluginInfo.setStartup(pluginInformationReader.getStartup().getOrElse(pluginInfo.getStartup()));
        pluginInfo.setModuleScanFolders(pluginInformationReader.getModuleScanFolders());

        final Map<String, Option<String>> readPermissions = pluginInformationReader.getPermissions();
        if (pluginInformationReader.hasAllPermissions()) {
            pluginInfo.setPermissions(ImmutableSet.of(PluginPermission.ALL));
        } else {
            final ImmutableSet.Builder<PluginPermission> permissions = ImmutableSet.builder();
            for (Map.Entry<String, Option<String>> permission : readPermissions.entrySet()) {
                final String permissionKey = permission.getKey();
                final Option<String> readInstallationMode = permission.getValue();
                final Option<InstallationMode> installationMode = InstallationMode.of(readInstallationMode.getOrNull());
                if (StringUtils.isNotBlank(readInstallationMode.getOrNull()) && !installationMode.isDefined()) {
                    log.warn("The parsed installation mode '{}' for permission '{}' didn't match any of the valid values: {}",
                            new Object[]{readInstallationMode, permission.getKey(),
                                    transform(copyOf(InstallationMode.values()), new Function<InstallationMode, String>() {
                                        @Override
                                        public String apply(InstallationMode im) {
                                            return im.getKey();
                                        }
                                    })});
                }

                permissions.add(new PluginPermission(permissionKey, installationMode));
            }
            pluginInfo.setPermissions(permissions.build());
        }

        return pluginInfo;
    }

    public String getKey() {
        return descriptorReader.getPluginKey();
    }

    public int getPluginsVersion() {
        return descriptorReader.getPluginsVersion();
    }

    public PluginInformation getPluginInformation() {
        return createPluginInformation();
    }

    public boolean isSystemPlugin() {
        return descriptorReader.isSystemPlugin();
    }
}
