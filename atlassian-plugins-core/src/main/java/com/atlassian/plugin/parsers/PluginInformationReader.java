package com.atlassian.plugin.parsers;


import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.Plugin;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.plugin.parsers.PluginDescriptorReader.elements;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.filter;

/**
 * Reads plugin information from a plugin descriptor.
 *
 * @see com.atlassian.plugin.parsers.PluginDescriptorReader#getPluginInformationReader()
 * @since 3.0.0
 */
public final class PluginInformationReader {
    static final String PLUGIN_INFO = "plugin-info";
    /**
     * The default scan-folder
     */
    static final String DEFAULT_SCAN_FOLDER = "META-INF/atlassian";

    private final Option<Element> pluginInfo;
    private final Set<Application> applications;
    private final int pluginsVersion;

    PluginInformationReader(Option<Element> pluginInfo, Set<Application> applications, int pluginsVersion) {
        this.pluginsVersion = pluginsVersion;
        this.pluginInfo = checkNotNull(pluginInfo);
        this.applications = copyOf(checkNotNull(applications));
    }

    public Option<String> getDescription() {
        return getDescriptionElement().map(new Function<Element, String>() {
            @Override
            public String apply(Element description) {
                return description.getTextTrim();
            }
        });
    }

    public Option<String> getDescriptionKey() {
        return getDescriptionElement().flatMap(new Function<Element, Option<String>>() {
            @Override
            public Option<String> apply(Element description) {
                return option(description.attributeValue("key"));
            }
        });
    }

    public Option<String> getVersion() {
        return childElement("version").map(new Function<Element, String>() {
            @Override
            public String apply(Element version) {
                return version.getTextTrim();
            }
        });
    }

    private Option<Element> childElement(final String name) {
        return pluginInfo.flatMap(new ChildElementFunction(name));
    }

    private Option<Iterable<Element>> childElements(final String name) {
        return pluginInfo.map(new ChildElementsFunction(name));
    }

    public Option<String> getVendorName() {
        return getVendorElement().flatMap(new Function<Element, Option<String>>() {
            @Override
            public Option<String> apply(Element vendor) {
                return option(vendor.attributeValue("name"));
            }
        });
    }

    public Option<String> getVendorUrl() {
        return getVendorElement().flatMap(new Function<Element, Option<String>>() {
            @Override
            public Option<String> apply(Element vendor) {
                return option(vendor.attributeValue("url"));
            }
        });
    }

    public Map<String, String> getParameters() {
        final ImmutableMap.Builder<String, String> params = ImmutableMap.builder();
        for (Element param : getParamElements()) {
            params.put(param.attribute("name").getData().toString(), param.getText());
        }
        return params.build();
    }

    public Option<Float> getMinVersion() {
        return getApplicationVersionElement().flatMap(new GetAttributeFunction("min")).map(new ParseAttributeValueAsFloatFunction());
    }

    public Option<Float> getMaxVersion() {
        return getApplicationVersionElement().flatMap(new GetAttributeFunction("max")).map(new ParseAttributeValueAsFloatFunction());
    }

    public Option<Float> getMinJavaVersion() {
        return childElement("java-version").flatMap(new GetAttributeFunction("min")).map(new ParseAttributeValueAsFloatFunction());
    }

    public Map<String, Option<String>> getPermissions() {
        final ImmutableMap.Builder<String, Option<String>> permissions = ImmutableMap.builder();
        for (Element permission : getPermissionElements()) {
            permissions.put(permission.getTextTrim(), option(permission.attributeValue("installation-mode")));
        }
        return permissions.build();
    }

    public boolean hasAllPermissions() {
        return getPermissions().isEmpty() && pluginsVersion < Plugin.VERSION_3;
    }

    public Set<String> getPermissions(final InstallationMode installationMode) {
        return copyOf(Maps.filterValues(getPermissions(), new Predicate<Option<String>>() {
            @Override
            public boolean apply(Option<String> input) {
                return input
                        .flatMap(new Function<String, Option<InstallationMode>>() {
                            @Override
                            public Option<InstallationMode> apply(String mode) {
                                return InstallationMode.of(mode);
                            }
                        })
                        .fold(Suppliers.alwaysTrue(), new Function<InstallationMode, Boolean>() {
                            @Override
                            public Boolean apply(InstallationMode input) {
                                return input.equals(installationMode);
                            }
                        });
            }
        }).keySet());
    }

    public Option<String> getStartup() {
        return childElement("startup").map(new Function<Element, String>() {
            @Override
            public String apply(final Element element) {
                return element.getTextTrim();
            }
        });
    }

    public Iterable<String> getModuleScanFolders() {
        final Set<String> scanFolders = Sets.newLinkedHashSet();
        return childElement("scan-modules")
                .map(new Function<Element, Iterable<Element>>() {
                    public Iterable<Element> apply(@Nullable Element scanModules) {
                        List<Element> elements = elements(scanModules, "folder");
                        if (elements.isEmpty()) {
                            scanFolders.add(DEFAULT_SCAN_FOLDER);
                        }
                        return elements;
                    }

                })
                .map(new Function<Iterable<Element>, Iterable<String>>() {
                    @Override
                    public Iterable<String> apply(@Nullable Iterable<Element> folders) {
                        for (Element folder : folders) {
                            scanFolders.add(folder.getTextTrim());
                        }
                        return scanFolders;
                    }
                })
                .getOrElse(ImmutableSet.<String>of());
    }

    private Iterable<Element> getPermissionElements() {
        return childElement("permissions")
                .map(new Function<Element, Iterable<Element>>() {
                    @Override
                    public Iterable<Element> apply(Element permissions) {
                        return elements(permissions, "permission");
                    }
                })
                .map(new Function<Iterable<Element>, Iterable<Element>>() {
                    @Override
                    public Iterable<Element> apply(@Nullable Iterable<Element> input) {
                        return filter(input, new ElementWithForApplicationsPredicate(applications));
                    }
                })
                .map(new Function<Iterable<Element>, Iterable<Element>>() {
                    @Override
                    public Iterable<Element> apply(@Nullable Iterable<Element> input) {
                        return filter(input, new Predicate<Element>() {
                            @Override
                            public boolean apply(Element p) {
                                return StringUtils.isNotBlank(p.getTextTrim());
                            }
                        });
                    }
                })
                .getOrElse(ImmutableList.<Element>of());
    }

    private Option<Element> getApplicationVersionElement() {
        return childElement("application-version");
    }

    private Iterable<Element> getParamElements() {
        return childElements("param")
                .map(new Function<Iterable<Element>, Iterable<Element>>() {
                    @Override
                    public Iterable<Element> apply(Iterable<Element> input) {
                        return filter(input, new Predicate<Element>() {
                            @Override
                            public boolean apply(Element param) {
                                return param.attribute("name") != null;
                            }
                        });
                    }
                })
                .getOrElse(ImmutableList.<Element>of());
    }

    private Option<Element> getVendorElement() {
        return childElement("vendor");
    }

    private Option<Element> getDescriptionElement() {
        return childElement("description");
    }

    private static final class ParseAttributeValueAsFloatFunction implements Function<Attribute, Float> {
        @Override
        public Float apply(Attribute attr) {
            return Float.parseFloat(attr.getValue());
        }
    }

    private static final class GetAttributeFunction implements Function<Element, Option<Attribute>> {
        private final String name;

        private GetAttributeFunction(String name) {
            this.name = name;
        }

        @Override
        public Option<Attribute> apply(Element applicationVersion) {
            return option(applicationVersion.attribute(name));
        }
    }

    private static final class ApplicationWithNamePredicate implements Predicate<Application> {
        @Nullable
        private final String name;

        public ApplicationWithNamePredicate(String name) {
            this.name = name;
        }

        @Override
        public boolean apply(Application app) {
            return app.getKey().equals(name); // name might be null
        }
    }

    private static final class ElementWithForApplicationsPredicate implements Predicate<Element> {
        private final Set<Application> applications;

        private ElementWithForApplicationsPredicate(Set<Application> applications) {
            this.applications = checkNotNull(applications);
        }

        @Override
        public boolean apply(final Element el) {
            final String appName = el.attributeValue("application");
            return appName == null || Iterables.any(applications, new ApplicationWithNamePredicate(appName));
        }
    }

    private static final class ChildElementFunction implements Function<Element, Option<Element>> {
        private final String name;

        public ChildElementFunction(String name) {
            this.name = name;
        }

        @Override
        public Option<Element> apply(Element el) {
            return option(el.element(name));
        }
    }

    private static final class ChildElementsFunction implements Function<Element, Iterable<Element>> {
        private final String name;

        public ChildElementsFunction(String name) {
            this.name = name;
        }

        @Override
        public Iterable<Element> apply(Element el) {
            return elements(el, name);
        }
    }
}
