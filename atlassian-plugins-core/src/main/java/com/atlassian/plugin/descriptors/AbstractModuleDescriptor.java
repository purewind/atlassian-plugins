package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModulePermissionException;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.loaders.LoaderUtils;
import com.atlassian.plugin.module.LegacyModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.util.ClassUtils;
import com.atlassian.plugin.util.JavaVersionUtils;
import com.atlassian.plugin.util.validation.ValidationPattern;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.util.Assertions.notNull;
import static com.atlassian.plugin.util.validation.ValidationPattern.createPattern;
import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static com.google.common.base.Preconditions.checkNotNull;

public abstract class AbstractModuleDescriptor<T> implements ModuleDescriptor<T>, StateAware {
    private static final Logger log = LoggerFactory.getLogger(AbstractModuleDescriptor.class);

    protected Plugin plugin;
    protected String key;
    protected String name;
    protected String moduleClassName;
    protected Class<T> moduleClass;
    private String description;
    private boolean enabledByDefault = true;
    private boolean systemModule = false;

    /**
     * @deprecated since 2.2.0
     */
    @Deprecated
    protected boolean singleton = true;
    private Map<String, String> params;
    protected Resources resources = Resources.EMPTY_RESOURCES;
    private Float minJavaVersion;
    private String i18nNameKey;
    private String descriptionKey;
    private String completeKey;
    private boolean enabled = false;
    protected final ModuleFactory moduleFactory;
    private boolean broken = false;

    public AbstractModuleDescriptor(final ModuleFactory moduleFactory) {
        this.moduleFactory = checkNotNull(moduleFactory, "Module creator factory cannot be null");
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException {
        validate(element);

        this.plugin = notNull("plugin", plugin);
        this.key = element.attributeValue("key");
        this.name = element.attributeValue("name");
        this.i18nNameKey = element.attributeValue("i18n-name-key");
        this.completeKey = buildCompleteKey(plugin, key);
        this.description = element.elementTextTrim("description");
        this.moduleClassName = element.attributeValue("class");
        final Element descriptionElement = element.element("description");
        this.descriptionKey = (descriptionElement != null) ? descriptionElement.attributeValue("key") : null;
        params = LoaderUtils.getParams(element);

        if ("disabled".equalsIgnoreCase(element.attributeValue("state"))) {
            enabledByDefault = false;
        }

        if ("true".equalsIgnoreCase(element.attributeValue("system"))) {
            systemModule = true;
        }

        if (element.element("java-version") != null) {
            minJavaVersion = Float.valueOf(element.element("java-version").attributeValue("min"));
        }

        if ("false".equalsIgnoreCase(element.attributeValue("singleton"))) {
            singleton = false;
        } else if ("true".equalsIgnoreCase(element.attributeValue("singleton"))) {
            singleton = true;
        } else {
            singleton = isSingletonByDefault();
        }

        resources = Resources.fromXml(element);
    }

    /**
     * This is a method that module should call (in their {@link #init(Plugin, Element) init method implementation} to
     * check that permissions are correctly set on the plugin. Use the {@link RequirePermission} annotation to add
     * permissions to a module, and/or {@link #getRequiredPermissions} if you need more dynamic permissions.
     *
     * If either all permissions are allowed or this is a system module, we don't bother to check permissions
     *
     * @throws ModulePermissionException if the plugin doesn't require the necessary permissions for the module to work.
     * @see RequirePermission
     * @see #getRequiredPermissions()
     * @since 3.0
     */
    protected final void checkPermissions() throws ModulePermissionException {
        if (plugin.hasAllPermissions() || isSystemModule()) {
            return;
        }

        final Sets.SetView<String> difference = Sets.difference(getAllRequiredPermissions(), plugin.getActivePermissions());
        if (!difference.isEmpty()) {
            throw new ModulePermissionException(getCompleteKey(), difference.immutableCopy());
        }
    }

    private Set<String> getAllRequiredPermissions() {
        final ImmutableSet.Builder<String> permissions = ImmutableSet.builder();
        permissions.addAll(Permissions.getRequiredPermissions(this.getClass()));
        permissions.addAll(getRequiredPermissions());
        return permissions.build();
    }

    /**
     * <p>The set of additionally required permissions. This is useful for dynamic permissions depending on module
     * configuration. One might prefer the {@link RequirePermission} annotation otherwise for 'static' permissions.
     * <p>Default implementation returns an empty set
     *
     * @return the set of additionally required permissions.
     * @see RequirePermission
     * @see #checkPermissions()
     * @since 3.0
     */
    protected Set<String> getRequiredPermissions() {
        return ImmutableSet.of();
    }

    /**
     * Validates the element, collecting the rules from
     * {@link #provideValidationRules(ValidationPattern)}
     *
     * @param element The configuration element
     * @since 2.2.0
     */
    private void validate(final Element element) {
        notNull("element", element);
        final ValidationPattern pattern = createPattern();
        provideValidationRules(pattern);
        pattern.evaluate(element);
    }

    /**
     * Provides validation rules for the pattern
     *
     * @param pattern The validation pattern
     * @since 2.2.0
     */
    protected void provideValidationRules(final ValidationPattern pattern) {
        pattern.rule(test("@key").withError("The key is required"));
    }

    /**
     * Override this for module descriptors which don't expect to be able to
     * load a class successfully
     *
     * @param plugin
     * @param element
     * @deprecated Since 2.1.0, use {@link #loadClass(Plugin, String)} instead
     */
    @Deprecated
    protected void loadClass(final Plugin plugin, final Element element) throws PluginParseException {
        loadClass(plugin, element.attributeValue("class"));
    }

    /**
     * Loads the module class that this descriptor provides, and will not
     * necessarily be the implementation class. Override this for module
     * descriptors whose type cannot be determined via generics.
     *
     * @param clazz The module class name to load
     * @throws IllegalStateException If the module class cannot be determined
     *                               and the descriptor doesn't define a module type via generics
     */
    protected void loadClass(final Plugin plugin, final String clazz) throws PluginParseException {
        if (moduleClassName != null) {
            if (moduleFactory instanceof LegacyModuleFactory) // not all plugins have to have a class
            {
                moduleClass = ((LegacyModuleFactory) moduleFactory).getModuleClass(moduleClassName, this);
            }

            // This is only here for backwards compatibility with old code that
            // uses {@link
            // com.atlassian.plugin.PluginAccessor#getEnabledModulesByClass(Class)}
            else if (moduleFactory instanceof PrefixDelegatingModuleFactory) {
                moduleClass = ((PrefixDelegatingModuleFactory) moduleFactory).guessModuleClass(moduleClassName, this);
            }
        }
        // If this module has no class, then we assume Void
        else {
            moduleClass = (Class<T>) Void.class;
        }

        // Usually is null when a prefix is used in the class name
        if (moduleClass == null) {
            try {

                Class moduleTypeClass = null;
                try {
                    moduleTypeClass = ClassUtils.getTypeArguments(AbstractModuleDescriptor.class, getClass()).get(0);
                } catch (RuntimeException ex) {
                    log.debug("Unable to get generic type, usually due to Class.forName() problems", ex);
                    moduleTypeClass = getModuleReturnClass();
                }
                moduleClass = moduleTypeClass;
            } catch (final ClassCastException ex) {
                throw new IllegalStateException("The module class must be defined in a concrete instance of " + "ModuleDescriptor and not as another generic type.");
            }

            if (moduleClass == null) {
                throw new IllegalStateException("The module class cannot be determined, likely because it needs a concrete "
                        + "module type defined in the generic type it passes to AbstractModuleDescriptor");
            }
        }
    }

    Class<?> getModuleReturnClass() {
        try {
            return getClass().getMethod("getModule").getReturnType();
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("The getModule() method is missing (!) on " + getClass());
        }
    }

    /**
     * Build the complete key based on the provided plugin and module key. This
     * method has no side effects.
     *
     * @param plugin    The plugin for which the module belongs
     * @param moduleKey The key for the module
     * @return A newly constructed complete key, null if the plugin is null
     */
    private String buildCompleteKey(final Plugin plugin, final String moduleKey) {
        if (plugin == null) {
            return null;
        }

        final StringBuffer completeKeyBuffer = new StringBuffer(32);
        completeKeyBuffer.append(plugin.getKey()).append(":").append(moduleKey);
        return completeKeyBuffer.toString();
    }

    /**
     * The default implementation disables the module if it's still enabled, and unreference the plugin.
     * Override this if your plugin needs to clean up when it's been removed,
     * and call super.destroy(Plugin) before returning.
     */
    @Override
    public void destroy() {
        destroy(plugin);
    }

    @Deprecated
    @Override
    public void destroy(final Plugin plugin) {
        if (enabled) {
            this.disabled();
        }
    }

    @Override
    public boolean isEnabledByDefault() {
        return enabledByDefault && satisfiesMinJavaVersion();
    }

    @Override
    public boolean isSystemModule() {
        return systemModule;
    }

    /**
     * @deprecated Since 2.2.0
     */
    @Deprecated
    public boolean isSingleton() {
        return singleton;
    }

    /**
     * @deprecated Since 2.2.0
     */
    @Deprecated
    protected boolean isSingletonByDefault() {
        return true;
    }

    /**
     * Check that the module class of this descriptor implements a given
     * interface, or extends a given class.
     *
     * @param requiredModuleClazz The class this module's class must implement
     *                            or extend.
     * @throws PluginParseException If the module class does not implement or
     *                              extend the given class.
     */
    final protected void assertModuleClassImplements(final Class<T> requiredModuleClazz) throws PluginParseException {
        if (!enabled) {
            throw new PluginParseException("Plugin module " + getKey() + " not enabled");
        }
        if (!requiredModuleClazz.isAssignableFrom(getModuleClass())) {
            throw new PluginParseException("Given module class: " + getModuleClass().getName() + " does not implement " + requiredModuleClazz.getName());
        }
    }

    @Override
    public String getCompleteKey() {
        return completeKey;
    }

    @Override
    public String getPluginKey() {
        return getPlugin().getKey();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<T> getModuleClass() {
        return moduleClass;
    }

    @Override
    public abstract T getModule();

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public String getI18nNameKey() {
        return i18nNameKey;
    }

    @Override
    public String getDescriptionKey() {
        return descriptionKey;
    }

    @Override
    public List<ResourceDescriptor> getResourceDescriptors() {
        return resources.getResourceDescriptors();
    }

    @Override
    public List<ResourceDescriptor> getResourceDescriptors(final String type) {
        return resources.getResourceDescriptors(type);
    }

    @Override
    public ResourceLocation getResourceLocation(final String type, final String name) {
        return resources.getResourceLocation(type, name);
    }

    @Override
    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        return resources.getResourceDescriptor(type, name);
    }

    @Override
    public Float getMinJavaVersion() {
        return minJavaVersion;
    }

    @Override
    public boolean satisfiesMinJavaVersion() {
        if (minJavaVersion != null) {
            return JavaVersionUtils.satisfiesMinVersion(minJavaVersion);
        }
        return true;
    }

    /**
     * Sets the plugin for the ModuleDescriptor
     *
     * @param plugin The plugin to set for this descriptor.
     */
    public void setPlugin(final Plugin plugin) {
        this.completeKey = buildCompleteKey(plugin, key);
        this.plugin = plugin;
    }

    /**
     * @return The plugin this module descriptor is associated with.
     */
    @Override
    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public boolean equals(Object obj) {
        return new ModuleDescriptors.EqualsBuilder().descriptor(this).isEqualTo(obj);
    }

    @Override
    public int hashCode() {
        return new ModuleDescriptors.HashCodeBuilder().descriptor(this).toHashCode();
    }

    @Override
    public String toString() {
        return getCompleteKey() + " (" + getDescription() + ")";
    }

    /**
     * Enables the descriptor by loading the module class. Classes overriding
     * this method MUST call super.enabled() before their own enabling code.
     *
     * @since 2.1.0
     */
    public void enabled() {
        loadClass(plugin, moduleClassName);
        enabled = true;
        broken = false;
    }

    /**
     * Disables the module descriptor. Classes overriding this method MUST call
     * super.disabled() after their own disabling code.
     */
    public void disabled() {
        enabled = false;
        moduleClass = null;
    }

    /**
     * @return The defined module class name
     * @since 3.0
     */
    protected String getModuleClassName() {
        return moduleClassName;
    }

    /**
     * Get whether this plugin module is enabled.
     *
     * @return If this module is enabled
     * @since 3.1
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setBroken() {
        broken = true;
    }

    @Override
    public boolean isBroken() {
        return broken;
    }
}
