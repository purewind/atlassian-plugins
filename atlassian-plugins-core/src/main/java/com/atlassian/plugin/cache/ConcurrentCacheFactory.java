package com.atlassian.plugin.cache;

import com.google.common.cache.LoadingCache;

/**
 * A generic factory for creating Guava {@link LoadingCache}s.  By allowing caches to be created by the host products, they are free to swap implementations and also instrument the
 * caches themselves so they can be fine tuned over time..
 *
 * @since 3.0.0
 */
public interface ConcurrentCacheFactory<K, V> {
    /**
     * This will be called to create the underlying cache
     *
     * @return a concurrent cache typically built via {@link com.google.common.cache.CacheBuilder}
     */
    LoadingCache<K, V> createCache();

}
