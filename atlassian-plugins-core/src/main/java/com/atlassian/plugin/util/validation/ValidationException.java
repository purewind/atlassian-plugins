package com.atlassian.plugin.util.validation;

import com.atlassian.plugin.PluginParseException;
import com.google.common.collect.ImmutableList;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Exception for a validation error parsing DOM4J nodes
 *
 * @since 2.2.0
 */
public class ValidationException extends PluginParseException {
    private final List<String> errors;

    public ValidationException(String msg, List<String> errors) {
        super(msg);
        this.errors = ImmutableList.copyOf(checkNotNull(errors));
    }

    /**
     * @return a list of the original errors
     */
    public List<String> getErrors() {
        return errors;
    }
}
