package com.atlassian.plugin.util.collect;

import com.atlassian.plugin.Plugin;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

public final class Transforms {
    private static final Function<Plugin, String> toPluginKeyFunction = new Function<Plugin, String>() {
        @Override
        public String apply(Plugin p) {
            return p.getKey();
        }
    };

    private Transforms() {
    }

    public static Iterable<String> toPluginKeys(Iterable<Plugin> plugins) {
        return Iterables.transform(plugins, toPluginKeyFunction);
    }
}
