package com.atlassian.plugin.instrumentation;

import com.atlassian.instrumentation.DefaultInstrumentRegistry;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.RegistryConfiguration;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.instrumentation.operations.SimpleOpTimerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Optional instrumentation provider for plugin system internals.
 * <p>
 * The instrumentation may be safely invoked in any plugin system code, however will no-op unless this provider is
 * enabled, which requires the following conditions to be met:
 * <ol>
 * <li>{@link InstrumentRegistry} is present in the class loader</li>
 * <li>The boolean system property <code>com.atlassian.plugin.instrumentation.PluginSystemInstrumentation.enabled</code> has been set</li>
 * </ol>
 * It is thus up to the application to provide the instrumentation classes and explicitly enable this instrumentation.
 * <p>
 * Note to maintainers: extreme care must be taken to ensure that instrumentation classes are not accessed at runtime if
 * they are not present.
 *
 * @since 4.1
 */
public class PluginSystemInstrumentation {
    private static final Logger log = LoggerFactory.getLogger(PluginSystemInstrumentation.class);

    public static final String INSTRUMENT_REGISTRY_CLASS = "com.atlassian.instrumentation.InstrumentRegistry";
    public static final String REGISTRY_NAME = "plugin.system";
    public static final File REGISTRY_HOME_DIRECTORY = new File(System.getProperty("java.io.tmpdir"));

    private static final String SINGLE_TIMER_NAME_FORMAT = "%s.%s"; // "<name>.<date>"
    private static final DateTimeFormatter timerDateFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    private final Optional<InstrumentRegistryProxy> instrumentRegistryProxy;

    private static PluginSystemInstrumentation instance;

    /**
     * Singleton accessor
     *
     * @return one and only instance
     */
    @Nonnull
    public static synchronized PluginSystemInstrumentation instance() {
        if (instance == null) {
            instance = new PluginSystemInstrumentation();
        }
        return instance;
    }

    /**
     * Name of the system property that enables instrumentation. Note that instrumentation defaults to "off".
     *
     * @return property name
     */
    @Nonnull
    public static String getEnabledProperty() {
        return PluginSystemInstrumentation.class.getName() + ".enabled";
    }

    /**
     * Private constructor for use by {#instance}
     */
    private PluginSystemInstrumentation() {

        // class must be present
        boolean instrumentationPresent;
        try {
            Class.forName(INSTRUMENT_REGISTRY_CLASS);
            instrumentationPresent = true;
        } catch (ClassNotFoundException e) {
            instrumentationPresent = false;
        }

        // system property to enable instrumentation must also be present
        if (instrumentationPresent && Boolean.getBoolean(getEnabledProperty())) {
            log.info("plugin system instrumentation ENABLED via system property '{}'", getEnabledProperty());
            instrumentRegistryProxy = Optional.of(new InstrumentRegistryProxy());
        } else {
            instrumentRegistryProxy = Optional.empty();
        }
    }

    /**
     * Retreive the instrument registry if instrumentation is enabled and present in the classloader.
     *
     * @return one and only registry
     */
    @Nonnull
    public Optional<InstrumentRegistry> getInstrumentRegistry() {
        return instrumentRegistryProxy
                .map(p -> Optional.of(p.getInstrumentRegistry()))
                .orElse(Optional.empty());
    }

    /**
     * Pull a timer from the instrument registry, if instrumentation is enabled and present in the classloader.
     * <p>
     * This timer records wall-clock time and CPU time on the thread it was instantiated from.
     * <p>
     * This should be used for a section of code that we wish to repeatedly measure and aggregate results for.
     * <p>
     * The timer is {@link java.io.Closeable} and is expected to be used as per the following example:
     * <pre>
     * try (Timer ignored = PluginSystemInstrumentation.instance().pullTimer("getEnabledModuleDescriptorsByClass")) {
     *     // block we wish to repeatedly measure
     * }
     * </pre>
     *
     * @param name that the timer will report
     * @return timer that may be empty - if no instrumentation it is still safe to close, which is a no-op
     * @see #pullSingleTimer(String)
     */
    @Nonnull
    public Timer pullTimer(@Nonnull final String name) {
        return new Timer(instrumentRegistryProxy
                .map(p -> Optional.of(p.pullTimer(checkNotNull(name))))
                .orElse(Optional.empty()));
    }

    /**
     * Pull a timer from the instrument registry, if instrumentation is enabled and present in the classloader.
     * <p>
     * This timer records wall-clock time and CPU time on the thread it was instantiated from.
     * <p>
     * This should be used for a section of code that we wish to measure once - the date and time appended to its name.
     * <p>
     * The timer is {@link java.io.Closeable} and is expected to be used as per the following example:
     * <pre>
     * try (Timer ignored = PluginSystemInstrumentation.instance().pullSingleTimer("earlyStartup")) {
     *     // block we wish to measure once, with a unique name
     * }
     * </pre>
     *
     * @param name that the timer will report
     * @return timer that may be empty - if no instrumentation it is still safe to close, which is a no-op
     * @see #pullTimer(String)
     */
    @Nonnull
    public Timer pullSingleTimer(@Nonnull final String name) {
        return new Timer(instrumentRegistryProxy
                .map(p -> Optional.of(p.pullTimer(formatSingleName(checkNotNull(name)))))
                .orElse(Optional.empty()));
    }

    /**
     * Format a user supplied name by appending the date.
     *
     * @param name user supplied
     * @return formatted name, with date appended after a single "."
     */
    @Nonnull
    private String formatSingleName(@Nonnull final String name) {
        return String.format(SINGLE_TIMER_NAME_FORMAT, checkNotNull(name), timerDateFormatter.format(LocalDateTime.now()));
    }

    /**
     * Proxy to wrap up the actual {@link InstrumentRegistry}.
     * <p>
     * It is necessary as this may not be present in the class loader, hence we only want to reference it if we've
     * already checked for its presence.
     */
    private class InstrumentRegistryProxy {
        final InstrumentRegistry instrumentRegistry = new DefaultInstrumentRegistry(
                new SimpleOpTimerFactory(),
                new RegistryConfiguration() {
                    @Override
                    public String getRegistryName() {
                        return REGISTRY_NAME;
                    }

                    @Override
                    public boolean isCPUCostCollected() {
                        return true;
                    }

                    @Override
                    public File getRegistryHomeDirectory() {
                        return REGISTRY_HOME_DIRECTORY;
                    }
                });

        @Nonnull
        InstrumentRegistry getInstrumentRegistry() {
            return instrumentRegistry;
        }

        @Nonnull
        OpTimer pullTimer(@Nonnull final String name) {
            return instrumentRegistry.pullTimer(checkNotNull(name));
        }
    }
}
