package com.atlassian.plugin.factories;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;
import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.io.IOUtils.closeQuietly;

/**
 * @since 3.0
 */
public abstract class AbstractPluginFactory implements PluginFactory {
    private static final Logger log = LoggerFactory.getLogger(AbstractPluginFactory.class);

    protected final DescriptorParserFactory descriptorParserFactory;
    protected final Set<Application> applications;

    protected AbstractPluginFactory(DescriptorParserFactory descriptorParserFactory, Set<Application> applications) {
        this.descriptorParserFactory = checkNotNull(descriptorParserFactory);
        this.applications = checkNotNull(applications);
    }

    /**
     * Determines if this deployer can handle this artifact by looking for the plugin descriptor
     *
     * @param pluginArtifact The artifact to test
     * @return The plugin key, null if it cannot load the plugin
     * @throws com.atlassian.plugin.PluginParseException If there are exceptions parsing the plugin configuration
     */
    public String canCreate(PluginArtifact pluginArtifact) throws PluginParseException {
        return getPluginKeyFromDescriptor(pluginArtifact);
    }

    /**
     * @deprecated Since 2.2.0, use {@link #create(com.atlassian.plugin.PluginArtifact, com.atlassian.plugin.ModuleDescriptorFactory)} instead
     */
    @Deprecated
    public final Plugin create(DeploymentUnit deploymentUnit, ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        return create(new JarPluginArtifact(checkNotNull(deploymentUnit).getPath()), moduleDescriptorFactory);
    }

    protected final boolean hasDescriptor(PluginArtifact pluginArtifact) {
        InputStream descriptorStream = null;
        try {
            descriptorStream = getDescriptorInputStream(pluginArtifact);
            return descriptorStream != null;
        } finally {
            closeQuietly(descriptorStream);
        }
    }

    protected final String getPluginKeyFromDescriptor(PluginArtifact pluginArtifact) {
        String pluginKey = null;
        InputStream descriptorStream = null;
        try {
            descriptorStream = getDescriptorInputStream(pluginArtifact);
            if (descriptorStream != null) {
                final DescriptorParser descriptorParser = descriptorParserFactory.getInstance(descriptorStream, applications);
                if (isValidPluginsVersion().apply(descriptorParser.getPluginsVersion())) {
                    pluginKey = descriptorParser.getKey();
                }
            }
        } finally {
            closeQuietly(descriptorStream);
        }
        return pluginKey;
    }

    protected abstract InputStream getDescriptorInputStream(PluginArtifact pluginArtifact);

    protected abstract Predicate<Integer> isValidPluginsVersion();
}
