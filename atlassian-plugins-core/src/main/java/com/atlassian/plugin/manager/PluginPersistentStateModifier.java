package com.atlassian.plugin.manager;

import com.atlassian.fugue.Effect;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;

import java.util.Map;

class PluginPersistentStateModifier {
    private final PluginPersistentStateStore store;

    public PluginPersistentStateModifier(final PluginPersistentStateStore store) {
        this.store = store;
    }

    public PluginPersistentState getState() {
        return store.load();
    }

    /*
     * @deprecated since 4.0, to be removed in 5.0: do not use
     */
    @Deprecated
    PluginPersistentStateStore getStore() {
        return store;
    }

    public synchronized void apply(Effect<PluginPersistentState.Builder> effect) {
        PluginPersistentState.Builder builder = PluginPersistentState.Builder.create(store.load());
        effect.apply(builder);
        store.save(builder.toState());
    }

    public void setEnabled(final Plugin plugin, final boolean enabled) {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(PluginPersistentState.Builder builder) {
                builder.setEnabled(plugin, enabled);
            }
        });
    }

    public void disable(final Plugin plugin) {
        setEnabled(plugin, false);
    }

    public void enable(final Plugin plugin) {
        setEnabled(plugin, true);
    }

    public void setEnabled(final ModuleDescriptor<?> module, final boolean enabled) {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(PluginPersistentState.Builder builder) {
                builder.setEnabled(module, enabled);
            }
        });
    }

    public void disable(final ModuleDescriptor<?> module) {
        setEnabled(module, false);
    }

    public void enable(final ModuleDescriptor<?> module) {
        setEnabled(module, true);
    }

    public void clearPluginRestartState() {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(PluginPersistentState.Builder builder) {
                builder.clearPluginRestartState();
            }
        });
    }

    public void setPluginRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(PluginPersistentState.Builder builder) {
                builder.setPluginRestartState(pluginKey, pluginRestartState);
            }
        });
    }

    public void addState(final Map<String, Boolean> state) {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(PluginPersistentState.Builder builder) {
                builder.addState(state);
            }
        });
    }

    public void removeState(final Plugin plugin) {
        apply(new Effect<PluginPersistentState.Builder>() {
            @Override
            public void apply(final PluginPersistentState.Builder builder) {
                builder.removeState(plugin.getKey());
                for (final ModuleDescriptor<?> moduleDescriptor : plugin.getModuleDescriptors()) {
                    builder.removeState(moduleDescriptor.getCompleteKey());
                }
            }
        });
    }
}
