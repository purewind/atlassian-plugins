package com.atlassian.plugin.manager;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInstaller;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.RevertablePluginInstaller;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptor;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptorFactory;
import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.BeforePluginDisabledEvent;
import com.atlassian.plugin.event.events.BeforePluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginContainerUnavailableEvent;
import com.atlassian.plugin.event.events.PluginDependentsChangedEvent;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginDisablingEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginEnablingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkResumingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartingEvent;
import com.atlassian.plugin.event.events.PluginInstalledEvent;
import com.atlassian.plugin.event.events.PluginInstallingEvent;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnablingEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.plugin.event.events.PluginUninstallingEvent;
import com.atlassian.plugin.event.events.PluginUpgradedEvent;
import com.atlassian.plugin.event.events.PluginUpgradingEvent;
import com.atlassian.plugin.exception.NoOpPluginExceptionInterception;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.impl.UnloadablePluginFactory;
import com.atlassian.plugin.instrumentation.PluginSystemInstrumentation;
import com.atlassian.plugin.instrumentation.Timer;
import com.atlassian.plugin.loaders.DiscardablePluginLoader;
import com.atlassian.plugin.loaders.DynamicPluginLoader;
import com.atlassian.plugin.loaders.PermissionCheckingPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.metadata.ClasspathFilePluginMetadata;
import com.atlassian.plugin.metadata.DefaultRequiredPluginValidator;
import com.atlassian.plugin.metadata.RequiredPluginValidator;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.EnabledPluginPredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfClassPredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;
import com.atlassian.plugin.predicate.PluginPredicate;
import com.atlassian.plugin.util.PluginUtils;
import com.atlassian.plugin.util.VersionStringComparator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import org.apache.commons.lang.time.StopWatch;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;
import static com.atlassian.plugin.impl.AbstractPlugin.cleanVersionString;
import static com.atlassian.plugin.util.Assertions.notNull;
import static com.atlassian.plugin.util.collect.Transforms.toPluginKeys;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Maps.filterKeys;

/**
 * This implementation delegates the initiation and classloading of plugins to a
 * list of {@link com.atlassian.plugin.loaders.PluginLoader}s and records the
 * state of plugins in a
 * {@link com.atlassian.plugin.manager.PluginPersistentStateStore}.
 * <p>
 * This class is responsible for enabling and disabling plugins and plugin
 * modules and reflecting these state changes in the PluginPersistentStateStore.
 * <p>
 * An interesting quirk in the design is that
 * {@link #installPlugin(com.atlassian.plugin.PluginArtifact)} explicitly stores
 * the plugin via a {@link com.atlassian.plugin.PluginInstaller}, whereas
 * {@link #uninstall(Plugin)} relies on the underlying
 * {@link com.atlassian.plugin.loaders.PluginLoader} to remove the plugin if
 * necessary.
 */
public class DefaultPluginManager implements PluginController, PluginAccessor, SplitStartupPluginSystemLifecycle {
    private static final Logger log = LoggerFactory.getLogger(DefaultPluginManager.class);

    @Internal
    public static String getStartupOverrideFileProperty() {
        return DefaultPluginManager.class.getName() + ".startupOverrideFile";
    }

    @Internal
    public static String getLateStartupEnableRetryProperty() {
        return DefaultPluginManager.class.getName() + ".lateStartupEnableRetry";
    }

    @Internal
    public static String getMinimumPluginVersionsFileProperty() {
        return DefaultPluginManager.class.getName() + ".minimumPluginVersionsFile";
    }

    private final List<DiscardablePluginLoader> pluginLoaders;
    private final PluginPersistentStateModifier persistentStateModifier;
    private final ModuleDescriptorFactory moduleDescriptorFactory;
    private final PluginEventManager pluginEventManager;

    private final PluginRegistry.ReadWrite pluginRegistry;
    private final PluginsClassLoader classLoader;
    private final PluginEnabler pluginEnabler;
    private final StateTracker tracker;

    private final boolean verifyRequiredPlugins;

    /**
     * Predicate for identify plugins which are not loaded at initialization.
     * <p>
     * This can be used to support two-phase "tenant aware" startup.
     */
    private final PluginPredicate delayLoadOf;

    /**
     * Installer used for storing plugins. Used by
     * {@link #installPlugin(PluginArtifact)}.
     */
    private RevertablePluginInstaller pluginInstaller;

    /**
     * Stores {@link Plugin}s as a key and {@link PluginLoader} as a value.
     */
    private final Map<Plugin, PluginLoader> installedPluginsToPluginLoader;

    /**
     * A map to pass information from {@link #earlyStartup} to {@link #addPlugins} without an API change.
     * <p>
     * This map allows earlyStartup to specify a plugin-specific loader for the list of plugins provided to addPlugins. It is only
     * valid when addPlugins is called from earlyStartup or lateStartup. Products may override addPlugins (to add clustering
     * behaviour for example), so fixing the API here is a more involved change.
     */
    private final Map<Plugin, DiscardablePluginLoader> candidatePluginsToPluginLoader;

    /**
     * A list of plugins to be re-enabled when adding plugins.
     * <p>
     * As with {@link #candidatePluginsToPluginLoader}, this is passing data from {@link #earlyStartup} and {@link #lateStartup} to
     * {@link #addPlugins} without an API change.
     */
    private final Collection<Plugin> additionalPluginsToEnable;

    private final DefaultPluginManagerJmxBridge defaultPluginManagerJmxBridge;

    /**
     * The list of plugins whose load was delayed.
     */
    private final List<Plugin> delayedPlugins;

    /**
     * A map of plugins which need to be removed during lateStartup.
     * <p>
     * The plugins which need to be removed on restart are discovered during earlyStartup so we can avoid installing them
     * when not required, but cannot be removed until persistence is available during late startup.
     */
    private final Map<Plugin, DiscardablePluginLoader> delayedPluginRemovalsToLoader;

    private final SafeModuleExtractor safeModuleExtractor;

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final boolean verifyRequiredPlugins) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withVerifyRequiredPlugins(verifyRequiredPlugins));
    }

    @ExperimentalApi
    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginPredicate delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withDelayLoadOf(delayLoadOf));
    }

    @ExperimentalApi
    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final PluginPredicate delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withDelayLoadOf(delayLoadOf));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final boolean verifyRequiredPlugins) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withVerifyRequiredPlugins(verifyRequiredPlugins));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final boolean verifyRequiredPlugins,
            final PluginPredicate delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withVerifyRequiredPlugins(verifyRequiredPlugins)
                .withDelayLoadOf(delayLoadOf));
    }

    protected DefaultPluginManager(Builder<? extends Builder> builder) {
        this.pluginLoaders = toPermissionCheckingPluginLoaders(notNull("Plugin Loaders list", builder.pluginLoaders));
        this.persistentStateModifier = new PluginPersistentStateModifier(notNull("PluginPersistentStateStore", builder.store));
        this.moduleDescriptorFactory = notNull("ModuleDescriptorFactory", builder.moduleDescriptorFactory);
        this.pluginEventManager = notNull("PluginEventManager", builder.pluginEventManager);
        this.pluginEnabler = new PluginEnabler(this, this, notNull("PluginExceptionInterception", builder.pluginExceptionInterception));
        this.verifyRequiredPlugins = builder.verifyRequiredPlugins;
        this.delayLoadOf = wrapDelayPredicateWithOverrides(builder.delayLoadOf);
        this.pluginRegistry = builder.pluginRegistry;
        this.classLoader = builder.pluginAccessor.map(pa -> PluginsClassLoader.class.cast(pa.getClassLoader()))
                .orElse(new PluginsClassLoader(null, this, this.pluginEventManager));

        this.tracker = new StateTracker();
        this.pluginInstaller = new NoOpRevertablePluginInstaller(new UnsupportedPluginInstaller());
        this.installedPluginsToPluginLoader = new HashMap<Plugin, PluginLoader>();
        this.candidatePluginsToPluginLoader = new HashMap<Plugin, DiscardablePluginLoader>();
        this.additionalPluginsToEnable = new ArrayList<Plugin>();
        this.delayedPlugins = new ArrayList<Plugin>();
        this.delayedPluginRemovalsToLoader = new HashMap<Plugin, DiscardablePluginLoader>();

        this.pluginEventManager.register(this);
        this.defaultPluginManagerJmxBridge = new DefaultPluginManagerJmxBridge(this);
        this.safeModuleExtractor = new SafeModuleExtractor(this);
    }

    @SuppressWarnings("unchecked")
    public static class Builder<T extends Builder<?>> {
        private PluginPersistentStateStore store;
        private List<PluginLoader> pluginLoaders = new ArrayList<>();
        private ModuleDescriptorFactory moduleDescriptorFactory;
        private PluginEventManager pluginEventManager;
        private PluginExceptionInterception pluginExceptionInterception = NoOpPluginExceptionInterception.NOOP_INTERCEPTION;
        private boolean verifyRequiredPlugins = false;
        private PluginPredicate delayLoadOf = p -> false;
        private PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();
        private Optional<PluginAccessor> pluginAccessor = Optional.empty();

        public T withStore(final PluginPersistentStateStore store) {
            this.store = store;
            return (T) this;
        }

        public T withPluginLoaders(final List<PluginLoader> pluginLoaders) {
            this.pluginLoaders.addAll(pluginLoaders);
            return (T) this;
        }

        public T withPluginLoader(final PluginLoader pluginLoader) {
            this.pluginLoaders.add(pluginLoader);
            return (T) this;
        }

        public T withModuleDescriptorFactory(final ModuleDescriptorFactory moduleDescriptorFactory) {
            this.moduleDescriptorFactory = moduleDescriptorFactory;
            return (T) this;
        }

        public T withPluginEventManager(final PluginEventManager pluginEventManager) {
            this.pluginEventManager = pluginEventManager;
            return (T) this;
        }

        public T withPluginExceptionInterception(final PluginExceptionInterception pluginExceptionInterception) {
            this.pluginExceptionInterception = pluginExceptionInterception;
            return (T) this;
        }

        public T withVerifyRequiredPlugins(final boolean verifyRequiredPlugins) {
            this.verifyRequiredPlugins = verifyRequiredPlugins;
            return (T) this;
        }

        public T withDelayLoadOf(final PluginPredicate delayLoadOf) {
            this.delayLoadOf = delayLoadOf;
            return (T) this;
        }

        public T withPluginRegistry(final PluginRegistry.ReadWrite pluginRegistry) {
            this.pluginRegistry = pluginRegistry;
            return (T) this;
        }

        public T withPluginAccessor(PluginAccessor pluginAccessor) {
            this.pluginAccessor = Optional.of(pluginAccessor);
            return (T) this;
        }

        public DefaultPluginManager build() {
            return new DefaultPluginManager(this);
        }
    }

    public static Builder<? extends Builder<?>> newBuilder() {
        return new Builder<>();
    }

    private List<DiscardablePluginLoader> toPermissionCheckingPluginLoaders(final List<PluginLoader> fromIterable) {
        return Lists.newArrayList(Iterables.transform(fromIterable, new Function<PluginLoader, DiscardablePluginLoader>() {
            @Override
            public PermissionCheckingPluginLoader apply(final PluginLoader pluginLoader) {
                return new PermissionCheckingPluginLoader(pluginLoader);
            }
        }));
    }

    private PluginPredicate wrapDelayPredicateWithOverrides(final PluginPredicate pluginPredicate) {
        final Map<String, String> startupOverridesMap = parseFileNamedByPropertyAsMap(getStartupOverrideFileProperty());

        return new PluginPredicate() {
            @Override
            public boolean matches(final Plugin plugin) {
                final String pluginKey = plugin.getKey();

                // Check startup overrides for plugin startup declaration
                final String stringFromFile = startupOverridesMap.get(pluginKey);
                final Boolean parsedFromFile = parseStartupToDelay(stringFromFile, pluginKey, "override file");
                if (null != parsedFromFile) {
                    return parsedFromFile;
                }

                // Check PluginInformation for plugin startup declaration
                final PluginInformation pluginInformation = plugin.getPluginInformation();
                final String stringFromInformation = (null != pluginInformation) ? pluginInformation.getStartup() : null;
                final Boolean parsedFromInformation = parseStartupToDelay(stringFromInformation, pluginKey, "PluginInformation");
                if (null != parsedFromInformation) {
                    return parsedFromInformation;
                }

                // No plugin startup information, so use product supplied predicate
                return pluginPredicate.matches(plugin);
            }

            private Boolean parseStartupToDelay(final String startup, final String pluginKey, final String source) {
                if (null != startup) {
                    if ("early".equals(startup)) {
                        return false;
                    }
                    if ("late".equals(startup)) {
                        return true;
                    }

                    log.warn("Unknown startup '{}' for plugin '{}' from {}", startup, pluginKey, source);
                    // and fall through
                }

                return null;
            }

        };
    }

    private Map<String, String> parseFileNamedByPropertyAsMap(final String property) {
        final Properties properties = new Properties();
        final String fileName = System.getProperty(property);
        if (null != fileName) {
            try {
                properties.load(new FileInputStream(fileName));
            } catch (final IOException eio) {
                log.warn("Failed to load file named by property {}, that is '{}': {}",
                        property, fileName, eio);
            }
        }
        return Maps.fromProperties(properties);
    }

    public void init() throws PluginParseException, NotificationException {
        earlyStartup();
        lateStartup();
    }

    @ExperimentalApi
    public void earlyStartup() throws PluginParseException, NotificationException {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullSingleTimer("earlyStartup")) {
            log.info("Plugin system earlyStartup begun");
            tracker.setState(StateTracker.State.STARTING);

            defaultPluginManagerJmxBridge.register();
            broadcastIgnoreError(new PluginFrameworkStartingEvent(this, this));
            pluginInstaller.clearBackups();
            final PluginPersistentState pluginPersistentState = getState();

            final Multimap<String, Plugin> candidatePluginKeyToVersionedPlugins = TreeMultimap.create();
            for (final DiscardablePluginLoader loader : pluginLoaders) {
                if (loader == null) {
                    continue;
                }

                final Iterable<Plugin> possiblePluginsToLoad = loader.loadAllPlugins(moduleDescriptorFactory);

                if (log.isDebugEnabled()) {
                    log.debug("Found {} plugins to possibly load: {}",
                            Iterables.size(possiblePluginsToLoad), toPluginKeys(possiblePluginsToLoad));
                }

                for (final Plugin plugin : possiblePluginsToLoad) {
                    if (pluginPersistentState.getPluginRestartState(plugin.getKey()) == PluginRestartState.REMOVE) {
                        log.info("Plugin {} was marked to be removed on restart.  Removing now.", plugin);
                        // We need to remove the plugin and clear its state, but we don't want to do any persistence until
                        // late startup. We may as well delay PluginLoader#removePlugin also, as it doesn't hurt,
                        // and it makes fewer assumptions about how the product persists the plugins themselves.
                        delayedPluginRemovalsToLoader.put(plugin, loader);
                    } else {
                        // We need the loaders for installed plugins so we can issue removePlugin() when
                        // the plugin is unloaded. So anything we didn't remove above, we put into the
                        // candidatePluginsToPluginLoader map. All of these need either removePlugin()
                        // or discardPlugin() for resource management.
                        candidatePluginsToPluginLoader.put(plugin, loader);
                        candidatePluginKeyToVersionedPlugins.put(plugin.getKey(), plugin);
                    }
                }
            }
            final List<Plugin> pluginsToInstall = new ArrayList<Plugin>();
            for (final Collection<Plugin> plugins : candidatePluginKeyToVersionedPlugins.asMap().values()) {
                final Plugin plugin = Ordering.natural().max(plugins);
                if (plugins.size() > 1) {
                    log.debug("Plugin {} contained multiple versions. installing version {}.", plugin.getKey(), plugin.getPluginInformation().getVersion());
                }
                pluginsToInstall.add(plugin);
            }

            // Partition pluginsToInstall into immediatePlugins that we install now, and delayedPlugins
            // that we install when instructed by a call to lateStartup.
            // In later versions of Guava, ImmutableListMultimap.index is another way to slice this if
            // you convert the PluginPredicate to a function. Whether or this is cleaner is a bit moot
            // since we can't use it yet anyway.
            final List<Plugin> immediatePlugins = new ArrayList<Plugin>();
            for (final Plugin plugin : pluginsToInstall) {
                if (delayLoadOf.matches(plugin)) {
                    delayedPlugins.add(plugin);
                } else {
                    immediatePlugins.add(plugin);
                }
            }

            // Install the non-delayed plugins
            addPlugins(null, immediatePlugins);

            // For each immediatePlugins, addPlugins has either called removePlugin()/discardPlugin()
            // for its loader, or is tracking it in installedPluginsToPluginLoader for a removePlugin()
            // when it is uninstalled (either via upgrade or shutdown).
            for (final Plugin plugin : immediatePlugins) {
                candidatePluginsToPluginLoader.remove(plugin);
            }

            if (Boolean.getBoolean(getLateStartupEnableRetryProperty())) {
                for (final Plugin plugin : immediatePlugins) {
                    // For each plugin that didn't enable but should have, make a note so we can try them again later. It's a little
                    // bit vexing that we are checking the persistent state here again just after addPlugins did it, but refactoring this
                    // stuff is fraught with API danger, so this can wait. PLUG-1116 seems like a time this might be worth revisiting.
                    if ((PluginState.ENABLED != plugin.getPluginState()) && pluginPersistentState.isEnabled(plugin)) {
                        additionalPluginsToEnable.add(plugin);
                    }
                }
                if (!additionalPluginsToEnable.isEmpty()) {
                    // Let people know we're going to retry them, so there is information in the logs about this near the resolution errors
                    log.warn("Failed to enable some ({}) early plugins, will fallback during lateStartup. Plugins: {}",
                            additionalPluginsToEnable.size(), additionalPluginsToEnable);
                }
            }

            // We need to keep candidatePluginsToPluginLoader populated with the delayedPlugins so that
            // addPlugins can do the right thing when called by lateStartup. However, we want to
            // discardPlugin anything we don't need so that loaders can release resources. So move what
            // we need later from candidatePluginsToPluginLoader to delayedPluginsLoaders.
            final Map<Plugin, DiscardablePluginLoader> delayedPluginsLoaders = new HashMap<Plugin, DiscardablePluginLoader>();
            for (final Plugin plugin : delayedPlugins) {
                final DiscardablePluginLoader loader = candidatePluginsToPluginLoader.remove(plugin);
                delayedPluginsLoaders.put(plugin, loader);
            }
            // Now candidatePluginsToPluginLoader contains exactly Plugins returned by loadAllPlugins
            // for which we didn't removePlugin() above, didn't pass on addPlugins(), and won't handle
            // in loadDelayedPlugins. So loaders can release resources, we discardPlugin() these.
            for (final Map.Entry<Plugin, DiscardablePluginLoader> entry : candidatePluginsToPluginLoader.entrySet()) {
                final Plugin plugin = entry.getKey();
                final DiscardablePluginLoader loader = entry.getValue();
                loader.discardPlugin(plugin);
            }
            // Finally, make candidatePluginsToPluginLoader contain what loadDelayedPlugins needs.
            candidatePluginsToPluginLoader.clear();
            candidatePluginsToPluginLoader.putAll(delayedPluginsLoaders);
            tracker.setState(StateTracker.State.DELAYED);

            log.info("Plugin system earlyStartup ended");

            broadcastIgnoreError(new PluginFrameworkDelayedEvent(this, this));
        }
    }

    @ExperimentalApi
    public void lateStartup() throws PluginParseException, NotificationException {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullSingleTimer("lateStartup")) {
            log.info("Plugin system lateStartup begun");

            tracker.setState(StateTracker.State.RESUMING);
            broadcastIgnoreError(new PluginFrameworkResumingEvent(this, this));

            addPlugins(null, delayedPlugins);
            delayedPlugins.clear();
            candidatePluginsToPluginLoader.clear();

            persistentStateModifier.clearPluginRestartState();
            for (final Map.Entry<Plugin, DiscardablePluginLoader> entry : delayedPluginRemovalsToLoader.entrySet()) {
                final Plugin plugin = entry.getKey();
                final DiscardablePluginLoader loader = entry.getValue();
                // Remove the plugin from the loader, and discard saved state (see PLUG-13).
                loader.removePlugin(plugin);
                persistentStateModifier.removeState(plugin);
            }
            delayedPluginRemovalsToLoader.clear();

            log.info("Plugin system lateStartup ended");

            tracker.setState(StateTracker.State.STARTED);
            if (verifyRequiredPlugins) {
                validateRequiredPlugins();
            }
            broadcastIgnoreError(new PluginFrameworkStartedEvent(this, this));
        }
    }

    private void validateRequiredPlugins() throws PluginException {
        final RequiredPluginValidator validator = new DefaultRequiredPluginValidator(this, new ClasspathFilePluginMetadata());
        final Collection<String> errors = validator.validate();
        if (errors.size() > 0) {
            log.error("Unable to validate required plugins or modules - plugin system shutting down");
            log.error("Failures:");
            for (final String error : errors) {
                log.error("\t{}", error);
            }
            shutdown();
            throw new PluginException("Unable to validate required plugins or modules");
        }
    }

    /**
     * Fires the shutdown event
     *
     * @throws IllegalStateException if already shutdown or already in the
     *                               process of shutting down.
     * @since 2.0.0
     */
    public void shutdown() {
        tracker.setState(StateTracker.State.SHUTTING_DOWN);

        log.info("Preparing to shut down the plugin system");
        broadcastIgnoreError(new PluginFrameworkShuttingDownEvent(this, this));

        log.info("Shutting down the plugin system");
        broadcastIgnoreError(new PluginFrameworkShutdownEvent(this, this));

        pluginRegistry.clear();
        pluginEventManager.unregister(this);
        tracker.setState(StateTracker.State.SHUTDOWN);
        defaultPluginManagerJmxBridge.unregister();
    }

    public final void warmRestart() {
        tracker.setState(StateTracker.State.WARM_RESTARTING);
        log.info("Initiating a warm restart of the plugin system");
        broadcastIgnoreError(new PluginFrameworkWarmRestartingEvent(this, this));

        // Make sure we reload plugins in order
        final List<Plugin> restartedPlugins = new ArrayList<Plugin>();
        final List<PluginLoader> loaders = new ArrayList<PluginLoader>(pluginLoaders);
        Collections.reverse(loaders);
        for (final PluginLoader loader : pluginLoaders) {
            for (final Map.Entry<Plugin, PluginLoader> entry : installedPluginsToPluginLoader.entrySet()) {
                if (entry.getValue() == loader) {
                    final Plugin plugin = entry.getKey();
                    if (isPluginEnabled(plugin.getKey())) {
                        disablePluginModules(plugin);
                        restartedPlugins.add(plugin);
                    }
                }
            }
        }

        // then enable them in reverse order
        Collections.reverse(restartedPlugins);
        for (final Plugin plugin : restartedPlugins) {
            enableConfiguredPluginModules(plugin);
        }

        broadcastIgnoreError(new PluginFrameworkWarmRestartedEvent(this, this));
        tracker.setState(StateTracker.State.STARTED);
    }

    @PluginEventListener
    public void onPluginModuleAvailable(final PluginModuleAvailableEvent event) {
        enableConfiguredPluginModule(event.getModule().getPlugin(), event.getModule(), new HashSet<ModuleDescriptor<?>>());
    }

    @PluginEventListener
    public void onPluginModuleUnavailable(final PluginModuleUnavailableEvent event) {
        disablePluginModuleNoPersist(event.getModule());
    }

    @PluginEventListener
    public void onPluginContainerUnavailable(final PluginContainerUnavailableEvent event) {
        disablePluginWithoutPersisting(event.getPluginKey());
    }

    @PluginEventListener
    public void onPluginRefresh(final PluginRefreshedEvent event) {
        final Plugin plugin = event.getPlugin();

        disablePluginModules(plugin);

        // It would be nice to fire this earlier, but doing it earlier than the disable of the plugin modules seems too early.
        // We should probably hook methods on NonValidatingOsgiBundleXmlApplicationContext (such as prepareRefresh ?) to
        // move this and the disable earlier if it makes sense.
        broadcastIgnoreError(new PluginEnablingEvent(plugin));

        if (enableConfiguredPluginModules(plugin)) {
            broadcastIgnoreError(new PluginEnabledEvent(plugin));
        }
    }

    /**
     * Set the plugin installation strategy for this manager
     *
     * @param pluginInstaller the plugin installation strategy to use
     * @see PluginInstaller
     */
    public void setPluginInstaller(final PluginInstaller pluginInstaller) {
        if (pluginInstaller instanceof RevertablePluginInstaller) {
            this.pluginInstaller = (RevertablePluginInstaller) pluginInstaller;
        } else {
            this.pluginInstaller = new NoOpRevertablePluginInstaller(pluginInstaller);
        }
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected final PluginPersistentStateStore getStore() {
        return persistentStateModifier.getStore();
    }

    public String installPlugin(final PluginArtifact pluginArtifact) throws PluginParseException {
        final Set<String> keys = installPlugins(pluginArtifact);
        if ((keys != null) && (keys.size() == 1)) {
            return keys.iterator().next();
        } else {
            // should never happen
            throw new PluginParseException("Could not install plugin");
        }
    }

    public Set<String> installPlugins(final PluginArtifact... pluginArtifacts) throws PluginParseException {
        final Map<String, PluginArtifact> validatedArtifacts = new LinkedHashMap<String, PluginArtifact>();
        try {
            for (final PluginArtifact pluginArtifact : pluginArtifacts) {
                validatedArtifacts.put(validatePlugin(pluginArtifact), pluginArtifact);
            }
        } catch (final PluginParseException ex) {
            throw new PluginParseException("All plugins could not be validated", ex);
        }

        for (final Map.Entry<String, PluginArtifact> entry : validatedArtifacts.entrySet()) {
            pluginInstaller.installPlugin(entry.getKey(), entry.getValue());
        }

        scanForNewPlugins();
        return validatedArtifacts.keySet();
    }

    /**
     * Validate a plugin jar. Looks through all plugin loaders for ones that can
     * load the plugin and extract the plugin key as proof.
     *
     * @param pluginArtifact the jar file representing the plugin
     * @return The plugin key
     * @throws PluginParseException if the plugin cannot be parsed
     * @throws NullPointerException if <code>pluginJar</code> is null.
     */
    String validatePlugin(final PluginArtifact pluginArtifact) throws PluginParseException {
        boolean foundADynamicPluginLoader = false;
        for (final PluginLoader loader : pluginLoaders) {
            if (loader.isDynamicPluginLoader()) {
                foundADynamicPluginLoader = true;
                final String key = ((DynamicPluginLoader) loader).canLoad(pluginArtifact);
                if (key != null) {
                    return key;
                }
            }
        }

        if (!foundADynamicPluginLoader) {
            throw new IllegalStateException("Should be at least one DynamicPluginLoader in the plugin loader list");
        }
        throw new PluginParseException("Jar " + pluginArtifact.getName() + " is not a valid plugin!");
    }

    public int scanForNewPlugins() throws PluginParseException {
        final StateTracker.State state = tracker.get();
        checkState((StateTracker.State.RESUMING == state) || (StateTracker.State.STARTED == state),
                "Cannot scanForNewPlugins in state %s", state);

        int numberFound = 0;

        for (final PluginLoader loader : pluginLoaders) {
            if (loader != null) {
                if (loader.supportsAddition()) {
                    final List<Plugin> pluginsToAdd = new ArrayList<Plugin>();
                    for (Plugin plugin : loader.loadFoundPlugins(moduleDescriptorFactory)) {
                        final Plugin oldPlugin = pluginRegistry.get(plugin.getKey());
                        // Only actually install the plugin if its module
                        // descriptors support it. Otherwise, mark it as
                        // unloadable.
                        if (!(plugin instanceof UnloadablePlugin)) {
                            if (PluginUtils.doesPluginRequireRestart(plugin)) {
                                if (oldPlugin == null) {
                                    markPluginInstallThatRequiresRestart(plugin);

                                    final UnloadablePlugin unloadablePlugin = UnloadablePluginFactory.createUnloadablePlugin(plugin);
                                    unloadablePlugin.setErrorText("Plugin requires a restart of the application due " + "to the following modules: " + PluginUtils.getPluginModulesThatRequireRestart(plugin));
                                    plugin = unloadablePlugin;
                                } else {
                                    // If a plugin has been installed but is waiting for restart then we do not want to
                                    // put the plugin into the update state, we want to keep it in the install state.
                                    if (!PluginRestartState.INSTALL.equals(getPluginRestartState(plugin.getKey()))) {
                                        markPluginUpgradeThatRequiresRestart(plugin);
                                    }
                                    continue;
                                }
                            }
                            // If the new plugin does not require restart we need to check what the restart state of
                            // the old plugin was and act accordingly
                            else if (oldPlugin != null && PluginUtils.doesPluginRequireRestart(oldPlugin)) {
                                // If you have installed the plugin that requires restart and before restart you have
                                // reinstalled a version of that plugin that does not require restart then you should
                                // just go ahead and install that plugin. This means reverting the previous install
                                // and letting the plugin fall into the plugins to add list
                                if (PluginRestartState.INSTALL.equals(getPluginRestartState(oldPlugin.getKey()))) {
                                    revertRestartRequiredChange(oldPlugin.getKey());
                                } else {
                                    markPluginUpgradeThatRequiresRestart(plugin);
                                    continue;
                                }
                            }
                            pluginsToAdd.add(plugin);
                        }
                    }
                    addPlugins(loader, pluginsToAdd);
                    numberFound += pluginsToAdd.size();
                }
            }
        }
        return numberFound;
    }

    private void markPluginInstallThatRequiresRestart(final Plugin plugin) {
        log.info("Installed plugin '{}' requires a restart due to the following modules: {}", plugin, PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.INSTALL);
    }

    private void markPluginUpgradeThatRequiresRestart(final Plugin plugin) {
        log.info("Upgraded plugin '{}' requires a restart due to the following modules: {}", plugin, PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.UPGRADE);
    }

    private void markPluginUninstallThatRequiresRestart(final Plugin plugin) {
        log.info("Uninstalled plugin '{}' requires a restart due to the following modules: {}", plugin, PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.REMOVE);
    }

    private void updateRequiresRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        persistentStateModifier.setPluginRestartState(pluginKey, pluginRestartState);
        onUpdateRequiresRestartState(pluginKey, pluginRestartState);
    }

    @SuppressWarnings("UnusedParameters")
    protected void onUpdateRequiresRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        // nothing to do in this implementation
    }

    /**
     * Uninstalls the given plugin, emitting disabled and uninstalled events as it does so.
     *
     * @param plugin the plugin to uninstall.
     * @throws PluginException If the plugin or loader doesn't support uninstallation
     */
    public void uninstall(final Plugin plugin) throws PluginException {
        uninstallPlugins(ImmutableList.of(plugin));
    }

    @Override
    public void uninstallPlugins(Collection<Plugin> plugins) throws PluginException {
        Map<Boolean, Set<Plugin>> requireRestart = plugins.stream()
                .collect(Collectors.partitioningBy(PluginUtils::doesPluginRequireRestart, Collectors.toSet()));

        requireRestart.get(true).stream()
                .forEach(plugin -> {
                    ensurePluginAndLoaderSupportsUninstall(plugin);
                    markPluginUninstallThatRequiresRestart(plugin);
                });

        Set<Plugin> pluginsToDisable = requireRestart.get(false);

        if (!pluginsToDisable.isEmpty()) {
            final DependentPlugins disabledPlugins = disablePlugins(
                    pluginsToDisable.stream().map(Plugin::getKey).collect(Collectors.toList()),
                    ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC));

            pluginsToDisable.stream().forEach(p -> broadcastIgnoreError(new PluginUninstallingEvent(p)));

            pluginsToDisable.stream().forEach(this::uninstallNoEvent);

            pluginsToDisable.stream().forEach(p -> broadcastIgnoreError(new PluginUninstalledEvent(p)));

            reenableDependent(pluginsToDisable, disabledPlugins, PluginState.UNINSTALLED);
        }
    }

    /**
     * Preforms an uninstallation without broadcasting the uninstallation event.
     *
     * @param plugin The plugin to uninstall
     * @since 2.5.0
     */
    protected void uninstallNoEvent(final Plugin plugin) {
        unloadPlugin(plugin);

        // PLUG-13: Plugins should not save state across uninstalls.
        persistentStateModifier.removeState(plugin);
    }

    /**
     * @param pluginKey The plugin key to revert
     * @throws PluginException If the revert cannot be completed
     */
    public void revertRestartRequiredChange(final String pluginKey) throws PluginException {
        notNull("pluginKey", pluginKey);
        final PluginRestartState restartState = getState().getPluginRestartState(pluginKey);
        if (restartState == PluginRestartState.UPGRADE) {
            pluginInstaller.revertInstalledPlugin(pluginKey);
        } else if (restartState == PluginRestartState.INSTALL) {
            pluginInstaller.revertInstalledPlugin(pluginKey);
            pluginRegistry.remove(pluginKey);
        }
        updateRequiresRestartState(pluginKey, PluginRestartState.NONE);
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected void removeStateFromStore(final PluginPersistentStateStore stateStore, final Plugin plugin) {
        new PluginPersistentStateModifier(stateStore).removeState(plugin);
    }

    /**
     * Unload a plugin. Called when plugins are added locally, or remotely in a
     * clustered application.
     *
     * @param plugin the plugin to remove
     * @throws PluginException if the plugin cannot be uninstalled
     */
    protected void unloadPlugin(final Plugin plugin) throws PluginException {
        final PluginLoader loader = ensurePluginAndLoaderSupportsUninstall(plugin);

        if (isPluginEnabled(plugin.getKey())) {
            notifyPluginDisabled(plugin);
        }

        notifyUninstallPlugin(plugin);
        if (loader != null) {
            removePluginFromLoader(plugin);
        }

        pluginRegistry.remove(plugin.getKey());
    }

    private PluginLoader ensurePluginAndLoaderSupportsUninstall(final Plugin plugin) {
        if (!plugin.isUninstallable()) {
            throw new PluginException("Plugin is not uninstallable: " + plugin);
        }

        final PluginLoader loader = installedPluginsToPluginLoader.get(plugin);

        if ((loader != null) && !loader.supportsRemoval()) {
            throw new PluginException("Not uninstalling plugin - loader doesn't allow removal. Plugin: " + plugin);
        }
        return loader;
    }

    private void removePluginFromLoader(final Plugin plugin) throws PluginException {
        if (plugin.isUninstallable()) {
            final PluginLoader pluginLoader = installedPluginsToPluginLoader.get(plugin);
            pluginLoader.removePlugin(plugin);
        }

        installedPluginsToPluginLoader.remove(plugin);
    }

    protected void notifyUninstallPlugin(final Plugin plugin) {
        classLoader.notifyUninstallPlugin(plugin);

        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            descriptor.destroy();
        }
    }

    protected PluginPersistentState getState() {
        return persistentStateModifier.getState();
    }

    /**
     * @deprecated Since 2.0.2, use {@link #addPlugins} instead
     */
    @Deprecated
    protected void addPlugin(final PluginLoader loader, final Plugin plugin) throws PluginParseException {
        addPlugins(loader, Collections.singletonList(plugin));
    }

    /**
     * Update the local plugin state and enable state aware modules.
     * <p>
     * If there is an existing plugin with the same key, the version strings of
     * the existing plugin and the plugin provided to this method will be parsed
     * and compared. If the installed version is newer than the provided
     * version, it will not be changed. If the specified plugin's version is the
     * same or newer, the existing plugin state will be saved and the plugin
     * will be unloaded before the provided plugin is installed. If the existing
     * plugin cannot be unloaded a {@link PluginException} will be thrown.
     *
     * @param loader           the loader used to load this plugin. This should only be null when called
     *                         internally from init(), in which case the loader is looked up per plugin in
     *                         candidatePluginsToPluginLoader.
     * @param pluginsToInstall the plugins to add
     * @throws PluginParseException if the plugin cannot be parsed
     * @since 2.0.2
     */
    protected void addPlugins(@Nullable final PluginLoader loader, final Collection<Plugin> pluginsToInstall)
            throws PluginParseException {
        final List<Plugin> pluginsToEnable = new ArrayList<Plugin>();
        final Set<PluginDependentsChangedEvent> dependentsChangedEvents = new HashSet<>();
        final Map<String, String> minimumPluginVersions = parseFileNamedByPropertyAsMap(getMinimumPluginVersionsFileProperty());

        // Install plugins, looking for upgrades and duplicates
        for (final Plugin plugin : new TreeSet<Plugin>(pluginsToInstall)) {
            boolean pluginUpgraded = false;
            // testing to make sure plugin keys are unique
            final String pluginKey = plugin.getKey();
            final Plugin existingPlugin = pluginRegistry.get(pluginKey);
            if (!pluginVersionIsAcceptable(plugin, minimumPluginVersions)) {
                log.info("Unacceptable plugin {} found - version less than minimum '{}'",
                        plugin, minimumPluginVersions.get(pluginKey));
                // We're not going to install it, so notify loader to release resources
                discardPlugin(loader, plugin);

                // Don't install the unacceptable plugin
                continue;

            } else if (null == existingPlugin) {
                broadcastIgnoreError(new PluginInstallingEvent(plugin));
            } else if (plugin.compareTo(existingPlugin) >= 0) {
                // upgrading a plugin
                try {
                    // disable mandatory, optional and dynamic dependencies, enabling them afterwards
                    final DependentPlugins disabledPlugins = disablePlugins(ImmutableList.of(plugin.getKey()),
                            ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC));
                    pluginsToEnable.addAll(disabledPlugins.get());

                    if (!disabledPlugins.get().isEmpty()) {
                        log.info("Found mandatory, optional and dynamically dependent plugins to re-enable after plugin upgrade '{}': {}.  Enabling...",
                                plugin, toPluginKeys(disabledPlugins.get()));
                    }

                    // Fire upgrading event for outgoing plugin. We do this before updatePlugin which removes the old plugin,
                    // but after disabling dependent plugins. This latter ordering is a bit of a judgement call, but this
                    // gives the plugin as much freedom to move as possible (knowing that clients have been disabled).
                    broadcastIgnoreError(new PluginUpgradingEvent(existingPlugin));
                    updatePlugin(existingPlugin, plugin);
                    pluginsToEnable.remove(existingPlugin);
                    pluginUpgraded = true;

                    if (!disabledPlugins.get().isEmpty()) {
                        dependentsChangedEvents.add(new PluginDependentsChangedEvent(plugin, PluginState.INSTALLED, ImmutableSet.<Plugin>of(), disabledPlugins.get()));
                    }
                } catch (final PluginException e) {
                    throw new PluginParseException("Duplicate plugin found (installed version is the same or older) and"
                            + " could not be unloaded: '" + pluginKey + "'", e);
                }
            } else {
                // If we find an older plugin, don't error (PLUG-12) ...
                log.debug("Duplicate plugin found (installed version is newer): '{}'", pluginKey);
                // ... just discard the older (unused) plugin
                discardPlugin(loader, plugin);

                // Don't install the older plugin
                continue;
            }

            plugin.install();

            final boolean isPluginEnabled = getState().isEnabled(plugin);
            if (isPluginEnabled) {
                log.debug("Plugin '{}' is to be enabled.", pluginKey);
                pluginsToEnable.add(plugin);
            } else {
                if (plugin.isSystemPlugin()) {
                    log.warn("System Plugin '{}' is disabled.", pluginKey);
                } else {
                    log.debug("Plugin '{}' is disabled.", pluginKey);
                }
            }

            if (pluginUpgraded) {
                broadcastIgnoreError(new PluginUpgradedEvent(plugin));
            } else {
                broadcastIgnoreError(new PluginInstalledEvent(plugin));
            }

            pluginRegistry.put(plugin);
            if (loader == null) {
                installedPluginsToPluginLoader.put(plugin, candidatePluginsToPluginLoader.get(plugin));
            } else {
                installedPluginsToPluginLoader.put(plugin, loader);
            }
        }

        // Include any additionalPluginsToEnable which are hanging over from a previous enable attempt. This is a safety net
        // to workaround issues where early plugins are failing to enabled because of newly added dependencies on late plugins.
        // The code in question is broken, and we warn below, but this is causing enough pain we're special casing for now.
        pluginsToEnable.addAll(additionalPluginsToEnable);

        // bounce mandatory, optional and dynamic dependencies
        enableDependentPlugins(pluginsToEnable);

        // broadcast dependency changes following an upgrade
        for (PluginDependentsChangedEvent event : dependentsChangedEvents) {
            broadcastIgnoreError(event);
        }
    }

    private void enableDependentPlugins(final Collection<Plugin> pluginsToEnable) {
        if (pluginsToEnable.isEmpty()) {
            log.debug("No dependent plugins found to enable.");
            return;
        }

        final List<Plugin> pluginsInEnableOrder = new PluginsInEnableOrder(pluginsToEnable, pluginRegistry).get();

        if (log.isDebugEnabled()) {
            log.debug("Found {} plugins to enable: {}", pluginsInEnableOrder.size(), toPluginKeys(pluginsInEnableOrder));
        }

        for (final Plugin plugin : pluginsInEnableOrder) {
            broadcastIgnoreError(new PluginEnablingEvent(plugin));
        }

        pluginEnabler.enable(pluginsInEnableOrder); // enable all plugins, waiting a time period for them to enable

        for (final Plugin plugin : additionalPluginsToEnable) {
            if (PluginState.ENABLED == plugin.getPluginState()) {
                log.warn("Plugin {} was early but failed to enable, but was fallback enabled in lateStartup."
                                + " It likely has dependencies on plugins which are late, in which case you should fix those plugins and"
                                + " make them early, or as a last resort make the offending plugin late",
                        plugin);
            }
        }
        additionalPluginsToEnable.clear();

        // handle the plugins that were able to be successfully enabled
        for (final Plugin plugin : pluginsInEnableOrder) {
            if (plugin.getPluginState() == PluginState.ENABLED) {
                if (enableConfiguredPluginModules(plugin)) {
                    broadcastIgnoreError(new PluginEnabledEvent(plugin));
                }
            }
        }
    }

    private void discardPlugin(final @Nullable PluginLoader loader, final Plugin plugin) {
        if (null == loader) {
            // This happens if we're discarding a plugin during init() due to version restrictions
            candidatePluginsToPluginLoader.get(plugin).discardPlugin(plugin);
        } else if (loader instanceof DiscardablePluginLoader) {
            // This happens if we're discarding a plugin because it would be a downgrade. The loader is one of our loaders from
            // pluginLoaders which was wrapped with PermissionCheckingPluginLoader which is a DiscardablePluginLoader.
            ((DiscardablePluginLoader) loader).discardPlugin(plugin);
        } else {
            // This is only reachable if the caller has supplied their own PluginLoader, since all of ours are wrapped in a
            // PermissionCheckingPluginLoader which implements DiscardablePluginLoader.  We ignore the discard, but we log because
            // (in the short term) this is more likely to be a PluginLoader which should really be a DiscardablePluginLoader then
            // one which is legitimately ignoring discardPlugin.
            log.debug("Ignoring discardPlugin({}, version {}) as delegate is not a DiscardablePluginLoader",
                    plugin.getKey(), plugin.getPluginInformation().getVersion());
        }
    }

    private boolean pluginVersionIsAcceptable(final Plugin plugin, final Map<String, String> minimumPluginVersions) {
        final String pluginKey = plugin.getKey();
        final String rawMinimumVersion = minimumPluginVersions.get(pluginKey);
        if (null == rawMinimumVersion) {
            // fine, no minimum given
            return true;
        }

        final String cleanMinimumVersion = cleanVersionString(rawMinimumVersion);

        // Rather than replicate some of the validation login in VersionStringComparator, let it validate and handle errors
        try {
            // Actually compare versions

            final PluginInformation pluginInformation = plugin.getPluginInformation();
            final String pluginVersion = cleanVersionString((pluginInformation != null) ? pluginInformation.getVersion() : null);
            final VersionStringComparator versionStringComparator = new VersionStringComparator();
            return versionStringComparator.compare(pluginVersion, cleanMinimumVersion) >= 0;
        } catch (IllegalArgumentException e_ia) {
            log.warn("Cannot compare minimum version '{}' for plugin {}: {}",
                    rawMinimumVersion, plugin, e_ia.getMessage());
            // accept the plugin if something goes wrong
            return true;
        }
    }

    /**
     * Disable all plugins which require, possibly indirectly, a given plugin specified by key.
     * <p>
     * The plugin with given key is not disabled.
     * <p>
     * This prevents a dependent plugin trying to access, indirectly, the felix global lock, which is held by the
     * PackageAdmin while refreshing. See https://ecosystem.atlassian.net/browse/PLUG-582 for details.
     *
     * @param pluginKeys      Plugin keys to disable dependents of.
     * @param dependencyTypes only disable these types
     * @return dependent plugins plugins that were disabled.
     */
    private DependentPlugins disablePlugins(final Collection<String> pluginKeys, final Set<PluginDependencies.Type> dependencyTypes) {
        final DependentPlugins dependentPlugins = new DependentPlugins(pluginKeys, getEnabledPlugins(), dependencyTypes);

        if (!dependentPlugins.get().isEmpty()) {
            log.info("Found dependent enabled plugins for plugins '{}': {}.  Disabling...",
                    pluginKeys, dependentPlugins.toPluginKeyDependencyTypes());

            for (Plugin p : dependentPlugins.get()) {
                broadcastPluginDisabling(p);
            }

            for (Plugin p : dependentPlugins.get()) {
                disablePluginWithModuleEvents(p);
            }

            for (Plugin p : dependentPlugins.get()) {
                broadcastPluginDisabled(p);
            }
        }

        return dependentPlugins;
    }

    /**
     * Replace an already loaded plugin with another version. Relevant stored
     * configuration for the plugin will be preserved.
     *
     * @param oldPlugin Plugin to replace
     * @param newPlugin New plugin to install
     * @throws PluginException if the plugin cannot be updated
     */
    protected void updatePlugin(final Plugin oldPlugin, final Plugin newPlugin) throws PluginException {
        if (!oldPlugin.getKey().equals(newPlugin.getKey())) {
            throw new IllegalArgumentException("New plugin '" + newPlugin +
                    "' must have the same key as the old plugin '" + oldPlugin + "'");
        }

        if (log.isInfoEnabled()) {
            // We know the keys are the same here, so log the versions (if available)
            final PluginInformation oldInformation = oldPlugin.getPluginInformation();
            final String oldVersion = (oldInformation == null) ? "?" : oldInformation.getVersion();
            final PluginInformation newInformation = newPlugin.getPluginInformation();
            final String newVersion = (newInformation == null) ? "?" : newInformation.getVersion();
            log.info("Updating plugin '{}' from version '{}' to version '{}'",
                    oldPlugin, oldVersion, newVersion);
        }

        // Preserve the old plugin configuration - uninstall changes it (as
        // disable is called on all modules) and then
        // removes it
        final Map<String, Boolean> oldPluginState = new HashMap<String, Boolean>(getState().getPluginStateMap(oldPlugin));

        log.debug("Uninstalling old plugin: {}", oldPlugin);
        uninstallNoEvent(oldPlugin);
        log.debug("Plugin uninstalled '{}', preserving old state", oldPlugin);

        // Build a set of module keys from the new plugin version
        final Set<String> newModuleKeys = new HashSet<String>();
        newModuleKeys.add(newPlugin.getKey());
        for (final ModuleDescriptor<?> moduleDescriptor : newPlugin.getModuleDescriptors()) {
            newModuleKeys.add(moduleDescriptor.getCompleteKey());
        }

        // for removing any keys from the old plugin state that do not exist in
        // the
        // new version
        final Predicate<String> filter = new Predicate<String>() {
            public boolean apply(final String o) {
                return newModuleKeys.contains(o);
            }
        };

        persistentStateModifier.addState(filterKeys(oldPluginState, filter));
    }

    public Collection<Plugin> getPlugins() {
        return pluginRegistry.getAll();
    }

    /**
     * @see PluginAccessor#getPlugins(com.atlassian.plugin.predicate.PluginPredicate)
     * @since 0.17
     */
    public Collection<Plugin> getPlugins(final PluginPredicate pluginPredicate) {
        return copyOf(filter(getPlugins(), new Predicate<Plugin>() {
            public boolean apply(final Plugin plugin) {
                return pluginPredicate.matches(plugin);
            }
        }));
    }

    /**
     * @see PluginAccessor#getEnabledPlugins()
     */
    public Collection<Plugin> getEnabledPlugins() {
        Collection<Plugin> enabledPlugins = getPlugins(new EnabledPluginPredicate(pluginEnabler.getPluginsBeingEnabled()));
        return enabledPlugins;
    }

    /**
     * @see PluginAccessor#getModules(com.atlassian.plugin.predicate.ModuleDescriptorPredicate)
     * @since 0.17
     */
    public <M> Collection<M> getModules(final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        return copyOf(getModules(getModuleDescriptors(getPlugins(), moduleDescriptorPredicate)));
    }

    /**
     * @see PluginAccessor#getModuleDescriptors(com.atlassian.plugin.predicate.ModuleDescriptorPredicate)
     * @since 0.17
     */
    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        return copyOf(getModuleDescriptors(getPlugins(), moduleDescriptorPredicate));
    }

    /**
     * Get the all the module descriptors from the given collection of plugins,
     * filtered by the predicate.
     * <p>
     * Be careful, your predicate must filter ModuleDescriptors that are not M,
     * this method does not guarantee that the descriptors are of the correct
     * type by itself.
     *
     * @param plugins a collection of {@link Plugin}s
     * @return a collection of {@link ModuleDescriptor descriptors}
     */
    private <M> Iterable<ModuleDescriptor<M>> getModuleDescriptors(final Collection<Plugin> plugins, final ModuleDescriptorPredicate<M> predicate) {
        // hack way to get typed descriptors from plugin and
        // keep generics happy
        final Function<ModuleDescriptor<?>, ModuleDescriptor<M>> coercer = new Function<ModuleDescriptor<?>, ModuleDescriptor<M>>() {
            public ModuleDescriptor<M> apply(final ModuleDescriptor<?> input) {
                @SuppressWarnings("unchecked")
                final ModuleDescriptor<M> result = (ModuleDescriptor<M>) input;
                return result;
            }
        };

        // google predicate adapter
        final Predicate<ModuleDescriptor<M>> adapter = new Predicate<ModuleDescriptor<M>>() {
            public boolean apply(final ModuleDescriptor<M> input) {
                return predicate.matches(input);
            }
        };

        // get the filtered module descriptors from a plugin
        final Function<Plugin, Iterable<ModuleDescriptor<M>>> descriptorExtractor = new Function<Plugin, Iterable<ModuleDescriptor<M>>>() {
            public Iterable<ModuleDescriptor<M>> apply(final Plugin plugin) {
                return filter(transform(plugin.getModuleDescriptors(), coercer), adapter);
            }
        };

        // concatenate all the descriptor iterables into one
        return concat(transform(plugins, descriptorExtractor));
    }

    /**
     * Get the modules of all the given descriptor. If any of the getModule()
     * calls fails, the error is recorded in the logs and the plugin is
     * disabled.
     *
     * @param moduleDescriptors the collection of module descriptors to get the
     *                          modules from.
     * @return a {@link Collection} modules that can be any type of object. This
     * collection will not contain any null value.
     */
    private <M> Iterable<M> getModules(final Iterable<ModuleDescriptor<M>> moduleDescriptors) {
        return safeModuleExtractor.getModules(moduleDescriptors);
    }

    public Plugin getPlugin(final String key) {
        return pluginRegistry.get(notNull("The plugin key must be specified", key));
    }

    public Plugin getEnabledPlugin(final String pluginKey) {
        if (!isPluginEnabled(pluginKey)) {
            return null;
        }
        return getPlugin(pluginKey);
    }

    public ModuleDescriptor<?> getPluginModule(final String completeKey) {
        return getPluginModule(new ModuleCompleteKey(completeKey));
    }

    private ModuleDescriptor<?> getPluginModule(final ModuleCompleteKey key) {
        final Plugin plugin = getPlugin(key.getPluginKey());
        if (plugin == null) {
            return null;
        }
        return plugin.getModuleDescriptor(key.getModuleKey());
    }

    public ModuleDescriptor<?> getEnabledPluginModule(final String completeKey) {
        final ModuleCompleteKey key = new ModuleCompleteKey(completeKey);

        // If it's disabled, return null
        if (!isPluginModuleEnabled(key)) {
            return null;
        }

        return getEnabledPlugin(key.getPluginKey()).getModuleDescriptor(key.getModuleKey());
    }

    /**
     * @see PluginAccessor#getEnabledModulesByClass(Class)
     */
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        return copyOf(getModules(getEnabledModuleDescriptorsByModuleClass(moduleClass)));
    }

    /**
     * @see PluginAccessor#getEnabledModulesByClassAndDescriptor(Class[], Class)
     * @deprecated since 0.17, use
     * {@link #getModules(com.atlassian.plugin.predicate.ModuleDescriptorPredicate)}
     * with an appropriate predicate instead.
     */
    @Deprecated
    public <M> List<M> getEnabledModulesByClassAndDescriptor(final Class<ModuleDescriptor<M>>[] descriptorClasses, final Class<M> moduleClass) {
        final Iterable<ModuleDescriptor<M>> moduleDescriptors = filterDescriptors(getEnabledModuleDescriptorsByModuleClass(moduleClass), new ModuleDescriptorOfClassPredicate<M>(descriptorClasses));

        return copyOf(getModules(moduleDescriptors));
    }

    /**
     * @see PluginAccessor#getEnabledModulesByClassAndDescriptor(Class, Class)
     * @deprecated since 0.17, use
     * {@link #getModules(com.atlassian.plugin.predicate.ModuleDescriptorPredicate)}
     * with an appropriate predicate instead.
     */
    @Deprecated
    public <M> List<M> getEnabledModulesByClassAndDescriptor(final Class<ModuleDescriptor<M>> descriptorClass, final Class<M> moduleClass) {
        final Iterable<ModuleDescriptor<M>> moduleDescriptors = getEnabledModuleDescriptorsByModuleClass(moduleClass);
        return copyOf(getModules(filterDescriptors(moduleDescriptors, new ModuleDescriptorOfClassPredicate<M>(
                descriptorClass))));
    }

    /**
     * Get all module descriptor that are enabled and for which the module is an
     * instance of the given class.
     *
     * @param moduleClass the class of the module within the module descriptor.
     * @return a collection of {@link ModuleDescriptor}s
     */
    private <M> Collection<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<M>(moduleClass);
        final EnabledModulePredicate<M> enabled = new EnabledModulePredicate<M>();
        return copyOf(getModuleDescriptors(getEnabledPlugins(), new ModuleDescriptorPredicate<M>() {
            public boolean matches(final ModuleDescriptor<? extends M> moduleDescriptor) {
                return ofType.matches(moduleDescriptor) && enabled.matches(moduleDescriptor);
            }
        }));
    }

    /**
     * This method has been reverted to pre PLUG-40 to fix performance issues
     * that were encountered during load testing. This should be reverted to the
     * state it was in at 54639 when the fundamental issue leading to this
     * slowdown has been corrected (that is, slowness of PluginClassLoader).
     *
     * @see PluginAccessor#getEnabledModuleDescriptorsByClass(Class)
     */
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        // TODO Avoid moving elements into a List until we call something other than iterator()
        // Most of the time getEnabledModuleDescriptorsByClass is called within a foreach loop so would only call iterator()

        final Predicate<ModuleDescriptor<?>> instanceOfPredicate = new Predicate<ModuleDescriptor<?>>() {
            @Override
            public boolean apply(ModuleDescriptor<?> moduleDescriptor) {
                return descriptorClazz.isInstance(moduleDescriptor);
            }
        };

        final Function<ModuleDescriptor<?>, D> castToClassFunction = new Function<ModuleDescriptor<?>, D>() {
            @Override
            public D apply(ModuleDescriptor<?> module) {
                return descriptorClazz.cast(module);
            }
        };

        return copyOf(concat(transform(getEnabledPlugins(), new Function<Plugin, Iterable<D>>() {
            @Override
            public Iterable<D> apply(Plugin plugin) {
                Iterable<ModuleDescriptor<?>> moduleDescriptorsOfClass = filter(plugin.getModuleDescriptors(), instanceOfPredicate);

                Iterable<ModuleDescriptor<?>> enabledModuleDescriptors = filter(moduleDescriptorsOfClass, (new EnabledModulePredicate<>())::matches);

                return transform(enabledModuleDescriptors, castToClassFunction);
            }
        })));
    }

    @Deprecated
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz, final boolean verbose) {
        return getEnabledModuleDescriptorsByClass(descriptorClazz);
    }

    /**
     * @see PluginAccessor#getEnabledModuleDescriptorsByType(String)
     * @deprecated since 0.17, use
     * {@link #getModuleDescriptors(com.atlassian.plugin.predicate.ModuleDescriptorPredicate)}
     * with an appropriate predicate instead.
     */
    @Deprecated
    public <M> List<ModuleDescriptor<M>> getEnabledModuleDescriptorsByType(final String type) throws PluginParseException, IllegalArgumentException {
        final ModuleDescriptorOfTypePredicate<M> ofType = new ModuleDescriptorOfTypePredicate<M>(moduleDescriptorFactory, type);
        final EnabledModulePredicate<M> enabled = new EnabledModulePredicate<M>();
        return copyOf(getModuleDescriptors(getEnabledPlugins(), new ModuleDescriptorPredicate<M>() {
            public boolean matches(final ModuleDescriptor<? extends M> moduleDescriptor) {
                return ofType.matches(moduleDescriptor) && enabled.matches(moduleDescriptor);
            }
        }));
    }

    /**
     * Filters out a collection of {@link ModuleDescriptor}s given a predicate.
     *
     * @param descriptors the collection of {@link ModuleDescriptor}s to filter.
     * @param predicate   the predicate to use for filtering.
     */
    private static <M> Iterable<ModuleDescriptor<M>> filterDescriptors(final Iterable<ModuleDescriptor<M>> descriptors, final ModuleDescriptorPredicate<M> predicate) {
        return filter(descriptors, new Predicate<ModuleDescriptor<M>>() {
            public boolean apply(final ModuleDescriptor<M> input) {
                return predicate.matches(input);
            }
        });
    }

    /**
     * Enable a set of plugins by key. This will implicitly and recursively
     * enable all dependent plugins.
     *
     * @param keys The plugin keys. Must not be null.
     * @since 2.5.0
     */
    public void enablePlugins(final String... keys) {
        final Collection<Plugin> pluginsToEnable = new ArrayList<Plugin>(keys.length);

        for (final String key : keys) {
            if (key == null) {
                throw new IllegalArgumentException("Keys passed to enablePlugins must be non-null");
            }

            final Plugin plugin = pluginRegistry.get(key);
            if (plugin == null) {
                final Plugin delayedPlugin = findDelayedPlugin(key);
                if (delayedPlugin == null) {
                    log.info("No plugin was found for key '{}'. Not enabling.", key);
                } else {
                    persistentStateModifier.enable(delayedPlugin);
                }
                continue;
            }

            if (!plugin.getPluginInformation().satisfiesMinJavaVersion()) {
                log.error("Minimum Java version of '" + plugin.getPluginInformation().getMinJavaVersion() + "' was not satisfied for module '" + key + "'. Not enabling.");
                continue;
            }

            //Do not enable if already enabled, which inturn will prevent the notify
            if (plugin.getPluginState() != PluginState.ENABLED) {
                pluginsToEnable.add(plugin);
            }
        }
        for (final Plugin plugin : pluginsToEnable) {
            broadcastIgnoreError(new PluginEnablingEvent(plugin));
        }

        final Collection<Plugin> enabledPlugins = pluginEnabler.enableAllRecursively(pluginsToEnable);

        for (final Plugin plugin : enabledPlugins) {
            persistentStateModifier.enable(plugin);
            if (enableConfiguredPluginModules(plugin)) {
                broadcastIgnoreError(new PluginEnabledEvent(plugin));
            }
        }
    }

    /**
     * @deprecated since 2.5.0, use {#link enablePlugins(String... keys)} instead
     */
    @Deprecated
    public void enablePlugin(final String key) {
        enablePlugins(key);
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected void enablePluginState(final Plugin plugin, final PluginPersistentStateStore stateStore) {
        new PluginPersistentStateModifier(stateStore).enable(plugin);
    }

    /**
     * Called on all clustered application nodes, rather than
     * {@link #enablePlugin(String)} to just update the local state, state aware
     * modules and loaders, but not affect the global plugin state.
     *
     * @param plugin the plugin being enabled
     * @deprecated since 4.0.0. This is not a safe mechanism to intercept or access plugin enablement. This method will be removed
     * (or made private) in a future release. Interception should be done via {@link PluginEnablingEvent} and
     * {@link PluginEnabledEvent}, and the public API {@link #enablePlugins} should be used for changing plugin state.
     */
    @Deprecated
    protected void notifyPluginEnabled(final Plugin plugin) {
        broadcastIgnoreError(new PluginEnablingEvent(plugin));
        plugin.enable();
        if (enableConfiguredPluginModules(plugin)) {
            broadcastIgnoreError(new PluginEnabledEvent(plugin));
        }
    }

    /**
     * For each module in the plugin, call the module descriptor's enabled()
     * method if the module is StateAware and enabled.
     * <p>
     * If any modules fail to enable then the plugin is replaced by an
     * UnloadablePlugin, and this method will return {@code false}.
     *
     * @param plugin the plugin to enable
     * @return true if the modules were all enabled correctly, false otherwise.
     */
    private boolean enableConfiguredPluginModules(final Plugin plugin) {
        final Set<ModuleDescriptor<?>> enabledDescriptors = new HashSet<ModuleDescriptor<?>>();
        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            if (!enableConfiguredPluginModule(plugin, descriptor, enabledDescriptors)) {
                return false;
            }
        }
        return true;
    }

    private boolean enableConfiguredPluginModule(final Plugin plugin, final ModuleDescriptor<?> descriptor, final Set<ModuleDescriptor<?>> enabledDescriptors) {
        try {
            // This can happen if the plugin available event is fired as part of the plugin initialization process
            if (pluginEnabler.isPluginBeingEnabled(plugin)) {
                log.debug("The plugin is currently being enabled, so we won't bother trying to enable the '{}' module",
                        descriptor.getKey());
                return true;
            }

            // We only want to re-enable modules that weren't explicitly
            // disabled by the user.
            if (!isPluginEnabled(descriptor.getPluginKey()) || !getState().isEnabled(descriptor)) {
                log.debug("Plugin module '{}' is explicitly disabled (or so by default), so not re-enabling.",
                        descriptor.getName() == null ? descriptor.getKey() : descriptor.getName());
                return true;
            }
            notifyModuleEnabled(descriptor);
            enabledDescriptors.add(descriptor);

            return true;
        } catch (final Throwable enableException) {
            // catch any errors and insert an UnloadablePlugin (PLUG-7)
            log.error("There was an error loading the descriptor '{}' of plugin '{}'. Disabling.",
                    descriptor.getName(), plugin, enableException);

            // Disable all previously enabled descriptors
            for (final ModuleDescriptor<?> descriptorToDisable : enabledDescriptors) {
                try {
                    notifyModuleDisabled(descriptorToDisable);
                } catch (final Exception disableException) {
                    log.error("Could not notify previously enabled descriptor {} of module disabled in plugin {}",
                            descriptorToDisable.getName(), plugin, disableException);
                }
            }

            //use the original exception
            replacePluginWithUnloadablePlugin(plugin, descriptor, enableException);
            return false;
        }
    }

    public void disablePlugin(final String key) {
        disablePluginInternal(key, true);
    }

    public void disablePluginWithoutPersisting(final String key) {
        disablePluginInternal(key, false);
    }

    protected void disablePluginInternal(final String key, final boolean persistDisabledState) {
        if (key == null) {
            throw new IllegalArgumentException("You must specify a plugin key to disable.");
        }

        final Plugin plugin = pluginRegistry.get(key);
        if (plugin == null) {
            final Plugin delayedPlugin = findDelayedPlugin(key);
            if (delayedPlugin == null) {
                log.info("No plugin was found for key '{}'. Not disabling.", key);
            } else {
                if (persistDisabledState) {
                    persistentStateModifier.disable(delayedPlugin);
                }
            }
            return;
        }

        // Do not disable if the plugin is already disabled
        if (plugin.getPluginState() != PluginState.DISABLED) {
            final DependentPlugins disabledPlugins = disablePlugins(ImmutableList.of(plugin.getKey()),
                    ImmutableSet.of(MANDATORY, OPTIONAL));

            notifyPluginDisabled(plugin);
            if (persistDisabledState) {
                persistentStateModifier.disable(plugin);
            }

            reenableDependent(ImmutableList.of(plugin), disabledPlugins, PluginState.DISABLED);
        }
    }

    // re-enable plugins dependent on a plugin depending on the final state
    // - for uninstalled enable optional and dynamic
    // - for disabled enable only optional
    private void reenableDependent(final Collection<Plugin> plugins, final DependentPlugins disabledPlugins, final PluginState state) {
        final Set<PluginDependencies.Type> cycledTypes = EnumSet.of(OPTIONAL);
        if (state == PluginState.UNINSTALLED) {
            cycledTypes.add(DYNAMIC);
        } else if (state != PluginState.DISABLED) {
            throw new IllegalArgumentException("State must be one of (UNINSTALLED,DISABLED)");
        }

        final Set<Plugin> cycled = disabledPlugins.getByTypes(cycledTypes);
        if (!cycled.isEmpty()) {
            log.info("Found optional/dynamic dependent plugins to re-enable after plugins {} '{}': {}.  Enabling...",
                    state, plugins, disabledPlugins.toPluginKeyDependencyTypes(cycledTypes));
            enableDependentPlugins(cycled);
        }

        final Set<Plugin> disabled = disabledPlugins.getByTypes(ImmutableSet.of(MANDATORY));
        if (!disabled.isEmpty() || !cycled.isEmpty()) {
            plugins.stream().forEach(p -> broadcastIgnoreError(new PluginDependentsChangedEvent(p, state, disabled, cycled)));
        }
    }

    private Plugin findDelayedPlugin(final String key) {
        return tryFind(delayedPlugins, new Predicate<Plugin>() {
            @Override
            public boolean apply(final Plugin input) {
                return input.getKey().equals(key);
            }
        }).orNull();
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected void disablePluginState(final Plugin plugin, final PluginPersistentStateStore stateStore) {
        new PluginPersistentStateModifier(stateStore).disable(plugin);
    }

    /**
     * Disable a plugin without broadcasting PluginDisabl(ing|ed) events
     * <p>
     * It is expected that {@link #broadcastPluginDisabling(Plugin)} will be called for the plugin before
     * and {@link #broadcastPluginDisabled(Plugin)} will be called after the method call
     * <p>
     * Note: all plugin's modules will be disabled in this method and corresponding PluginModule* events
     * will still be broadcasted
     *
     * @param plugin the plugin to disable
     */
    private void disablePluginWithModuleEvents(final Plugin plugin) {
        if (plugin.getPluginState() == PluginState.DISABLED) {
            return;
        }

        disablePluginModules(plugin);

        // This needs to happen after modules are disabled to prevent errors
        plugin.disable();
    }

    private void broadcastPluginDisabling(final Plugin plugin) {
        log.info("Disabling {}", plugin);
        // Continue to fire the deprecated event for transition compatibility
        broadcastIgnoreError(new BeforePluginDisabledEvent(plugin));
        broadcastIgnoreError(new PluginDisablingEvent(plugin));
    }

    private void broadcastPluginDisabled(final Plugin plugin) {
        broadcastIgnoreError(new PluginDisabledEvent(plugin));
    }

    /**
     * @param plugin the plugin being disabled.
     * @deprecated since 4.0.0. This is not a safe mechanism to intercept or access plugin disablement. This method will be removed
     * (or made private) in a future release. Interception should be done via {@link PluginDisablingEvent} and
     * {@link PluginDisabledEvent}, and the public APIs {@link #disablePlugin} or {@link #disablePluginWithoutPersisting} should be
     * used for changing plugin state.
     */
    @Deprecated
    protected void notifyPluginDisabled(final Plugin plugin) {
        broadcastPluginDisabling(plugin);

        disablePluginWithModuleEvents(plugin);

        broadcastPluginDisabled(plugin);
    }


    private void disablePluginModules(final Plugin plugin) {
        final List<ModuleDescriptor<?>> moduleDescriptors = new ArrayList<ModuleDescriptor<?>>(plugin.getModuleDescriptors());
        Collections.reverse(moduleDescriptors); // disable in reverse order

        for (final ModuleDescriptor<?> module : moduleDescriptors) {
            // don't actually disable the module, just fire the events because
            // its plugin is being disabled
            // if the module was actually disabled, you'd have to reenable each
            // one when enabling the plugin

            disablePluginModuleNoPersist(module);
        }
    }

    private void disablePluginModuleNoPersist(final ModuleDescriptor<?> module) {
        if (isPluginModuleEnabled(module.getCompleteKey())) {
            publishModuleDisabledEvents(module, false);
        }
    }

    public void disablePluginModule(final String completeKey) {
        if (completeKey == null) {
            throw new IllegalArgumentException("You must specify a plugin module key to disable.");
        }

        final ModuleDescriptor<?> module = getPluginModule(completeKey);

        if (module == null) {
            log.info("Returned module for key '{}' was null. Not disabling.", completeKey);
            return;
        }
        if (module.getClass().isAnnotationPresent(CannotDisable.class)) {
            log.info("Plugin module '{}' cannot be disabled; it is annotated with {}", completeKey, CannotDisable.class.getName());
            return;
        }
        persistentStateModifier.disable(module);
        notifyModuleDisabled(module);
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected void disablePluginModuleState(final ModuleDescriptor<?> module, final PluginPersistentStateStore stateStore) {
        new PluginPersistentStateModifier(stateStore).disable(module);
    }

    protected void notifyModuleDisabled(final ModuleDescriptor<?> module) {
        publishModuleDisabledEvents(module, true);
    }

    private void publishModuleDisabledEvents(final ModuleDescriptor<?> module, final boolean persistent) {
        log.debug("Disabling {}", module.getKey());
        // Continue to fire the deprecated event for transition compatibility
        broadcastIgnoreError(new BeforePluginModuleDisabledEvent(module, persistent));
        broadcastIgnoreError(new PluginModuleDisablingEvent(module, persistent));

        if (module instanceof StateAware) {
            ((StateAware) module).disabled();
        }

        broadcastIgnoreError(new PluginModuleDisabledEvent(module, persistent));
    }

    public void enablePluginModule(final String completeKey) {
        if (completeKey == null) {
            throw new IllegalArgumentException("You must specify a plugin module key to disable.");
        }

        final ModuleDescriptor<?> module = getPluginModule(completeKey);

        if (module == null) {
            log.info("Returned module for key '{}' was null. Not enabling.", completeKey);
            return;
        }

        if (!module.satisfiesMinJavaVersion()) {
            log.error("Minimum Java version of '" + module.getMinJavaVersion() + "' was not satisfied for module '" + completeKey + "'. Not enabling.");
            return;
        }

        persistentStateModifier.enable(module);
        notifyModuleEnabled(module);
    }

    /**
     * @deprecated since 4.0, to be removed in 5.0
     */
    @Deprecated
    protected void enablePluginModuleState(final ModuleDescriptor<?> module, final PluginPersistentStateStore stateStore) {
        new PluginPersistentStateModifier(stateStore).enable(module);
    }

    protected void notifyModuleEnabled(final ModuleDescriptor<?> module) {
        log.debug("Enabling {}", module.getKey());
        broadcastIgnoreError(new PluginModuleEnablingEvent(module));
        if (module instanceof StateAware) {
            ((StateAware) module).enabled();
        }
        broadcastIgnoreError(new PluginModuleEnabledEvent(module));
    }

    public boolean isPluginModuleEnabled(final String completeKey) {
        // completeKey may be null
        return (completeKey != null) && isPluginModuleEnabled(new ModuleCompleteKey(completeKey));
    }

    private boolean isPluginModuleEnabled(final ModuleCompleteKey key) {
        if (!isPluginEnabled(key.getPluginKey())) {
            return false;
        }
        final ModuleDescriptor<?> pluginModule = getPluginModule(key);
        return (pluginModule != null) && pluginModule.isEnabled();
    }

    /**
     * This method checks to see if the plugin is enabled based on the state
     * manager and the plugin.
     *
     * @param key The plugin key
     * @return True if the plugin is enabled
     */
    public boolean isPluginEnabled(final String key) {
        final Plugin plugin = pluginRegistry.get(notNull("The plugin key must be specified", key));

        return plugin != null &&
                plugin.getPluginState() == PluginState.ENABLED;
    }

    public InputStream getDynamicResourceAsStream(final String name) {
        return getClassLoader().getResourceAsStream(name);
    }

    public Class<?> getDynamicPluginClass(final String className) throws ClassNotFoundException {
        return getClassLoader().loadClass(className);
    }

    public PluginsClassLoader getClassLoader() {
        return classLoader;
    }

    public InputStream getPluginResourceAsStream(final String pluginKey, final String resourcePath) {
        final Plugin plugin = getEnabledPlugin(pluginKey);
        if (plugin == null) {
            log.error("Attempted to retreive resource " + resourcePath + " for non-existent or inactive plugin " + pluginKey);
            return null;
        }

        return plugin.getResourceAsStream(resourcePath);
    }

    /**
     * Disables and replaces a plugin currently loaded with an UnloadablePlugin.
     *
     * @param plugin     the plugin to be replaced
     * @param descriptor the descriptor which caused the problem
     * @param throwable  the problem caught when enabling the descriptor
     * @return the UnloadablePlugin which replaced the broken plugin
     */
    private UnloadablePlugin replacePluginWithUnloadablePlugin(final Plugin plugin, final ModuleDescriptor<?> descriptor, final Throwable throwable) {
        final UnloadableModuleDescriptor unloadableDescriptor = UnloadableModuleDescriptorFactory.createUnloadableModuleDescriptor(plugin,
                descriptor, throwable);
        final UnloadablePlugin unloadablePlugin = UnloadablePluginFactory.createUnloadablePlugin(plugin, unloadableDescriptor);

        // Add the error text at the plugin level as well. This is useful for
        // logging.
        unloadablePlugin.setErrorText(unloadableDescriptor.getErrorText());
        pluginRegistry.put(unloadablePlugin);

        // PLUG-390: We used to persist the disabled state here, but we don't
        // want to do this.
        // We want to try load this plugin again on restart as the user may have
        // installed a fixed version of this plugin.
        return unloadablePlugin;
    }

    public boolean isSystemPlugin(final String key) {
        final Plugin plugin = getPlugin(key);
        return (plugin != null) && plugin.isSystemPlugin();
    }

    public PluginRestartState getPluginRestartState(final String key) {
        return getState().getPluginRestartState(key);
    }

    private void broadcastIgnoreError(final Object event) {
        try {
            pluginEventManager.broadcast(event);
        } catch (final NotificationException ex) {
            log.warn("Error broadcasting '{}': {}.  Continuing anyway.", event, ex);
            for (Throwable throwable : ex.getAllCauses()) {
                log.debug("Cause:", throwable);
            }
        }
    }

    @Override
    public ModuleDescriptor<?> addDynamicModule(final Plugin maybePluginInternal, final Element module) {
        final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

        // identify the loader that loaded this plugin
        final PluginLoader pluginLoader = installedPluginsToPluginLoader.get(plugin);
        if (pluginLoader == null) {
            throw new PluginException("cannot locate PluginLoader that created plugin '" + plugin + "'");
        }

        // attempt to create the module
        final ModuleDescriptor moduleDescriptor = pluginLoader.createModule(plugin, module, moduleDescriptorFactory);
        if (moduleDescriptor == null) {
            throw new PluginException("cannot add dynamic module of type '" + module.getName() + "' to plugin '" + plugin + "' as the PluginLoader does not know how to create the module");
        }

        // check for duplicate module keys within the plugin; this is a safety-check as it should have been verified during module init
        if (plugin.getModuleDescriptor(moduleDescriptor.getKey()) != null) {
            throw new PluginException("duplicate module key '" + moduleDescriptor.getKey() + "' for plugin '" + plugin + "'");
        }

        // add to the plugin
        if (!plugin.addDynamicModuleDescriptor(moduleDescriptor)) {
            throw new PluginException("cannot add dynamic module '" + moduleDescriptor.getKey() + "' to plugin '" + plugin + "' as it is already present");
        }

        // enable the module only if the plugin is enabled and the module wasn't explicitly disabled by the user
        if (plugin.getPluginState() == PluginState.ENABLED && getState().isEnabled(moduleDescriptor)) {
            notifyModuleEnabled(moduleDescriptor);
        }

        return moduleDescriptor;
    }

    @Override
    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin maybePluginInternal) {
        final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

        return plugin.getDynamicModuleDescriptors();
    }

    @Override
    public void removeDynamicModule(final Plugin maybePluginInternal, final ModuleDescriptor<?> module) {
        final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

        // remove from the plugin
        if (!plugin.removeDynamicModuleDescriptor(module)) {
            throw new PluginException("cannot remove dynamic module '" + module.getKey() + "' from plugin '" + plugin + "' as it wasn't added by addDynamicModule");
        }

        // disable it
        persistentStateModifier.disable(module);
        notifyModuleDisabled(module);

        // destroy it
        module.destroy();
    }

    @VisibleForTesting
    PluginInternal checkPluginInternal(final Plugin maybePluginInternal) {
        // check the type
        if (!(maybePluginInternal instanceof PluginInternal)) {
            throw new IllegalArgumentException(maybePluginInternal + " does not implement com.atlassian.plugin.PluginInternal it is a " + maybePluginInternal.getClass().getCanonicalName());
        }

        return (PluginInternal) maybePluginInternal;
    }
}
