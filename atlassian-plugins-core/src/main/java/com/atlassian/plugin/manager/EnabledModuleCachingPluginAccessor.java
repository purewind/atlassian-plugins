package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.instrumentation.PluginSystemInstrumentation;
import com.atlassian.plugin.instrumentation.Timer;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.cache.LoadingCache;

import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.cache.CacheBuilder.newBuilder;
import static com.google.common.cache.CacheLoader.from;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * A caching decorator which caches {@link #getEnabledModuleDescriptorsByClass(Class)} and {@link
 * #getEnabledModulesByClass(Class)} on {@link com.atlassian.plugin.PluginAccessor} interface.
 */
public final class EnabledModuleCachingPluginAccessor extends ForwardingPluginAccessor implements PluginAccessor {
    private static final long DESCRIPTOR_TIMEOUT_SEC = Long.getLong("com.atlassian.plugin.descriptor.class.cache.timeout.sec", 30 * 60);
    private static final long MODULE_TIMEOUT_SEC = Long.getLong("com.atlassian.plugin.module.class.cache.timeout.sec", 30 * 60);

    private final SafeModuleExtractor safeModuleExtractor;

    private final LoadingCache<Class<ModuleDescriptor<Object>>, List<ModuleDescriptor<Object>>> cacheByDescriptorClass =
            newBuilder()
                    .expireAfterAccess(DESCRIPTOR_TIMEOUT_SEC, SECONDS)
                    .build(from(new ByModuleDescriptorClassCacheLoader()));

    private final LoadingCache<Class<?>, List<ModuleDescriptor<Object>>> cacheByModuleClass =
            newBuilder()
                    .expireAfterAccess(MODULE_TIMEOUT_SEC, SECONDS)
                    .build(from(new ByModuleClassCacheLoader()));

    public EnabledModuleCachingPluginAccessor(final PluginAccessor delegate, final PluginEventManager pluginEventManager, final PluginController pluginController) {
        super(delegate);
        checkNotNull(pluginEventManager);

        this.safeModuleExtractor = new SafeModuleExtractor(pluginController);

        // Strictly speaking a bad idea to register in constructor, but probably unlikely to get events during startup
        // https://extranet.atlassian.com/display/CONF/2015/04/28/Publishing+unconstructed+objects+into+main+memory+considered+harmful constructor may not be safe
        pluginEventManager.register(this);
    }

    /**
     * Clears the enabled module cache when any plugin is disabled. This ensures old modules are never returned from
     * disabled plugins.
     *
     * @param event The plugin disabled event
     */
    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginDisable(PluginDisabledEvent event) {
        // Safer not to assume we also receive module disable events for each individual module.
        invalidateAll();
    }

    /**
     * Clears the cache when any plugin is enabled.
     */
    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginEnable(PluginEnabledEvent event) {
        // PLUG-840 Don't assume we also receive module enable events for each individual module
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        // We invalidate from here and let cache loader reconstruct the state,
        // rather than having the cache entries be their own event listeners,
        // because if the cache entry is being constructed, we have a race condition: it may already have been told
        // the module is disabled, but not yet have registered to receive this enable event.

        // Just invalidate everything: entries for module/descriptor superclasses and interfaces all need invalidating,
        // this is simpler, and the cache mainly benefits web requests during post-startup steady state of plugin system.
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        // We invalidate from here and let cache loader reconstruct the state,
        // rather than having the cache entries be their own event listeners,
        // because if the cache entry is being constructed, we have a race condition: it may already have been told
        // the module is enabled, but not yet have registered to receive this disable event.

        // Just invalidate everything: entries for module/descriptor superclasses and interfaces all need invalidating,
        // this is simpler, and the cache mainly benefits web requests during post-startup steady state of plugin system.
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginFrameworkShutdown(final PluginFrameworkShutdownEvent event) {
        invalidateAll();
    }

    private void invalidateAll() {
        cacheByDescriptorClass.invalidateAll();
        cacheByModuleClass.invalidateAll();
    }

    /**
     * Cache loader for module descriptor cache
     */
    private class ByModuleDescriptorClassCacheLoader implements Function<Class<ModuleDescriptor<Object>>, List<ModuleDescriptor<Object>>> {
        public List<ModuleDescriptor<Object>> apply(final Class<ModuleDescriptor<Object>> moduleDescriptorClass) {
            return delegate.getEnabledModuleDescriptorsByClass(moduleDescriptorClass);
        }
    }

    /**
     * Cache loader for module class cache
     */
    private class ByModuleClassCacheLoader implements Function<Class, List<ModuleDescriptor<Object>>> {
        @SuppressWarnings("unchecked")
        public List<ModuleDescriptor<Object>> apply(final Class moduleClass) {
            return getEnabledModuleDescriptorsByModuleClass(moduleClass);
        }
    }

    /**
     * This method overrides the same method on PluginAccessor from the plugin system. We keep an up to date cache
     * of (module descriptor class M -> collection of module descriptors of descriptor class M) so we can avoid the expensive
     * call to the plugin system.
     *
     * @param descriptorClazz The module descriptor class you wish to find all enabled instances of
     * @return A list of all instances of enabled plugin module descriptors of the specified descriptor class
     */
    @SuppressWarnings("unchecked")
    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullTimer("getEnabledModuleDescriptorsByClass")) {
            final List<ModuleDescriptor<Object>> descriptors =
                    cacheByDescriptorClass.getUnchecked((Class<ModuleDescriptor<Object>>) descriptorClazz);
            return (List<D>) descriptors;
        }
    }

    /**
     * This method overrides the same method on PluginAccessor from the plugin system. We keep an up to date cache
     * of (module class M -> collection of module descriptors that have module class M) so we can avoid the expensive
     * call to the plugin system.
     *
     * @param moduleClass The module class you wish to find all instances of
     * @return A list of all instances of enabled plugin modules of the specified class
     */
    @Override
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullTimer("getEnabledModulesByClass")) {
            //noinspection unchecked
            return (List<M>) copyOf(safeModuleExtractor.getModules(cacheByModuleClass.getUnchecked(moduleClass)));
        }
    }

    /**
     * Creates a collection of module descriptors that all share the same given module class
     *
     * @param moduleClass The module class we are interested in
     * @param <M>         The module class we are interested in
     * @return A collection of all module descriptors in the system with module class M
     */
    private <M> List<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<>(moduleClass);
        final EnabledModulePredicate<M> enabled = new EnabledModulePredicate<>();
        return copyOf(getModuleDescriptors(delegate.getEnabledPlugins(), new ModuleDescriptorPredicate<M>() {
            public boolean matches(final ModuleDescriptor<? extends M> moduleDescriptor) {
                return ofType.matches(moduleDescriptor) && enabled.matches(moduleDescriptor);
            }
        }));
    }

    /**
     * Builds an iterable of module descriptors for the given module predicate out of a collection of plugins.
     *
     * @param plugins   A collection of plugins to search through
     * @param predicate A predicate to filter the module descriptors by
     * @param <M>       The module class type
     * @return An Iterable of module descriptors of module class M
     */
    private <M> Iterable<ModuleDescriptor<M>> getModuleDescriptors(final Collection<Plugin> plugins, final ModuleDescriptorPredicate<M> predicate) {
        // hack way to get typed descriptors from plugin and
        // keep generics happy
        final Function<ModuleDescriptor<?>, ModuleDescriptor<M>> coercer = new Function<ModuleDescriptor<?>, ModuleDescriptor<M>>() {
            public ModuleDescriptor<M> apply(final ModuleDescriptor<?> input) {
                @SuppressWarnings("unchecked")
                final ModuleDescriptor<M> result = (ModuleDescriptor<M>) input;
                return result;
            }
        };

        // google predicate adapter
        final Predicate<ModuleDescriptor<M>> adapter = new Predicate<ModuleDescriptor<M>>() {
            public boolean apply(final ModuleDescriptor<M> input) {
                return predicate.matches(input);
            }
        };

        // get the filtered module descriptors from a plugin
        final Function<Plugin, Iterable<ModuleDescriptor<M>>> descriptorExtractor = new Function<Plugin, Iterable<ModuleDescriptor<M>>>() {
            public Iterable<ModuleDescriptor<M>> apply(final Plugin plugin) {
                return filter(transform(plugin.getModuleDescriptors(), coercer), adapter);
            }
        };

        // concatenate all the descriptor iterables into one
        return concat(transform(plugins, descriptorExtractor));
    }
}
