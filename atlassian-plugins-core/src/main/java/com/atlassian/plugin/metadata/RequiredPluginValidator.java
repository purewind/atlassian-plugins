package com.atlassian.plugin.metadata;

import java.util.Collection;

public interface RequiredPluginValidator {
    /**
     * Validates that the plugins that have been marked as required by the host application are enabled.
     *
     * @return a Collection of all plugin and module keys that did not validate.
     */
    Collection<String> validate();
}
