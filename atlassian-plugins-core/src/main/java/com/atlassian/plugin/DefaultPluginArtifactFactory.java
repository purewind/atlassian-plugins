package com.atlassian.plugin;

import java.io.File;
import java.net.URI;

/**
 * Creates plugin artifacts by handling URI's that are files and looking at the file's extension
 *
 * @since 2.1.0
 */
public class DefaultPluginArtifactFactory implements PluginArtifactFactory {
    /**
     * The {@link ReferenceMode} passed to {@link JarPluginArtifact} instances created by this
     * factory.
     */
    final ReferenceMode referenceMode;

    /**
     * Create a factory which produces artifacts that do not allow reference installation.
     */
    public DefaultPluginArtifactFactory() {
        this(ReferenceMode.FORBID_REFERENCE);
    }

    /**
     * Create a factory which produces artifacts that optionally allow reference installation.
     *
     * @param referenceMode the {@link ReferenceMode} passed to {@link JarPluginArtifact} instances
     *                      created by this factory.
     */
    public DefaultPluginArtifactFactory(final ReferenceMode referenceMode) {
        this.referenceMode = referenceMode;
    }

    /**
     * Create a factory which produces artifacts that optionally allow reference installation.
     *
     * @param referenceMode the legacy {@link com.atlassian.plugin.PluginArtifact.AllowsReference.ReferenceMode} whose
     *                      modern equivalent {@link ReferenceMode} is passed to {@link JarPluginArtifact} instances created by this factory.
     * @deprecated since 4.0.0, to be removed in 5.0.0: Use {@link DefaultPluginArtifactFactory(ReferenceMode)} which uses the non
     * legacy {@link ReferenceMode}.
     */
    public DefaultPluginArtifactFactory(final com.atlassian.plugin.PluginArtifact.AllowsReference.ReferenceMode referenceMode) {
        this(referenceMode.toModern());
    }

    /**
     * Creates the artifact by looking at the file extension
     *
     * @param artifactUri The artifact URI
     * @return The created artifact
     * @throws IllegalArgumentException If an artifact cannot be created from the URL
     */
    public PluginArtifact create(URI artifactUri) {
        PluginArtifact artifact = null;

        String protocol = artifactUri.getScheme();

        if ("file".equalsIgnoreCase(protocol)) {
            File artifactFile = new File(artifactUri);

            String file = artifactFile.getName();
            if (file.endsWith(".jar")) {
                artifact = new JarPluginArtifact(artifactFile, referenceMode);
            } else if (file.endsWith(".xml")) {
                artifact = new XmlPluginArtifact(artifactFile);
            }
        }

        if (artifact == null) {
            throw new IllegalArgumentException("The artifact URI " + artifactUri + " is not a valid plugin artifact");
        }

        return artifact;
    }
}
