package com.atlassian.plugin.eventlistener.descriptors;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.LazyReference;
import org.dom4j.Element;

import javax.annotation.Nonnull;

@RequirePermission(Permissions.EXECUTE_JAVA)
public class EventListenerModuleDescriptor extends AbstractModuleDescriptor<Object> {
    private final EventPublisher eventPublisher;

    private LazyReference<Object> moduleObj = new LazyReference<Object>() {
        @Override
        protected Object create() throws Exception {
            return moduleFactory.createModule(moduleClassName, EventListenerModuleDescriptor.this);
        }
    };

    public EventListenerModuleDescriptor(ModuleFactory moduleFactory, EventPublisher eventPublisher) {
        super(moduleFactory);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        checkPermissions();
    }

    @Override
    public Object getModule() {
        return moduleObj.get();
    }

    @Override
    public void enabled() {
        super.enabled();
        eventPublisher.register(getModule());
    }

    public void disabled() {
        eventPublisher.unregister(getModule());
        super.disabled();
    }
}