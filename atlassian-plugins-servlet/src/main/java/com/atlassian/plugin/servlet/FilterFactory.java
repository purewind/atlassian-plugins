package com.atlassian.plugin.servlet;

import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.filter.DelegatingPluginFilter;

import javax.servlet.Filter;

/**
 * Creates {@link javax.servlet.Filter} instances.
 *
 * @since 3.2.20
 */
class FilterFactory {
    Filter newFilter(final ServletFilterModuleDescriptor descriptor) {
        return new DelegatingPluginFilter(descriptor);
    }
}
