package com.atlassian.plugin.servlet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.servlet.filter.PluginFilterConfig;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;
import com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor;
import com.atlassian.plugin.util.ClassLoaderStack;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor.byWeight;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple servletModuleManager to track and retrieve the loaded servlet plugin modules.
 *
 * @since 2.1.0
 */
public class DefaultServletModuleManager implements ServletModuleManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultServletModuleManager.class);

    private final PathMapper servletMapper;
    private final Map<String, ServletModuleDescriptor> servletDescriptors = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, LazyReference<HttpServlet>> servletRefs = new ConcurrentHashMap<>();

    private final PathMapper filterMapper;
    private final Map<String, ServletFilterModuleDescriptor> filterDescriptors = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, LazyReference<Filter>> filterRefs = new ConcurrentHashMap<>();

    private final FilterFactory filterFactory;
    private final ConcurrentMap<Plugin, ContextLifecycleReference> pluginContextRefs = new ConcurrentHashMap<>();

    private final AtomicReference<PluginController> pluginControllerRef = new AtomicReference<>();

    /**
     * Constructor that sets itself in the servlet context for later use in dispatching servlets and filters.
     *
     * @param servletContext     The servlet context to store itself in
     * @param pluginEventManager The plugin event manager
     * @since 2.2.0
     */
    public DefaultServletModuleManager(final ServletContext servletContext, final PluginEventManager pluginEventManager) {
        this(pluginEventManager);
        ServletContextServletModuleManagerAccessor.setServletModuleManager(servletContext, this);
    }

    /**
     * Creates the servlet module manager, but assumes you will be calling {@link com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor#setServletModuleManager(javax.servlet.ServletContext,
     * ServletModuleManager)} yourself if you don't extend the dispatching servlet and filter classes to provide the servlet module
     * manager instance.
     *
     * @param pluginEventManager The plugin event manager
     * @deprecated Use {@link #DefaultServletModuleManager(com.atlassian.plugin.event.PluginEventManager,
     * com.atlassian.plugin.servlet.util.PathMapper, com.atlassian.plugin.servlet.util.PathMapper, FilterFactory)} instead. Since
     * v3.2.20.
     */
    @Deprecated
    public DefaultServletModuleManager(final PluginEventManager pluginEventManager) {
        this(pluginEventManager, new DefaultPathMapper(), new DefaultPathMapper());
    }

    /**
     * Creates the servlet module manager, but assumes you will be calling {@link com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor#setServletModuleManager(javax.servlet.ServletContext,
     * ServletModuleManager)} yourself if you don't extend the dispatching servlet and filter classes to provide the servlet module
     * manager instance.
     *
     * @param pluginEventManager The plugin event manager
     * @param servletPathMapper  The path mapper used for mapping servlets to paths
     * @param filterPathMapper   The path mapper used for mapping filters to paths
     * @deprecated Use {@link #DefaultServletModuleManager(com.atlassian.plugin.event.PluginEventManager,
     * com.atlassian.plugin.servlet.util.PathMapper, com.atlassian.plugin.servlet.util.PathMapper, FilterFactory)} instead. Since
     * v3.2.20.
     */
    @Deprecated
    public DefaultServletModuleManager(final PluginEventManager pluginEventManager, final PathMapper servletPathMapper, final PathMapper filterPathMapper) {
        this(pluginEventManager, servletPathMapper, filterPathMapper, new FilterFactory());
    }

    /**
     * The broadest constructor should be the last one standing when: <ul> <li>dependency injection is fixed</li> <li>passing
     * <code>this</code> before construction finishes is removed</li> </ul>
     *
     * @param pluginEventManager the event manager to register to
     * @param servletMapper      maps servlet paths
     * @param filterMapper       maps filter paths
     * @param filterFactory      creates filters
     * @since 3.2.20
     */
    DefaultServletModuleManager(
            final PluginEventManager pluginEventManager,
            final PathMapper servletMapper,
            final PathMapper filterMapper,
            final FilterFactory filterFactory) {
        this.servletMapper = servletMapper;
        this.filterMapper = filterMapper;
        this.filterFactory = filterFactory;
        pluginEventManager.register(this);
    }

    public void addServletModule(final ServletModuleDescriptor descriptor) {
        servletDescriptors.put(descriptor.getCompleteKey(), descriptor);

        // for some reason the JDK complains about getPaths not returning a
        // List<String> ?!?!?
        final List<String> paths = descriptor.getPaths();
        for (final String path : paths) {
            servletMapper.put(descriptor.getCompleteKey(), path);
        }
        final LazyReference<HttpServlet> servletRef = servletRefs.remove(descriptor.getCompleteKey());
        if (servletRef != null) {
            servletRef.get().destroy();
        }
    }

    public HttpServlet getServlet(final String path, final ServletConfig servletConfig) throws ServletException {
        final String completeKey = servletMapper.get(path);

        if (completeKey == null) {
            return null;
        }
        final ServletModuleDescriptor descriptor = servletDescriptors.get(completeKey);
        if (descriptor == null) {
            return null;
        }

        final HttpServlet servlet = getServlet(descriptor, servletConfig);
        if (servlet == null) {
            servletRefs.remove(descriptor.getCompleteKey());
        }
        return servlet;
    }

    public void removeServletModule(final ServletModuleDescriptor descriptor) {
        servletDescriptors.remove(descriptor.getCompleteKey());
        servletMapper.put(descriptor.getCompleteKey(), null);

        final LazyReference<HttpServlet> servletRef = servletRefs.remove(descriptor.getCompleteKey());
        if (servletRef != null) {
            servletRef.get().destroy();
        }
    }

    public void addFilterModule(final ServletFilterModuleDescriptor descriptor) {
        filterDescriptors.put(descriptor.getCompleteKey(), descriptor);

        for (final String path : descriptor.getPaths()) {
            filterMapper.put(descriptor.getCompleteKey(), path);
        }
        final LazyReference<Filter> filterRef = filterRefs.remove(descriptor.getCompleteKey());
        if (filterRef != null) {
            filterRef.get().destroy();
        }
    }

    public Iterable<Filter> getFilters(final FilterLocation location, final String path, final FilterConfig filterConfig)
            throws ServletException {
        return getFilters(location, path, filterConfig, FilterDispatcherCondition.REQUEST);
    }

    public Iterable<Filter> getFilters(FilterLocation location, String path, FilterConfig filterConfig, FilterDispatcherCondition condition)
            throws ServletException {
        checkNotNull(condition);
        final List<ServletFilterModuleDescriptor> matchingFilterDescriptors = new ArrayList<ServletFilterModuleDescriptor>();

        for (final String completeKey : filterMapper.getAll(path)) {
            final ServletFilterModuleDescriptor descriptor = filterDescriptors.get(completeKey);
            if (!descriptor.getDispatcherConditions().contains(condition)) {
                if (log.isTraceEnabled()) {
                    log.trace("Skipping filter " + descriptor.getCompleteKey() + " as condition " + condition +
                            " doesn't match list:" + Arrays.asList(descriptor.getDispatcherConditions()));
                }
                continue;
            }

            if (location.equals(descriptor.getLocation())) {
                matchingFilterDescriptors.add(descriptor);
            }
        }
        Collections.sort(matchingFilterDescriptors, byWeight);
        final List<Filter> filters = new LinkedList<Filter>();
        for (final ServletFilterModuleDescriptor descriptor : matchingFilterDescriptors) {
            final Filter filter = getFilter(descriptor, filterConfig);
            if (filter == null) {
                filterRefs.remove(descriptor.getCompleteKey());
            } else {
                filters.add(filter);
            }
        }

        return filters;
    }


    public void removeFilterModule(final ServletFilterModuleDescriptor descriptor) {
        filterDescriptors.remove(descriptor.getCompleteKey());
        filterMapper.put(descriptor.getCompleteKey(), null);

        final LazyReference<Filter> filterRef = filterRefs.remove(descriptor.getCompleteKey());
        if (filterRef != null) {
            filterRef.get().destroy();
        }
    }

    @Override
    public void addServlet(final Plugin plugin, final String servletName, final String className) {
        // create a module descriptor descriptor with the required class to instantiate
        final Element e = createServletModuleElement(servletName);
        e.addAttribute("class", className);

        // create the module; servlet will be instantiated on first use
        pluginControllerRef.get().addDynamicModule(plugin, e);
    }

    @Override
    public void addServlet(final Plugin plugin, final String servletName, final HttpServlet servlet, final ServletContext servletContext) {
        // create a module descriptor descriptor with no class
        final Element e = createServletModuleElement(servletName);

        // create the module
        final ModuleDescriptor moduleDescriptor = pluginControllerRef.get().addDynamicModule(plugin, e);
        if (!(moduleDescriptor instanceof ServletModuleDescriptor)) {
            throw new PluginException("expected com.atlassian.plugin.PluginController#addDynamicModule(com.atlassian.plugin.Plugin, org.dom4j.Element)} to return an instance of com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor; a " + (moduleDescriptor == null ? null : moduleDescriptor.getClass()) + " was returned");
        }

        // add a reference to the servlet given; will be initialised on first use
        LazyLoadedServletReference servletRef = new LazyLoadedServletReference((HttpServlet) servlet, (ServletModuleDescriptor) moduleDescriptor, servletContext);
        if (servletRefs.putIfAbsent(moduleDescriptor.getCompleteKey(), servletRef) != null) {
            pluginControllerRef.get().removeDynamicModule(plugin, moduleDescriptor);
            throw new IllegalStateException("a servlet with atlassian-plugins module key '" + moduleDescriptor.getCompleteKey() + "' has already been registered");
        }
    }

    @PluginEventListener
    public void onPluginFrameworkStartingEvent(final PluginFrameworkStartedEvent event) {
        pluginControllerRef.set(event.getPluginController());
    }

    @PluginEventListener
    public void onPluginFrameworkShutdownEvent(final PluginFrameworkShutdownEvent event) {
        if (pluginControllerRef.getAndSet(null) != event.getPluginController()) {
            log.warn("PluginController passed via the PluginFrameworkShutdownEvent did not match that passed via PluginFrameworkStartedEvent");
        }
    }

    /**
     * Call the plugins servlet context listeners contextDestroyed methods and cleanup any servlet contexts that are associated with
     * the plugin that was disabled.
     */
    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        final Plugin plugin = event.getPlugin();
        final ContextLifecycleReference context = pluginContextRefs.remove(plugin);
        if (context == null) {
            return;
        }

        context.get().contextDestroyed();
    }

    @PluginEventListener
    public void onPluginFrameworkBeforeShutdown(final PluginFrameworkShuttingDownEvent event) {
        destroy();
    }

    private void destroy() {
        destroyModuleDescriptors(servletDescriptors);

        destroyModuleDescriptors(filterDescriptors);

        // Defensively copy the collection we are going to iterate over, because we call out to listeners, and they
        // might call back (as happens with the ModuleDescriptor case).
        for (final ContextLifecycleReference context : new ArrayList<ContextLifecycleReference>(pluginContextRefs.values())) {
            if (context != null) {
                ContextLifecycleManager lifecycleManager = context.get();
                if (lifecycleManager != null) {
                    lifecycleManager.contextDestroyed();
                }
            }
        }
        pluginContextRefs.clear();
    }

    private <T extends ModuleDescriptor> void destroyModuleDescriptors(Map<String, T> descriptors) {
        // The ModuleDescriptor.destroy() can call back and modify the collection we wish to iterate over,
        // so iterate over a copy. Since we need to be null safe (preserving historical behaviour), we use
        // ArrayList rather than an ImmutableList.
        for (final ModuleDescriptor moduleDescriptor : new ArrayList<T>(descriptors.values())) {
            if (moduleDescriptor != null) {
                moduleDescriptor.destroy();
            }
        }
        descriptors.clear();
    }

    @Deprecated
    public void onPluginFrameworkShutdown(final PluginFrameworkShutdownEvent event) {
        destroy();
    }

    /**
     * Returns a wrapped Servlet for the servlet module. If a wrapped servlet for the module has not been created yet, we create one
     * using the servletConfig.
     * <p/>
     * Note: We use a map of lazily loaded references to the servlet so that only one can ever be created and initialized for each
     * module descriptor.
     */
    HttpServlet getServlet(final ServletModuleDescriptor descriptor, final ServletConfig servletConfig) {
        return getInstance(servletRefs, descriptor, new LazyLoadedServletReference(null, descriptor, servletConfig.getServletContext()));

    }

    /**
     * Returns a wrapped Filter for the filter module. If a wrapped filter for the module has not been created yet, we create one
     * using the filterConfig.
     * <p/>
     * Note: We use a map of lazily loaded references to the filter so that only one can ever be created and initialized for each
     * module descriptor.
     *
     * @return The filter, or null if the filter is invalid and should be removed
     */
    Filter getFilter(final ServletFilterModuleDescriptor descriptor, final FilterConfig filterConfig) {
        return getInstance(filterRefs, descriptor, new LazyLoadedFilterReference(descriptor, filterConfig));
    }

    private <T> T getInstance(ConcurrentMap<String, LazyReference<T>> refs, AbstractModuleDescriptor descriptor,
                              LazyReference<T> newRef) {
        try {
            final LazyReference<T> oldRef = refs.putIfAbsent(descriptor.getCompleteKey(), newRef);
            return oldRef != null ? oldRef.get() : newRef.get();
        } catch (final RuntimeException ex) {
            log.error("Unable to create new reference " + newRef, ex);
            return null;
        }

    }

    /**
     * Returns a wrapped ServletContext for the plugin. If a wrapped servlet context for the plugin has not been created yet, we
     * create using the baseContext, any context params specified in the plugin and initialize any context listeners the plugin may
     * define.
     * <p/>
     * Note: We use a map of lazily loaded references to the context so that only one can ever be created for each plugin.
     *
     * @param plugin      Plugin for whom we're creating a wrapped servlet context.
     * @param baseContext The applications base servlet context which we will be wrapping.
     * @return A wrapped, fully initialized servlet context that can be used for all the plugins filters and servlets.
     */
    private ServletContext getWrappedContext(final Plugin plugin, final ServletContext baseContext) {
        ContextLifecycleReference pluginContextRef = pluginContextRefs.get(plugin);
        if (pluginContextRef == null) {
            pluginContextRef = new ContextLifecycleReference(this, plugin, baseContext);
            if (pluginContextRefs.putIfAbsent(plugin, pluginContextRef) != null) {
                pluginContextRef = pluginContextRefs.get(plugin);
            }
        }
        return pluginContextRef.get().servletContext;
    }

    /**
     * Create an element that may be used to create a servlet module descriptor.
     * <p/>
     * The name passed is used for: <ul> <li>key, with "-servlet" appended</li> <li>name, with "Servlet" appended</li>
     * <li>url-pattern, with "/" prepended</li> </ul>
     *
     * @param servletName mandatory
     * @return valid Element
     */
    private Element createServletModuleElement(final String servletName) {
        final Element e = new DOMElement("servlet");
        e.addAttribute("key", servletName + "-servlet");
        e.addAttribute("name", servletName + "Servlet");

        Element url = new DOMElement("url-pattern");
        url.setText("/" + servletName);
        e.add(url);

        return e;
    }

    @VisibleForTesting
    ImmutableMap<String, LazyReference<HttpServlet>> getServletRefs() {
        return ImmutableMap.copyOf(servletRefs);
    }

    private final class LazyLoadedFilterReference extends LazyReference<Filter> {
        private final ServletFilterModuleDescriptor descriptor;
        private final FilterConfig filterConfig;

        private LazyLoadedFilterReference(final ServletFilterModuleDescriptor descriptor, final FilterConfig filterConfig) {
            this.descriptor = descriptor;
            this.filterConfig = filterConfig;
        }

        @Override
        protected Filter create() throws Exception {
            final Filter filter = filterFactory.newFilter(descriptor);
            final ServletContext servletContext = getWrappedContext(descriptor.getPlugin(), filterConfig.getServletContext());
            filter.init(new PluginFilterConfig(descriptor, servletContext));
            return filter;
        }

        @Override
        public String toString() {
            return Objects.toStringHelper(this)
                    .add("descriptor", descriptor)
                    .add("filterConfig", filterConfig)
                    .toString();
        }
    }

    @VisibleForTesting
    final class LazyLoadedServletReference extends LazyReference<HttpServlet> {
        private HttpServlet servlet;
        private final ServletModuleDescriptor descriptor;
        private final ServletContext servletContext;

        private LazyLoadedServletReference(final HttpServlet servlet, final ServletModuleDescriptor descriptor, final ServletContext servletContext) {
            this.servlet = servlet;
            this.descriptor = descriptor;
            this.servletContext = servletContext;
        }

        @Override
        protected HttpServlet create() throws Exception {
            // only create if a servlet wasn't supplied
            if (servlet == null) {
                servlet = new DelegatingPluginServlet(descriptor);
            }
            final ServletContext wrappedContext = getWrappedContext(descriptor.getPlugin(), servletContext);
            servlet.init(new PluginServletConfig(descriptor, wrappedContext));
            return servlet;
        }

        @Override
        public String toString() {
            return Objects.toStringHelper(this)
                    .add("descriptor", descriptor)
                    .add("servletContext", servletContext)
                    .toString();
        }
    }

    private static final class ContextLifecycleReference extends LazyReference<ContextLifecycleManager> {
        private final ServletModuleManager servletModuleManager;
        private final Plugin plugin;
        private final ServletContext baseContext;

        private ContextLifecycleReference(final ServletModuleManager servletModuleManager, final Plugin plugin, final ServletContext baseContext) {
            this.servletModuleManager = servletModuleManager;
            this.plugin = plugin;
            this.baseContext = baseContext;
        }

        @Override
        protected ContextLifecycleManager create() throws Exception {
            final ConcurrentMap<String, Object> contextAttributes = new ConcurrentHashMap<String, Object>();
            final Map<String, String> initParams = mergeInitParams(baseContext, plugin);
            final ServletContext context = new PluginServletContextWrapper(servletModuleManager, plugin, baseContext, contextAttributes, initParams);

            ClassLoaderStack.push(plugin.getClassLoader());
            final List<ServletContextListener> listeners = new ArrayList<ServletContextListener>();
            try {
                for (final ServletContextListenerModuleDescriptor descriptor : findModuleDescriptorsByType(ServletContextListenerModuleDescriptor.class, plugin)) {
                    listeners.add(descriptor.getModule());
                }
            } finally {
                ClassLoaderStack.pop();
            }

            return new ContextLifecycleManager(context, listeners);
        }

        private Map<String, String> mergeInitParams(final ServletContext baseContext, final Plugin plugin) {
            final Map<String, String> mergedInitParams = new HashMap<String, String>();
            @SuppressWarnings("unchecked")
            final Enumeration<String> e = baseContext.getInitParameterNames();
            while (e.hasMoreElements()) {
                final String paramName = e.nextElement();
                mergedInitParams.put(paramName, baseContext.getInitParameter(paramName));
            }
            for (final ServletContextParamModuleDescriptor descriptor : findModuleDescriptorsByType(ServletContextParamModuleDescriptor.class, plugin)) {
                mergedInitParams.put(descriptor.getParamName(), descriptor.getParamValue());
            }
            return Collections.unmodifiableMap(mergedInitParams);
        }
    }

    static <T extends ModuleDescriptor<?>> Iterable<T> findModuleDescriptorsByType(final Class<T> type, final Plugin plugin) {
        final Set<T> descriptors = new HashSet<T>();
        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            if (type.isAssignableFrom(descriptor.getClass())) {
                descriptors.add(type.cast(descriptor));
            }
        }
        return descriptors;
    }

    static final class ContextLifecycleManager {
        private final ServletContext servletContext;
        private final Iterable<ServletContextListener> listeners;

        ContextLifecycleManager(final ServletContext servletContext, final Iterable<ServletContextListener> listeners) {
            this.servletContext = servletContext;
            this.listeners = listeners;
            for (final ServletContextListener listener : listeners) {
                listener.contextInitialized(new ServletContextEvent(servletContext));
            }
        }

        ServletContext getServletContext() {
            return servletContext;
        }

        void contextDestroyed() {
            final ServletContextEvent event = new ServletContextEvent(servletContext);
            for (final ServletContextListener listener : listeners) {
                listener.contextDestroyed(event);
            }
        }
    }
}
