package com.atlassian.plugin.servlet;

import com.atlassian.plugin.Plugin;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;
import javax.servlet.http.HttpServlet;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * A wrapper around servlet context that allows plugin servlets to add
 * attributes which will not be shared/clobbered by other plugins.
 */
public class PluginServletContextWrapper implements ServletContext {
    private final ServletModuleManager servletModuleManager;
    private final Plugin plugin;
    private final ServletContext context;
    private final ConcurrentMap<String, Object> attributes;
    private final Map<String, String> initParams;

    private final Method methodGetContextPath;

    public PluginServletContextWrapper(ServletModuleManager servletModuleManager, Plugin plugin, ServletContext context, ConcurrentMap<String, Object> attributes, Map<String, String> initParams) {
        Method tmpMethod = null;
        this.servletModuleManager = servletModuleManager;
        this.plugin = plugin;
        this.context = context;
        this.attributes = attributes;
        this.initParams = initParams;

        Class<?> cls = context.getClass();
        try {
            tmpMethod = cls.getMethod("getContextPath");
        } catch (NoSuchMethodException e) {
            // no problem, Servlet 2.4 or earlier found
        }
        methodGetContextPath = tmpMethod;
    }

    /**
     * <p>Gets the named attribute.  The attribute is first looked for in the local
     * attribute map, if it is not found there it is looked for in the wrapped
     * contexts attribute map.  If it is not there, null is returned.</p>
     *
     * <p>A consequence of this ordering is that servlets may, in their own
     * context, override but not overwrite attributes from the wrapped context.</p>
     */
    @Override
    public Object getAttribute(String name) {
        Object attr = attributes.get(name);
        if (attr == null)
            attr = context.getAttribute(name);

        return attr;
    }

    /**
     * @return an enumeration of all the attributes from the wrapped
     * context as well as the local attributes.
     */
    @Override
    public Enumeration<String> getAttributeNames() {
        Collection<String> names = new HashSet<String>();
        names.addAll(attributes.keySet());
        names.addAll(Collections.list(context.getAttributeNames()));
        return Collections.enumeration(names);
    }

    /**
     * Removes an attribute from the local context.  Leaves the wrapped context
     * completely untouched.
     */
    @Override
    public void removeAttribute(String name) {
        attributes.remove(name);
    }

    /**
     * <p>Sets an attribute in the local attribute map, leaving the wrapped
     * context untouched.</p>
     *
     * <p>Servlets may use this and the lookup ordering of the
     * <code>getAttribute()</code> method to effectively override the value
     * of an attribute in the wrapped servlet context with their own value and
     * this overridden value will only be seen in the plugins own scope.</p>
     */
    @Override
    public void setAttribute(String name, Object object) {
        attributes.put(name, object);
    }

    /**
     * @return the init parameter from the servlet module descriptor.
     */
    @Override
    public String getInitParameter(String name) {
        return initParams.get(name);
    }

    /**
     * @return an enumeration of the init parameters from the servlet module
     * descriptor.
     */
    @Override
    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(initParams.keySet());
    }

    /**
     * @return the resource from the plugin classloader if it exists, otherwise the
     * resource is looked up from the wrapped context and returned
     */
    @Override
    public URL getResource(String path) throws MalformedURLException {
        URL url = plugin.getResource(path);
        if (url == null) {
            url = context.getResource(path);
        }
        return url;
    }

    /**
     * @return the resource stream from the plugin classloader if it exists, otherwise
     * the resource stream is attempted to be retrieved from the wrapped context
     */
    @Override
    public InputStream getResourceAsStream(String path) {
        InputStream in = plugin.getResourceAsStream(path);
        if (in == null) {
            in = context.getResourceAsStream(path);
        }
        return in;
    }

    /**
     * @return null so that servlet plugins can't escape their box
     */
    @Override
    public ServletContext getContext(String uripath) {
        return null;
    }

    @Override
    public String getContextPath() {

        // all this crap to deal with Servlet 2.4 containers better
        if (methodGetContextPath != null) {
            try {
                return (String) methodGetContextPath.invoke(context);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Cannot access this method", e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Unable to execute getContextPath()", e.getCause());
            }
        } else {
            throw new UnsupportedOperationException("This servlet context doesn't support 2.5 methods");
        }

    }

    //---- All methods below simply delegate to the wrapped servlet context ----

    @Override
    public int getMajorVersion() {
        return context.getMajorVersion();
    }

    @Override
    public String getMimeType(String file) {
        return context.getMimeType(file);
    }

    @Override
    public int getMinorVersion() {
        return context.getMinorVersion();
    }

    @Override
    public RequestDispatcher getNamedDispatcher(String name) {
        return context.getNamedDispatcher(name);
    }

    @Override
    public String getRealPath(String path) {
        return context.getRealPath(path);
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String path) {
        return context.getRequestDispatcher(path);
    }

    @Override
    public Set<String> getResourcePaths(String arg0) {
        return context.getResourcePaths(arg0);
    }

    @Override
    public String getServerInfo() {
        return context.getServerInfo();
    }

    @Override
    public Servlet getServlet(String name) throws ServletException {
        return context.getServlet(name);
    }

    @Override
    public String getServletContextName() {
        return context.getServletContextName();
    }

    @Override
    public Enumeration<String> getServletNames() {
        return context.getServletNames();
    }

    @Override
    public Enumeration<Servlet> getServlets() {
        return context.getServlets();
    }

    @Override
    public void log(Exception exception, String msg) {
        context.log(exception, msg);
    }

    @Override
    public void log(String message, Throwable throwable) {
        context.log(message, throwable);
    }

    @Override
    public void log(String msg) {
        context.log(msg);
    }

    //---- Servlet 3.0 methods ----

    @Override
    public boolean setInitParameter(String name, String value) {
        if (initParams.containsKey(name)) {
            return false;
        }
        initParams.put(name, value);
        return true;
    }

    @Override
    public int getEffectiveMajorVersion() {
        return context.getEffectiveMajorVersion();
    }

    @Override
    public int getEffectiveMinorVersion() {
        return context.getEffectiveMinorVersion();
    }

    @Override
    public SessionCookieConfig getSessionCookieConfig() {
        return context.getSessionCookieConfig();
    }

    @Override
    public void setSessionTrackingModes(Set<SessionTrackingMode> sessionTrackingModes) {
        context.setSessionTrackingModes(sessionTrackingModes);
    }

    @Override
    public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
        return context.getDefaultSessionTrackingModes();
    }

    @Override
    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
        return context.getEffectiveSessionTrackingModes();
    }

    @Override
    public JspConfigDescriptor getJspConfigDescriptor() {
        return context.getJspConfigDescriptor();
    }

    @Override
    public ClassLoader getClassLoader() {
        return context.getClassLoader();
    }

    @Override
    public void declareRoles(String... roleNames) {
        context.declareRoles(roleNames);
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String servletName, String className) {
        servletModuleManager.addServlet(plugin, servletName, className);
        return null;
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String servletName, Servlet servlet) {
        // atlassian-plugins limitation - HttpServlet only
        if (!(servlet instanceof HttpServlet)) {
            throw new IllegalArgumentException("only javax.servlet.http.HttpServlet is supported by atlassian-plugins for javax.servlet.ServletContext#addServlet(String, javax.servlet.Servlet)}");
        }

        servletModuleManager.addServlet(plugin, servletName, (HttpServlet) servlet, this);
        return null;
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String servletName, Class<? extends Servlet> servletClass) {
        servletModuleManager.addServlet(plugin, servletName, servletClass.getName());
        return null;
    }

    @Override
    public <T extends Servlet> T createServlet(Class<T> clazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ServletRegistration getServletRegistration(String servletName) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, ? extends ServletRegistration> getServletRegistrations() {
        throw new UnsupportedOperationException();
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String filterName, String className) {
        throw new UnsupportedOperationException();
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String filterName, Filter filter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String filterName, Class<? extends Filter> filterClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T extends Filter> T createFilter(Class<T> clazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public FilterRegistration getFilterRegistration(String filterName) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addListener(String className) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T extends EventListener> void addListener(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addListener(Class<? extends EventListener> listenerClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T extends EventListener> T createListener(Class<T> clazz) {
        throw new UnsupportedOperationException();
    }
}
