package com.atlassian.plugin.servlet.descriptors;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.servlet.ServletContextListener;

/**
 * Provides a way for plugins to declare {@link ServletContextListener}s so they can be notified when the
 * {@link javax.servlet.ServletContext} is created for the plugin.  Implementors need to extend this class and implement the
 * {#link autowireObject} method.
 *
 * @since 2.1.0
 */
@RequirePermission(Permissions.EXECUTE_JAVA)
public class ServletContextListenerModuleDescriptor extends AbstractModuleDescriptor<ServletContextListener> {
    protected static final Logger log = LoggerFactory.getLogger(ServletContextListenerModuleDescriptor.class);

    /**
     * @since 2.5.0
     */
    public ServletContextListenerModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        checkPermissions();
    }

    @Override
    public ServletContextListener getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }

}
