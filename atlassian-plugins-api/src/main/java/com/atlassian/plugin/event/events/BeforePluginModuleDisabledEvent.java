package com.atlassian.plugin.event.events;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * Event fired when a plugin module is about to be disabled, which can also happen when its
 * plugin is about to be disabled or uninstalled.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.0.5
 * @deprecated since 4.0.0. Use {@link PluginModuleDisablingEvent} instead.
 */
@Deprecated
public class BeforePluginModuleDisabledEvent extends PluginModulePersistentEvent {
    public BeforePluginModuleDisabledEvent(final ModuleDescriptor<?> module, final boolean persistent) {
        super(module, persistent);
    }
}
