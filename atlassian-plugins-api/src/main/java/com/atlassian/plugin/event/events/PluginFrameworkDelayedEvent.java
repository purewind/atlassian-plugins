package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework is delayed after early startup and is waiting
 * to commence late startup.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.2.0
 */
@PublicApi
public class PluginFrameworkDelayedEvent extends PluginFrameworkEvent {
    public PluginFrameworkDelayedEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
