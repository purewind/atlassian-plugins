package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired before a plugin is installed at runtime.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginInstallingEvent extends PluginEvent {
    public PluginInstallingEvent(final Plugin plugin) {
        super(plugin);
    }
}
