package com.atlassian.plugin;

import java.io.File;
import java.io.InputStream;
import java.util.Set;

/**
 * Allows the retrieval of files and/or an input stream of a plugin artifact. Implementations
 * must allow multiple calls to {@link #getInputStream()}.
 *
 * @see PluginController
 * @since 2.0.0
 */
public interface PluginArtifact {
    /**
     * @return true if the resource exists in this artifact, otherwise false
     * @since 2.2.0
     */
    boolean doesResourceExist(String name);

    /**
     * @return an input stream of the resource specified inside the artifact.  Null if the resource cannot be found.
     * @throws PluginParseException if the there was an exception retrieving the resource from the artifact
     */
    InputStream getResourceAsStream(String name) throws PluginParseException;

    /**
     * @return the original name of the plugin artifact file. Typically used
     * for persisting it to disk with a meaningful name.
     */
    String getName();

    /**
     * @return an InputStream for the entire plugin artifact. Calling this
     * multiple times will return a fresh input stream each time.
     */
    InputStream getInputStream();

    /**
     * @return the artifact as a file, or its underlying file if it is already one
     * @since 2.2.0
     */
    File toFile();

    /**
     * @return {@code true} if the plugin contains or references java executable code.
     * @since 3.0
     */
    boolean containsJavaExecutableCode();

    /**
     * @return {@code true} if the plugin contains Spring context files or instructions
     * @since 4.1
     */
    boolean containsSpringContext();

    /**
     * @return the ReferenceMode specifying whether or not this PluginArtifact may be reference installed.
     * @since 4.0.0
     */
    ReferenceMode getReferenceMode();

    /**
     * Additional interface for a plugin artifact which may support reference installation.
     *
     * @deprecated since 4.0.0, to be removed in 5.0.0: Equivalent functionality is now provided directly by
     * {@link PluginArtifact#getReferenceMode()}.
     */
    @Deprecated
    interface AllowsReference {
        /**
         * @see com.atlassian.plugin.ReferenceMode
         */
        enum ReferenceMode {
            /**
             * {@link AllowsReference#allowsReference} returns false.
             */
            FORBID_REFERENCE(com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE),

            /**
             * {@link AllowsReference#allowsReference} returns true.
             */
            PERMIT_REFERENCE(com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE);

            private final com.atlassian.plugin.ReferenceMode modern;

            private ReferenceMode(final com.atlassian.plugin.ReferenceMode modern) {
                this.modern = modern;
            }

            boolean allowsReference() {
                return toModern().allowsReference();
            }

            com.atlassian.plugin.ReferenceMode toModern() {
                return modern;
            }
        }

        /**
         * @see {@link PluginArtifact#getReferenceMode} and {@link com.atlassian.plugin.ReferenceMode#allowsReference}
         */
        boolean allowsReference();

        /**
         * Host to default implementation of {@link AllowsReference#allowsReference}.
         */
        class Default {
            /**
             * Forwards to {@link PluginArtifact#getReferenceMode} and {@link com.atlassian.plugin.ReferenceMode#allowsReference}.
             *
             * @param pluginArtifact the plugin artifact to check.
             * @return {@code pluginArtifact.getReferenceMode().allowsReference()}
             */
            public static boolean allowsReference(final PluginArtifact pluginArtifact) {
                return pluginArtifact.getReferenceMode().allowsReference();
            }
        }
    }

    interface HasExtraModuleDescriptors {
        /**
         * @return An Iterable containing the paths to any additional xml files found in scan folders
         * @since 3.2.16
         */
        Set<String> extraModuleDescriptorFiles(String rootFolder);
    }
}
