package com.atlassian.plugin;

import com.atlassian.fugue.Option;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The mode of installation of a plugin
 *
 * @since 3.0
 */
public enum InstallationMode {
    /**
     * Denotes a plugin installed from a remote plugin. This is the _proxy_ plugin created to represent and make the link
     * with the remote plugin.
     */
    REMOTE("remote"),

    /**
     * Denotes a plugin installed locally to the host application. This is the standard mode for plugins up to version 2.
     */
    LOCAL("local");

    private static final Logger LOGGER = LoggerFactory.getLogger(InstallationMode.class);

    private final String key;

    private InstallationMode(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static Option<InstallationMode> of(String name) {
        for (InstallationMode mode : values()) {
            if (mode.getKey().equalsIgnoreCase(name)) {
                return Option.some(mode);
            }
        }

        if (!Strings.isNullOrEmpty(name)) {
            LOGGER.warn("Could not match installation mode '{}' to any of existing {}. Ignoring.", name, values());
        }

        return Option.none();
    }
}
