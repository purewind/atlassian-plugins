package com.atlassian.plugin;

import com.atlassian.annotations.PublicApi;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;

/**
 * Represents the fully qualified key of a plugin module.
 * <p>
 * The complete key is the combination of the plugin key and the "simple" module key.
 * For example if the plugin key is "com.acme.myplugin" and the module key is "my-foo-module", then the complete key is
 * "com.acme.myplugin:my-foo-module".
 */
@PublicApi
public final class ModuleCompleteKey {
    @VisibleForTesting
    protected static final String SEPARATOR = ":";

    private final String pluginKey;
    private final String moduleKey;

    /**
     * Constructs a ModuleCompleteKey given the String representation of the complete key.
     * The String representation includes the plugin key, followed by a ':', followed by the simple module key.
     *
     * @param completeKey the String representation of the complete key
     */
    public ModuleCompleteKey(final String completeKey) {
        this(pluginKeyFromCompleteKey(completeKey), moduleKeyFromCompleteKey(completeKey));
    }

    /**
     * Constructs a ModuleCompleteKey given the separate plugin key and module key.
     *
     * @param pluginKey the plugin key
     * @param moduleKey the module key
     */
    public ModuleCompleteKey(final String pluginKey, final String moduleKey) {
        this.pluginKey = Strings.nullToEmpty(pluginKey).trim();

        if (!isValidKey(this.pluginKey)) {
            throw new IllegalArgumentException("Invalid plugin key specified: " + this.pluginKey);
        }

        this.moduleKey = Strings.nullToEmpty(moduleKey).trim();

        if (Strings.isNullOrEmpty(this.moduleKey)) // just validate that we have a non-empty module key
        {
            throw new IllegalArgumentException("Invalid module key specified: " + this.moduleKey);
        }
    }

    private boolean isValidKey(final String key) {
        return !Strings.isNullOrEmpty(Strings.nullToEmpty(key).trim()) && !key.contains(SEPARATOR);
    }

    /**
     * Returns the "simple" module key.
     * This is the second half of the "complete" key.
     *
     * @return the simple module key.
     */
    public String getModuleKey() {
        return moduleKey;
    }

    /**
     * Returns the plugin key.
     * This is the first half of the "complete" key.
     *
     * @return the plugin key.
     */
    public String getPluginKey() {
        return pluginKey;
    }

    /**
     * Returns the String representation of the complete key.
     * This includes the plugin key, followed by a ':', followed by the simple module key.
     *
     * @return the String representation of the complete key.
     */
    public String getCompleteKey() {
        return pluginKey + SEPARATOR + moduleKey;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ModuleCompleteKey that = (ModuleCompleteKey) o;

        if (!moduleKey.equals(that.moduleKey)) {
            return false;
        }
        if (!pluginKey.equals(that.pluginKey)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = pluginKey.hashCode();
        result = 31 * result + moduleKey.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getCompleteKey();
    }

    @VisibleForTesting
    static String pluginKeyFromCompleteKey(final String completeKey) {
        if (completeKey != null) {
            return completeKey.split(SEPARATOR)[0];
        }
        return "";
    }

    @VisibleForTesting
    static String moduleKeyFromCompleteKey(final String completeKey) {
        if (completeKey != null) {
            final String[] split = completeKey.split(SEPARATOR, 2);
            if (split.length == 2) {
                return split[1];
            }
        }
        return "";
    }
}
