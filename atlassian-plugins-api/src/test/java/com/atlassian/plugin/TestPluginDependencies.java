package com.atlassian.plugin;

import com.atlassian.plugin.PluginDependencies.Type;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestPluginDependencies {
    private PluginDependencies deps;

    @Before
    public void setUp() throws Exception {
        deps = PluginDependencies.builder()
                .withDynamic("A")
                .withDynamic("B")
                .withDynamic("C", "D")
                .withOptional("E")
                .withOptional("F")
                .withOptional("G", "H")
                .withMandatory("I")
                .withMandatory("J")
                .withMandatory("K", "L")
                .withDynamic("X")
                .withMandatory("X")
                .withOptional("X")
                .build();
    }

    @Test
    public void testGetMandatory() throws Exception {
        assertThat(deps.getMandatory(), is(equalTo((Set) ImmutableSet.of("I", "J", "K", "L", "X"))));
    }

    @Test
    public void testGetOptional() throws Exception {
        assertThat(deps.getOptional(), is(equalTo((Set) ImmutableSet.of("E", "F", "G", "H", "X"))));
    }

    @Test
    public void testGetDynamic() throws Exception {
        assertThat(deps.getDynamic(), is(equalTo((Set) ImmutableSet.of("A", "B", "C", "D", "X"))));
    }

    @Test
    public void testGetAll() throws Exception {
        assertThat(deps.getAll(), is(equalTo((Set) ImmutableSet.of(
                "A", "B", "C", "D",
                "E", "F", "G", "H",
                "I", "J", "K", "L", "X"
        ))));
    }

    @Test
    public void testGetByPluginKey() throws Exception {
        Multimap<String, Type> actual = deps.getByPluginKey();
        Multimap<String, Type> expected = ImmutableMultimap.<String, Type>builder()
                .put("A", DYNAMIC)
                .put("B", DYNAMIC)
                .put("C", DYNAMIC)
                .put("D", DYNAMIC)
                .put("X", DYNAMIC)
                .put("E", OPTIONAL)
                .put("F", OPTIONAL)
                .put("G", OPTIONAL)
                .put("H", OPTIONAL)
                .put("X", OPTIONAL)
                .put("I", MANDATORY)
                .put("J", MANDATORY)
                .put("K", MANDATORY)
                .put("L", MANDATORY)
                .put("X", MANDATORY)
                .build();

        assertThat(actual.entries(), containsInAnyOrder(expected.entries().toArray()));
    }
}