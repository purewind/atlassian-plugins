package com.atlassian.plugin;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TestModuleDescriptor {
    ModuleDescriptor moduleDescriptor = Mockito.mock(ForwardDefaultMethodsModuleDescriptor.class);

    @Before
    public void setUp() throws Exception {
        Mockito.doCallRealMethod().when(moduleDescriptor).setBroken();
        Mockito.doCallRealMethod().when(moduleDescriptor).isBroken();
    }

    @Test
    public void notBrokenByDefault() {
        assertThat(moduleDescriptor.isBroken(), is(false));
    }

    @Test
    public void setBrokenIsNoop() {
        moduleDescriptor.setBroken();
        assertThat(moduleDescriptor.isBroken(), is(false));
    }

    static abstract class ForwardDefaultMethodsModuleDescriptor implements ModuleDescriptor {
        @Override
        public void setBroken() {
            ModuleDescriptor.super.setBroken();
        }

        @Override
        public boolean isBroken() {
            return ModuleDescriptor.super.isBroken();
        }
    }
}
