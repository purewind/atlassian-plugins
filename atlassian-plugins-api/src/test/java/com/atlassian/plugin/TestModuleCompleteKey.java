package com.atlassian.plugin;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestModuleCompleteKey {

    @Test
    public void spacesAreStrippedFromKeys() {
        String pluginKey = "    com.acme  ";
        String moduleKey = "   tools  ";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(pluginKey, moduleKey);

        assertEquals(pluginKey.trim(), moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey.trim(), moduleCompleteKey.getModuleKey());
    }

    @Test
    public void spacesAreStrippedFromKeys2() {
        String moduleCompleteKeyString = "    com.acme  :         tools       ";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(moduleCompleteKeyString);

        assertEquals("com.acme", moduleCompleteKey.getPluginKey());
        assertEquals("tools", moduleCompleteKey.getModuleKey());
    }

    @Test
    public void pluginKeyAndModuleKeyConstructor() {
        String pluginKey = "com.acme";
        String moduleKey = "tools";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(pluginKey, moduleKey);

        assertEquals(pluginKey, moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey, moduleCompleteKey.getModuleKey());
    }

    @Test
    public void moduleCompleteKeyCanBeUsedAsMapKey() {
        Map<ModuleCompleteKey, String> map = new HashMap<>();
        String expected = "bananas";

        map.put(new ModuleCompleteKey("foo", "bar"), expected);

        assertEquals(expected, map.get(new ModuleCompleteKey("foo", "bar")));
    }

    @Test
    public void equals() {
        assertTrue(new ModuleCompleteKey("foo", "bar").equals(new ModuleCompleteKey("foo", "bar")));
        assertFalse(new ModuleCompleteKey("foo", "bar").equals(new ModuleCompleteKey("foo", "baz")));
        assertFalse(new ModuleCompleteKey("fon", "bar").equals(new ModuleCompleteKey("foo", "baz")));
    }

    @Test
    public void testToString() {
        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey("foo", "bar");

        assertEquals(moduleCompleteKey.getCompleteKey(), moduleCompleteKey.toString());
    }

    @Test
    public void workingKey() {
        ModuleCompleteKey key = new ModuleCompleteKey("foo:bar");

        assertEquals("foo", key.getPluginKey());
        assertEquals("bar", key.getModuleKey());
        assertEquals("foo:bar", key.getCompleteKey());
    }

    @Test(expected = IllegalArgumentException.class)
    public void colonInPluginKey() {
        new ModuleCompleteKey("foo:", "bar");
    }

    @Test
    public void colonInModuleKeyDoesNotThrowException() {
        final ModuleCompleteKey completeKey = new ModuleCompleteKey("foo", ":bar");
        assertEquals("foo", completeKey.getPluginKey());
        assertEquals(":bar", completeKey.getModuleKey());
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey1() {
        new ModuleCompleteKey("foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey2() {
        new ModuleCompleteKey("foo:");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey3() {
        new ModuleCompleteKey(":foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey4() {
        new ModuleCompleteKey("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey5() {
        new ModuleCompleteKey(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPluginKey() {
        new ModuleCompleteKey(null, "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullModuleKey() {
        new ModuleCompleteKey("foo", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPluginKeyAndNullModuleKey() {
        new ModuleCompleteKey(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyPluginKey() throws Exception {
        new ModuleCompleteKey("", "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyModuleKey() throws Exception {
        new ModuleCompleteKey("foo", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyPluginKeyAndEmptyModuleKey() throws Exception {
        new ModuleCompleteKey("", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void pluginKeyOfSpaces() throws Exception {
        new ModuleCompleteKey("      ", "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void moduleKeyOfSpaces() throws Exception {
        new ModuleCompleteKey("foo", "      ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void pluginKeyAndModuleKeyOfSpaces() throws Exception {
        new ModuleCompleteKey("       ", "      ");
    }

    @Test
    public void pluginModuleKeyCanHaveAColonPresent() {
        String pluginKey = "com.acme";
        String moduleKey = "tools:foo";
        String completeKey = "com.acme:tools:foo";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(completeKey);

        assertEquals(pluginKey, moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey, moduleCompleteKey.getModuleKey());
    }

    @Test
    public void pluginKeyFromCompleteKeyNull() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey(null), is(""));
    }

    @Test
    public void pluginKeyFromCompleteKeyNoSeparator() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("blargh"), is("blargh"));
    }

    @Test
    public void pluginKeyFromCompleteKeySeparatorMid() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("bla:rgh"), is("bla"));
    }

    @Test
    public void pluginKeyFromCompleteKeySeparatorEnd() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("blargh:"), is("blargh"));
    }

    @Test
    public void moduleKeyFromCompleteKeyNull() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey(null), is(""));
    }

    @Test
    public void moduleKeyFromCompleteKeyNoSeparator() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("blargh"), is(""));
    }

    @Test
    public void moduleKeyFromCompleteKeySeparatorMid() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("bla:rgh"), is("rgh"));
    }

    @Test
    public void moduleKeyFromCompleteKeySeparatorEnd() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("blargh:"), is(""));
    }
}