package com.atlassian.plugin.validation;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.removeAllNamespaces;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * Reads information from the schema of plugin descriptors (dynamic per instance)
 *
 * @since 3.0.0
 */
public final class SchemaReader {
    private Document schema;

    public SchemaReader(Document schema) {
        this.schema = removeAllNamespaces(checkNotNull(schema));
    }

    public Set<String> getAllowedPermissions() {
        return copyOf(transform(getPermissionElements(), new Function<Element, String>() {
            @Override
            public String apply(Element permission) {
                return permission.attributeValue("value");
            }
        }));
    }

    public Map<String, Set<String>> getModulesRequiredPermissions() {
        final List<Element> modulesDefinitions = selectNodes("//complexType[@name='AtlassianPluginType']/sequence/choice/element");
        ImmutableMap.Builder<String, Set<String>> permissions = ImmutableMap.builder();
        for (Element modulesDefinition : modulesDefinitions) {
            permissions.put(modulesDefinition.attributeValue("name"), getModuleRequiredPermissions(modulesDefinition));
        }
        return permissions.build();
    }

    private Set<String> getModuleRequiredPermissions(Element modulesDefinition) {
        return copyOf(transform(selectNodes(modulesDefinition, "annotation//required-permissions/permission"), new Function<Element, String>() {
            @Override
            public String apply(Element permission) {
                return permission.getTextTrim();
            }
        }));
    }

    private List<Element> getPermissionElements() {
        return selectNodes("//simpleType[@name='PermissionValueType']//enumeration");
    }

    private List<Element> selectNodes(String xpathExpression) {
        return selectNodes(schema, xpathExpression);
    }

    @SuppressWarnings("unchecked")
    private static List<Element> selectNodes(Node node, String xpathExpression) {
        return node.selectNodes(xpathExpression);
    }
}
