package com.atlassian.plugin.validation;

import org.dom4j.Document;

import java.io.IOException;
import java.io.InputStream;

final class ResourcesLoader {
    static Document getTestDocument(String name) throws IOException {
        try (InputStream inputStream = getInput(name)) {
            return Dom4jUtils.readDocument(inputStream);
        }
    }

    static InputStream getInput(final String name) {
        return ResourcesLoader.class.getResourceAsStream(name);
    }
}
