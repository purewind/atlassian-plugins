package com.atlassian.plugin.main;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.BundledPluginLoader;
import com.atlassian.plugin.loaders.ClassPathPluginLoader;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.manager.DefaultPluginManager;
import com.atlassian.plugin.manager.PluginRegistryImpl;
import com.atlassian.plugin.manager.ProductPluginAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager;
import com.atlassian.plugin.osgi.factory.OsgiBundleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPluginFactory;
import com.atlassian.plugin.osgi.factory.RemotablePluginFactory;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Facade interface to the Atlassian Plugins framework.  See the package Javadocs for usage information.
 */
public class AtlassianPlugins {
    private OsgiContainerManager osgiContainerManager;
    private DefaultPluginEventManager pluginEventManager;
    private PluginAccessor pluginAccessor;
    private PluginController pluginController;
    private SplitStartupPluginSystemLifecycle pluginSystemLifecycle;
    private HotDeployer hotDeployer;
    private EventPublisher eventPublisher;

    /**
     * Suffix for temporary directories which will be removed on shutdown
     */
    public static final String TEMP_DIRECTORY_SUFFIX = ".tmp";

    /**
     * Constructs an instance of the plugin framework with the specified config.  No additional validation is performed on
     * the configuration, so it is recommended you use the {@link PluginsConfigurationBuilder} class to create a
     * configuration instance.
     *
     * @param config The plugins configuration to use
     */
    public AtlassianPlugins(final PluginsConfiguration config) {
        pluginEventManager = new DefaultPluginEventManager();
        eventPublisher = pluginEventManager.getEventPublisher();

        final AtomicReference<PluginAccessor> pluginAccessorRef = new AtomicReference<PluginAccessor>();
        osgiContainerManager = new FelixOsgiContainerManager(
                config.getFrameworkBundleDirectory(),
                config.getOsgiPersistentCache(),
                config.getPackageScannerConfiguration(),
                new CriticalHostComponentProvider(config.getHostComponentProvider(), pluginEventManager, pluginAccessorRef),
                pluginEventManager);

        final Set<Application> applications = config.getApplication() != null ?
                Sets.newHashSet(config.getApplication()) : ImmutableSet.<Application>of();

        // plugin factories/deployers
        final OsgiPluginFactory osgiPluginDeployer = new OsgiPluginFactory(
                config.getPluginDescriptorFilename(),
                applications,
                config.getOsgiPersistentCache(),
                osgiContainerManager,
                pluginEventManager);

        final OsgiBundleFactory osgiBundleDeployer = new OsgiBundleFactory(osgiContainerManager);

        final RemotablePluginFactory remotablePluginFactory = new RemotablePluginFactory(
                config.getPluginDescriptorFilename(),
                applications,
                osgiContainerManager,
                pluginEventManager);

        final List<PluginFactory> pluginDeployers = new LinkedList<PluginFactory>(Arrays.asList(osgiPluginDeployer, osgiBundleDeployer, remotablePluginFactory));
        if (config.isUseLegacyDynamicPluginDeployer()) {
            pluginDeployers.add(new LegacyDynamicPluginFactory(config.getPluginDescriptorFilename()));
        }

        final List<PluginLoader> pluginLoaders = new ArrayList<PluginLoader>();

        // classpath plugins
        pluginLoaders.add(new ClassPathPluginLoader());

        // bundled plugins
        if (config.getBundledPluginUrl() != null) {
            pluginLoaders.add(new BundledPluginLoader(config.getBundledPluginUrl(), config.getBundledPluginCacheDirectory(), pluginDeployers, pluginEventManager));
        }

        // osgi/v2 and v3 plugins
        pluginLoaders.add(new DirectoryPluginLoader(config.getPluginDirectory(), pluginDeployers, pluginEventManager));

        PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();
        pluginAccessor = new ProductPluginAccessor(pluginRegistry, config.getPluginStateStore(), config.getModuleDescriptorFactory(), pluginEventManager);

        DefaultPluginManager defaultPluginManager = DefaultPluginManager.newBuilder()
                .withPluginRegistry(pluginRegistry)
                .withStore(config.getPluginStateStore())
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(config.getModuleDescriptorFactory())
                .withPluginEventManager(pluginEventManager)
                .withPluginAccessor(pluginAccessor)
                .build();

        pluginController = defaultPluginManager;
        pluginSystemLifecycle = defaultPluginManager;

        pluginAccessorRef.set(pluginAccessor);

        defaultPluginManager.setPluginInstaller(new FilePluginInstaller(config.getPluginDirectory()));

        if (config.getHotDeployPollingPeriod() > 0) {
            hotDeployer = new HotDeployer(pluginController, config.getHotDeployPollingPeriod());
        }
    }

    public void afterPropertiesSet() {
        pluginEventManager.register(this);
    }

    public void destroy() {
        pluginEventManager.unregister(this);
    }

    /**
     * Starts the plugins framework.
     *
     * @throws PluginParseException If there was any problems parsing any of the plugins
     * @deprecated Use {@link #afterPropertiesSet}, {@link #getPluginSystemLifecycle}.init().
     */
    @Deprecated
    public void start() throws PluginParseException {
        afterPropertiesSet();
        getPluginSystemLifecycle().init();
    }

    /**
     * Stops the framework.
     *
     * @deprecated Use {@link #getPluginSystemLifecycle}.shutdown(), {@link #destroy}.
     */
    @Deprecated
    public void stop() {
        getPluginSystemLifecycle().shutdown();
        destroy();
    }

    /**
     * @return the underlying OSGi container manager
     */
    public OsgiContainerManager getOsgiContainerManager() {
        return osgiContainerManager;
    }

    /**
     * @return the plugin event manager
     */
    public PluginEventManager getPluginEventManager() {
        return pluginEventManager;
    }

    /**
     * @return the event publisher used by plugin event manager
     */
    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    /**
     * @return the plugin controller for manipulating plugins
     */
    public PluginController getPluginController() {
        return pluginController;
    }

    /**
     * @return the plugin accessor for accessing plugins
     */
    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    /**
     * @return the plugin system lifecycle for starting and stopping the plugin system
     */
    public SplitStartupPluginSystemLifecycle getPluginSystemLifecycle() {
        return pluginSystemLifecycle;
    }


    @PluginEventListener
    public void onPluginFrameworkStartedEvent(final PluginFrameworkStartedEvent event) {
        if (hotDeployer != null && !hotDeployer.isRunning()) {
            hotDeployer.start();
        }
    }

    @PluginEventListener
    public void onPluginFrameworkShutdownEvent(final PluginFrameworkShutdownEvent event) {
        if (hotDeployer != null && hotDeployer.isRunning()) {
            hotDeployer.stop();
        }
    }

    private static class CriticalHostComponentProvider implements HostComponentProvider {
        private final HostComponentProvider delegate;
        private final PluginEventManager pluginEventManager;
        private final AtomicReference<PluginAccessor> pluginManagerRef;

        public CriticalHostComponentProvider(
                final HostComponentProvider delegate,
                final PluginEventManager pluginEventManager,
                final AtomicReference<PluginAccessor> pluginManagerRef) {
            this.delegate = delegate;
            this.pluginEventManager = pluginEventManager;
            this.pluginManagerRef = pluginManagerRef;
        }

        public void provide(final ComponentRegistrar registrar) {
            registrar.register(PluginEventManager.class).forInstance(pluginEventManager);
            registrar.register(PluginAccessor.class).forInstance(pluginManagerRef.get());
            delegate.provide(registrar);
        }
    }
}
