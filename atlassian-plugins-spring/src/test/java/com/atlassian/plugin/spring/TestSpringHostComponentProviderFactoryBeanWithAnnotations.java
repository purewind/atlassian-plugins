package com.atlassian.plugin.spring;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.PropertyBuilder;
import com.atlassian.plugin.osgi.hostcomponents.impl.DefaultComponentRegistrar;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import junit.framework.TestCase;
import org.aopalliance.aop.Advice;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.StaticListableBeanFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestSpringHostComponentProviderFactoryBeanWithAnnotations extends TestCase {
    public void testProvide() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new FooableBean(),
                "string", "hello"));
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("bean", list.get(0).getProperties().get("bean-name"));
        List<Class<?>> ifs = Arrays.asList(list.get(0).getMainInterfaceClasses());

        // Test locally declared interface
        assertTrue(ifs.contains(Fooable.class));

        // Test super interface of locally declared interface
        assertTrue(ifs.contains(Barable.class));

        // Test interface of super class
        assertTrue(ifs.contains(Map.class));
    }

    public void testProvideWithValuesParam() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new AnotherAnnotatedBean()));
        List<Class<?>> ifs = Arrays.asList(list.get(0).getMainInterfaceClasses());

        // Only explicitly specified interface
        assertEquals(Arrays.asList(Fooable.class), ifs);
    }

    public void testProvideWithInterfacesParam() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new SomeAnnotatedBean()));
        List<Class<?>> ifs = Arrays.asList(list.get(0).getMainInterfaceClasses());

        // Only explicitly specified interfaces
        assertEquals(Arrays.asList(Fooable.class, Serializable.class), ifs);
    }

    public void testProvideWithInterfacesTwoDifferentWaysProducesCombinedEffect() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new WeirdlyAnnotatedBean()));
        List<Class<?>> ifs = Arrays.asList(list.get(0).getMainInterfaceClasses());

        // The explicitly specified interfaces overrides 'value'.
        assertEquals(Arrays.asList(Barable.class, Fooable.class, Serializable.class, Map.class), ifs);
    }

    public void testProvideWithCCLStrategy() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new FooablePluginService()));
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("bean", list.get(0).getProperties().get(PropertyBuilder.BEAN_NAME));
        assertEquals(ContextClassLoaderStrategy.USE_PLUGIN.name(), list.get(0).getProperties().get(PropertyBuilder.CONTEXT_CLASS_LOADER_STRATEGY));
    }

    public void testProvideWithProxy() throws Exception {
        ProxyFactory pFactory = new ProxyFactory(new FooableBean());
        pFactory.addInterface(Fooable.class);
        pFactory.addAdvice(new Advice() {
        });

        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", pFactory.getProxy(),
                "string", "hello"));

        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("bean", list.get(0).getProperties().get("bean-name"));
        assertEquals(Fooable.class.getName(), list.get(0).getMainInterfaces()[0]);
    }

    public void testProvideOnlySingletonBeans() throws Exception {
        List<HostComponentRegistration> list = calculateHostComponentRegistrations(ImmutableMap.<String, Object>of("bean", new FooableBean(),
                        "bean2", new FooablePluginService()),
                ImmutableSet.<String>of("bean"));
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("bean2", list.get(0).getProperties().get("bean-name"));
    }

    private List<HostComponentRegistration> calculateHostComponentRegistrations(Map<String, Object> beanMap) throws Exception {
        return calculateHostComponentRegistrations(beanMap, Collections.<String>emptySet());
    }

    private List<HostComponentRegistration> calculateHostComponentRegistrations(Map<String, Object> beanMap, final Set<String> namesOfScopedBeans) throws Exception {
        StaticListableBeanFactory factory = new StaticListableBeanFactory() {
            @Override
            public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
                return !namesOfScopedBeans.contains(name);
            }
        };

        for (Map.Entry<String, Object> entry : beanMap.entrySet()) {
            factory.addBean(entry.getKey(), entry.getValue());
        }

        HostComponentProvider provider = getHostComponentProvider(factory);

        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        provider.provide(registrar);

        return registrar.getRegistry();
    }

    private HostComponentProvider getHostComponentProvider(BeanFactory factory) throws Exception {
        SpringHostComponentProviderFactoryBean providerFactoryBean = new SpringHostComponentProviderFactoryBean();
        SpringHostComponentProviderConfig config = new SpringHostComponentProviderConfig();
        config.setUseAnnotation(true);
        providerFactoryBean.setSpringHostComponentProviderConfig(config);
        providerFactoryBean.setBeanFactory(factory);
        providerFactoryBean.afterPropertiesSet();
        return (HostComponentProvider) providerFactoryBean.getObject();
    }
}
