package com.atlassian.plugin.spring;

import java.io.Serializable;
import java.util.HashMap;

@AvailableToPlugins(Fooable.class)
public class AnotherAnnotatedBean extends HashMap implements Fooable, Serializable {
    public void sayHi() {
        System.out.println("hi");
    }
}
