package com.atlassian.plugin.spring.pluginns;

import com.atlassian.plugin.spring.PluginBeanDefinitionRegistry;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.xml.BeanDefinitionDecorator;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;


public class PluginTrackBundleBeanDefinitionDecorator implements BeanDefinitionDecorator {

    public BeanDefinitionHolder decorate(Node source, BeanDefinitionHolder holder, ParserContext ctx) {
        final String trackBundleAsString = ((Attr) source).getValue();
        if (isTrackBundleEnabled(trackBundleAsString)) {
            new PluginBeanDefinitionRegistry(ctx.getRegistry()).addBundleTrackingBean(holder.getBeanName());
        }
        return holder;

    }

    private boolean isTrackBundleEnabled(String trackBundle) {
        return Boolean.parseBoolean(trackBundle);
    }
}