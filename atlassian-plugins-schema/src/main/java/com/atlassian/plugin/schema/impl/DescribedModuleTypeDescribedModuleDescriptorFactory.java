package com.atlassian.plugin.schema.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.schema.spi.Schema;
import com.atlassian.plugin.schema.spi.SchemaFactory;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singleton;

/**
 * Described module descriptor factory for internal use
 */
final class DescribedModuleTypeDescribedModuleDescriptorFactory<T extends ModuleDescriptor<?>> implements DescribedModuleDescriptorFactory {
    private final ContainerManagedPlugin plugin;
    private final String type;
    private final Iterable<String> typeList;
    private final Class<T> moduleDescriptorClass;
    private final SchemaFactory schemaFactory;

    /**
     * Constructs an instance using a specific host container
     *
     * @param type                  The type of module
     * @param moduleDescriptorClass The descriptor class
     * @param schemaFactory         the schema factory
     * @since 3.0.0
     */
    DescribedModuleTypeDescribedModuleDescriptorFactory(ContainerManagedPlugin plugin,
                                                        final String type,
                                                        final Class<T> moduleDescriptorClass,
                                                        SchemaFactory schemaFactory) {
        this.plugin = checkNotNull(plugin);
        this.moduleDescriptorClass = moduleDescriptorClass;
        this.type = type;
        this.schemaFactory = schemaFactory;
        this.typeList = singleton(type);
    }

    public ModuleDescriptor getModuleDescriptor(final String type) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        T result = null;
        if (this.type.equals(type)) {
            result = plugin.getContainerAccessor().createBean(moduleDescriptorClass);
        }
        return result;
    }

    public boolean hasModuleDescriptor(final String type) {
        return (this.type.equals(type));
    }

    @Override
    @Nullable
    public Schema getSchema(String type) {
        return (this.type.equals(type) ? schemaFactory.getSchema() : null);
    }

    @Override
    public Iterable<String> getModuleDescriptorKeys() {
        return typeList;
    }

    public Class<? extends ModuleDescriptor<?>> getModuleDescriptorClass(final String type) {
        return (this.type.equals(type) ? moduleDescriptorClass : null);
    }

    public Set<Class<? extends ModuleDescriptor>> getModuleDescriptorClasses() {
        return ImmutableSet.<Class<? extends ModuleDescriptor>>of(moduleDescriptorClass);
    }
}
