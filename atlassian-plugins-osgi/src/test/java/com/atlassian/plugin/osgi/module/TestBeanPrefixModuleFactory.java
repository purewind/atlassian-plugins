package com.atlassian.plugin.osgi.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import junit.framework.TestCase;

import java.io.InputStream;
import java.net.URL;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBeanPrefixModuleFactory extends TestCase {
    ModuleFactory moduleCreator;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        moduleCreator = new BeanPrefixModuleFactory();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreateBeanFailedUsingHostContainer() throws Exception {
        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);

        try {
            moduleCreator.createModule("someBean", moduleDescriptor);
            fail("Container not available for non osgi plugins. Bean creation should have failed");
        } catch (IllegalArgumentException e) {
            assertEquals("Failed to resolve 'someBean'. You cannot use 'bean' prefix with non spring plugins", e.getMessage());
        }
    }

    public void testCreateBeanUsingPluginContainer() throws Exception {
        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final ContainerAccessor pluginContainerAccessor = mock(ContainerAccessor.class);
        final Plugin plugin = new MockContainerManagedPlugin(pluginContainerAccessor);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        final Object pluginBean = new Object();
        when(pluginContainerAccessor.getBean("someBean")).thenReturn(pluginBean);
        final Object obj = moduleCreator.createModule("someBean", moduleDescriptor);
        verify(pluginContainerAccessor).getBean("someBean");
        assertEquals(obj, pluginBean);
    }

    private class MockContainerManagedPlugin extends AbstractPlugin implements ContainerManagedPlugin {
        private ContainerAccessor containerAccessor;

        public MockContainerManagedPlugin(ContainerAccessor containerAccessor) {
            super(null);
            this.containerAccessor = containerAccessor;
        }

        public ContainerAccessor getContainerAccessor() {
            return containerAccessor;
        }

        public boolean isUninstallable() {
            return false;
        }

        public boolean isDeleteable() {
            return false;
        }

        public boolean isDynamicallyLoaded() {
            return false;
        }

        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
            return (Class<T>) Class.forName(clazz);
        }

        public ClassLoader getClassLoader() {
            return null;
        }

        public URL getResource(final String path) {
            return null;
        }

        public InputStream getResourceAsStream(final String name) {
            return null;
        }
    }

}
