package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.test.PluginJarBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.osgi.framework.Constants;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;

/**
 * Verifies which of OSGiBundleFactory or OSGiPluginFactory processes a given plugin.
 * <p>
 * The products just walk a list of {@link PluginFactory} instances and select the first one for which
 * {@link PluginFactory#canCreate} is true. We don't mandate the order of this list, and it may vary from product to
 * product.
 * <p>
 * This test suite verifies that {@link OsgiBundleFactory} and {@link OsgiPluginFactory} select the correct plugins
 * regardless of the order of the calls, by verifying there are no intersections in the accepted plugins for each.
 *
 * @author pandronov
 */
@RunWith(Parameterized.class)
public class TestPluginFactorySelection {
    private static final String OSGI_BUNDLE = "OsgiBundleFactory";
    private static final String OSGI_PLUGIN = "OsgiPluginFactory";

    /*
     * Decision space can be described by three boolean variables that represent the different characteristics of
     * each plugin:
     * 1. Spring               - the plugin has a Spring context,
     * 2. Transformerless      - the plugin does not require transformation,
     * 3. atlassian-plugin.xml - the artifact has an atlassian plugin descriptor.
     * 
     * Here is a table describing decision space:
     * +--------+------------+-----------------+-------------------+
     * | Spring | plugin.xml | transformerless | result type       |
     * +--------+------------+-----------------+-------------------+
     * |   0    |     0      |       0         | OsgiBundlePlugin  | if OSGi bundle, i.e has SymbolicName
     * +--------+------------+-----------------+-------------------+
     * |   1    |     0      |       0         | OsgiBundlePlugin  | if OSGi bundle, i.e has SymbolicName
     * +--------+------------+-----------------+-------------------+
     * |   0    |     1      |       0         | OsgiPlugin        | Transformation adds a Spring context
     * +--------+------------+-----------------+-------------------+
     * |   1    |     1      |       0         | OsgiPlugin        |
     * +--------+------------+-----------------+-------------------+
     * |   0    |     0      |       1         | OsgiBundlePlugin  |
     * +--------+------------+-----------------+-------------------+
     * |   1    |     0      |       1         | OsgiPlugin        |
     * +--------+------------+-----------------+-------------------+
     * |   0    |     1      |       1         | OsgiBundlePlugin  |
     * +--------+------------+-----------------+-------------------+
     * |   1    |     1      |       1         | OsgiPlugin        |
     * +--------+------------+-----------------+-------------------+
     *
     * Note that to make table complete it is necessary to introduce one more variable - isOsgiBundle but
     * for two factories considered below, JAR implies to be a proper OSGi Bundle or a proper Atlassian Plugin,
     * so isBundle variable has no influence. There are only two cases when it is important (null expectation in
     * tests table below), all the rest should give same results no matter if it is an OSGi Bundle or no 
     */
    @Parameters(name = "test({0},{1},{2},{3})={4}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                // hasSpring | isPlugin | isOsgiBundle | isTransformless | expected |
                // ---------------------- Is a bundle
                {false, false, true, false, OSGI_BUNDLE},
                {true, false, true, false, OSGI_BUNDLE},
                {false, true, true, false, OSGI_PLUGIN},
                {true, true, true, false, OSGI_PLUGIN},
                {false, false, true, true, OSGI_BUNDLE},
                {true, false, true, true, OSGI_PLUGIN},
                {false, true, true, true, OSGI_BUNDLE},
                {true, true, true, true, OSGI_PLUGIN},
                // ---------------------- Not a bundle
                {false, false, false, false, null},
                {true, false, false, false, null},
                {false, true, false, false, OSGI_PLUGIN},
                {true, true, false, false, OSGI_PLUGIN},
                {false, false, false, true, OSGI_BUNDLE},
                {true, false, false, true, OSGI_PLUGIN},
                {false, true, false, true, OSGI_BUNDLE},
                {true, true, false, true, OSGI_PLUGIN},
        });
    }

    private final
    @Nonnull
    OsgiPluginFactory opFactory;
    private final
    @Nonnull
    OsgiBundleFactory obFactory;

    @Parameter(value = 0)
    public boolean hasSpring;

    @Parameter(value = 1)
    public boolean isPlugin;

    @Parameter(value = 2)
    public boolean isOsgiBundle;

    @Parameter(value = 3)
    public boolean isTransformless;

    @Parameter(value = 4)
    public String expected;

    public TestPluginFactorySelection() {
        OsgiContainerManager osgi = mock(OsgiContainerManager.class);
        this.opFactory = new OsgiPluginFactory(
                "atlassian-plugin.xml",
                Collections.<Application>emptySet(),
                mock(OsgiPersistentCache.class),
                osgi,
                mock(PluginEventManager.class)
        );
        this.obFactory = new OsgiBundleFactory(osgi);
    }

    @Test
    public void verificator() throws IOException {
        // Create artifact based on test parameters
        Map<String, String> manifest = new HashMap<>();
        manifest.put(Constants.BUNDLE_NAME, "TestPlugin");
        manifest.put(Constants.BUNDLE_VENDOR, "Cool Tests Inc.");
        manifest.put(Constants.BUNDLE_DESCRIPTION, "Test plugin");

        if (isOsgiBundle || isTransformless) {
            manifest.put(Constants.BUNDLE_VERSION, "1.0.0");
            manifest.put(Constants.BUNDLE_SYMBOLICNAME, "test-plugin");
        }

        if (isTransformless) {
            manifest.put("Atlassian-Plugin-Key", "test-plugin");
        }

        if (hasSpring) {
            manifest.put("Spring-Context", "*");
        }

        // Create plugin itself: most of the code in factories uses getResourceAsStream call
        // to calculate various characteristics of plugin. Because of that  it is hard to
        // create mock object
        PluginJarBuilder builder = new PluginJarBuilder("test-plugin.jar").manifest(manifest);
        if (isPlugin) {
            builder.addPluginInformation("test-plugin", "Test Plugin", "1.0.0");
        }

        // Backdate the jar so we can meaningfully test getDateInstalled vs getDateLoaded
        File bundleJar = builder.build();
        if (!bundleJar.setLastModified(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1))) {
            throw new IOException("Test broken, cannot backdate bundleJar '" + bundleJar + "'");
        }

        // Verify
        JarPluginArtifact ar = new JarPluginArtifact(bundleJar);
        if (expected == null) {
            assertThat("OsgiPlugin", opFactory.canCreate(ar), nullValue());
            assertThat("OsgiBundle", obFactory.canCreate(ar), nullValue());
        } else if (expected == OSGI_PLUGIN) {
            assertThat("OsgiPlugin", opFactory.canCreate(ar), notNullValue());
            assertThat("OsgiBundle", obFactory.canCreate(ar), nullValue());
        } else if (expected == OSGI_BUNDLE) {
            assertThat("OsgiPlugin", opFactory.canCreate(ar), nullValue());
            assertThat("OsgiBundle", obFactory.canCreate(ar), notNullValue());
        }
    }
}
