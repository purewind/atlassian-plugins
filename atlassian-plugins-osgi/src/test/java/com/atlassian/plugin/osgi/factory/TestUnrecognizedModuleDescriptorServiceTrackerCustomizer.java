package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.StubServletModuleDescriptor;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUnrecognizedModuleDescriptorServiceTrackerCustomizer {
    private PluginEventManager pluginEventManager;
    private OsgiPlugin plugin;
    private UnrecognizedModuleDescriptorServiceTrackerCustomizer instance;
    private Bundle bundle;
    private BundleContext bundleContext;

    @Before
    public void setUp() throws Exception {
        pluginEventManager = mock(PluginEventManager.class);
        plugin = mock(OsgiPlugin.class);
        bundle = mock(Bundle.class);
        when(plugin.getBundle()).thenReturn(bundle);
        bundleContext = mock(BundleContext.class);
        when(bundle.getBundleContext()).thenReturn(bundleContext);
        instance = new UnrecognizedModuleDescriptorServiceTrackerCustomizer(plugin, pluginEventManager);
    }

    @Test
    public void testGetModuleDescriptorsByDescriptorClass() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ServletModuleManager servletModuleManager = mock(ServletModuleManager.class);
        StubServletModuleDescriptor stubServletModuleDescriptor =
                new StubServletModuleDescriptor(moduleFactory, servletModuleManager);
        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(stubServletModuleDescriptor));

        List<ServletModuleDescriptor> result = instance.getModuleDescriptorsByDescriptorClass(
                ServletModuleDescriptor.class);
        assertEquals(Collections.<ServletModuleDescriptor>singletonList(stubServletModuleDescriptor), result);
    }

    @Test
    public void testGetModuleDescriptorsByDescriptorClassWithSubclass() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ServletModuleManager servletModuleManager = mock(ServletModuleManager.class);
        ServletModuleDescriptor servletModuleDescriptor =
                new ServletModuleDescriptor(moduleFactory, servletModuleManager);
        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(servletModuleDescriptor));

        List<StubServletModuleDescriptor> result = instance.getModuleDescriptorsByDescriptorClass(
                StubServletModuleDescriptor.class);
        assertTrue(result.isEmpty());
    }

    @Test
    public void unrecognisedModuleDescriptorIsRecognisedOnceAvailable() throws Exception {
        UnrecognisedModuleDescriptor unrecognised = new UnrecognisedModuleDescriptor();
        unrecognised.setKey("unique-key");
        unrecognised.setPlugin(plugin);
        Element elem = new DOMElement("not-initially-known");

        Map<String, Element> moduleElements = Collections.singletonMap("unique-key", elem);
        when(plugin.getModuleElements()).thenReturn(moduleElements);

        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(unrecognised));

        ListableModuleDescriptorFactory moduleDescriptorFactory = mock(ListableModuleDescriptorFactory.class);
        when(moduleDescriptorFactory.hasModuleDescriptor("not-initially-known")).thenReturn(true);
        ModuleDescriptor md = mock(ModuleDescriptor.class);
        when(moduleDescriptorFactory.getModuleDescriptor("not-initially-known")).thenReturn(md);

        ServiceReference serviceReference = mock(ServiceReference.class);
        when(bundleContext.getService(serviceReference)).thenReturn(moduleDescriptorFactory);
        instance.addingService(serviceReference);

        verify(md).init(plugin, elem);
        verify(plugin).addModuleDescriptor(md);
    }
}
