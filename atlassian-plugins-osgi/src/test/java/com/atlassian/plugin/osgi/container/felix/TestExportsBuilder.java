package com.atlassian.plugin.osgi.container.felix;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.impl.MockRegistration;
import com.atlassian.plugin.testpackage1.Dummy1;
import com.atlassian.plugin.util.PluginFrameworkUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.twdata.pkgscanner.DefaultOsgiVersionConverter;
import org.twdata.pkgscanner.ExportPackage;

import javax.management.DescriptorAccess;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.servlet.ServletContext;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.plugin.test.Matchers.fileNamed;
import static org.apache.commons.io.FileUtils.toFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestExportsBuilder {
    private static final String JAVA_VERSION_KEY = "java.specification.version";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public RestoreSystemProperties restoreSystemProperties =
            new RestoreSystemProperties(JAVA_VERSION_KEY, ExportsBuilder.getLegacyScanModeProperty());

    @Mock
    private ExportsBuilder.CachedExportPackageLoader loader;

    private DefaultPackageScannerConfiguration configuration;

    private ExportsBuilder builder;

    @Before
    public void setUp() {
        when(loader.load()).thenReturn(null);
        // We use the default here, because we want the test to get the default values
        configuration = new DefaultPackageScannerConfiguration();
        builder = new ExportsBuilder(loader);
    }

    @After
    public void tearDown() {
        builder = null;
    }

    @Test
    public void testDetermineExports() {
        final String exports = builder.determineExports(emptyRegistrations(), configuration);
        assertThat(exports, not(containsString(",,")));
    }

    @Test
    public void testDetermineExportsIncludeServiceInterfaces() {
        final List<HostComponentRegistration> registrations = ImmutableList.<HostComponentRegistration>of(
                new MockRegistration(new HashAttributeSet(), AttributeSet.class),
                new MockRegistration(new DefaultTableModel(), TableModel.class));
        final String imports = builder.determineExports(registrations, configuration);
        assertThat(imports, notNullValue());
        assertThat(imports, containsString(AttributeSet.class.getPackage().getName()));
        assertThat(imports, containsString("javax.swing.event"));
    }

    @Test
    public void testConstructJdkExportsWithJdk6And7() {
        setJavaVersion(ExportsBuilder.JDK_6);
        assertThat(exports(), containsString("javax.script"));

        setJavaVersion(ExportsBuilder.JDK_7);
        assertThat(exports(), containsString("javax.script"));
    }

    private static void setJavaVersion(final String jdkVersion) {
        System.setProperty(JAVA_VERSION_KEY, jdkVersion);
    }

    private String exports() {
        return builder.determineExports(emptyRegistrations(), configuration);
    }

    private List<HostComponentRegistration> emptyRegistrations() {
        return ImmutableList.of();
    }

    @Test
    public void testDetermineExportWhileConflictExists() {
        final DescriptorAccess descriptorAccess = mock(DescriptorAccess.class);

        final List<HostComponentRegistration> registrations = ImmutableList.<HostComponentRegistration>of(
                new MockRegistration(descriptorAccess, DescriptorAccess.class));

        configuration.setPackageVersions(ImmutableMap.of("javax.management", "1.2.3"));

        final String exports = builder.determineExports(registrations, configuration);

        int packageCount = 0;
        for (final String imp : exports.split("[,]")) {
            if (imp.split("[;]")[0].equals("javax.management")) {
                packageCount++;
            }
        }

        assertThat("even though the package is found twice, we must export it only once", packageCount, is(1));
        assertThat("found earlier always wins", exports, containsString(",javax.management,"));
    }

    @Test
    public void testPrecalculatedPackages() {
        when(loader.load()).thenReturn(new ExportsBuilder.PackageScannerExportsFileLoader("precalc-exports.xml").load());
        final String exports = builder.determineExports(emptyRegistrations(), configuration);
        assertThat(exports, containsString("bar;version=1"));
    }

    @Test
    public void testPackagesUnderPluginFrameworkExportedAsPluginFrameworkVersion() {
        configuration.setPackageVersions(ImmutableMap.of("com.atlassian.plugin.testpackage1", "98.76.54"));
        configuration.setPackageIncludes(ImmutableList.of("org.slf4j*"));

        final ImmutableList<HostComponentRegistration> registrations = ImmutableList.<HostComponentRegistration>of(
                new MockRegistration(new Dummy1() {
                }, Dummy1.class));
        final String exports = builder.determineExports(registrations, configuration);

        final String osgiVersionString = new DefaultOsgiVersionConverter().getVersion(PluginFrameworkUtils.getPluginFrameworkVersion());

        assertThat("packages under com.atlassian.plugin are exported as the framework version",
                exports, containsString("com.atlassian.plugin.testpackage1;version=" + osgiVersionString + ","));
        assertThat("packages under com.atlassian.plugin are exported as the framework version",
                exports, not(containsString("com.atlassian.plugin.testpackage1;version=98.76.54,")));
    }

    @Test
    public void defaultGenerateExportsFindsStandardLog4j() throws Exception {
        configuration.setServletContext(mockServletContext());
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));

        final Collection<ExportPackage> exports = builder.generateExports(configuration);

        assertThat(exports, notNullValue());
        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exports,
                Matchers.<ExportPackage>hasItem(isExportPackage("org.apache.log4j", "1.2.15", "log4j-1.2.15.jar")));
    }

    @Test
    public void generateExportsFallsThroughToServletContextScanning() throws Exception {
        configuration.setServletContext(mockServletContext());
        // Set up the excludes so that we don't find the slf4j jar in the class loader, which makes the code fallback to the
        // servlet context as mocked above, and we find log4j package in the test jar instead.
        configuration.setJarIncludes(Arrays.asList("testlog*", "mock*"));
        configuration.setJarExcludes(Arrays.asList("log4j*"));
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));

        final Collection<ExportPackage> exports = builder.generateExports(configuration);

        assertThat(exports, notNullValue());
        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exports,
                Matchers.<ExportPackage>hasItem(isExportPackage("org.apache.log4j", "1.2.15", "testlog-1.2.15.jar")));
    }

    @Test
    public void generateExportsFailsWhenFallbackServletContextScanningFails() throws Exception {
        configuration.setServletContext(mockServletContext());
        configuration.setJarIncludes(Arrays.asList("testlog4j23*"));
        configuration.setJarExcludes(Collections.<String>emptyList());
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));

        expectedException.expect(IllegalStateException.class);
        builder.generateExports(configuration);
    }

    @Test
    public void generateExportsFailsWhenFallbackAndNoServletContext() {
        // Test failure when no servlet context
        configuration.setJarIncludes(Arrays.asList("testlog4j23*"));
        configuration.setJarExcludes(Collections.<String>emptyList());
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));

        expectedException.expect(IllegalStateException.class);
        builder.generateExports(configuration);
    }

    @Test
    public void testGenerateExportsWithCorrectServletVersion() throws Exception {
        configuration.setServletContext(mockServletContext());
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));

        final Collection<ExportPackage> exports = builder.generateExports(configuration);

        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exports, Matchers.<ExportPackage>hasItem(isExportPackage("javax.servlet", "5.3.0")));
        //noinspection RedundantTypeArguments
        assertThat(exports, Matchers.<ExportPackage>hasItem(isExportPackage("javax.servlet.http", "5.3.0")));
    }

    private ServletContext mockServletContext() throws MalformedURLException {
        final ServletContext context = mock(ServletContext.class);
        when(context.getMajorVersion()).thenReturn(5);
        when(context.getMinorVersion()).thenReturn(3);
        final ClassLoader classLoader = getClass().getClassLoader();
        when(context.getResource("/WEB-INF/lib")).thenReturn(classLoader.getResource("scanbase/WEB-INF/lib"));
        when(context.getResource("/WEB-INF/classes")).thenReturn(classLoader.getResource("scanbase/WEB-INF/classes"));
        return context;
    }

    @Test
    public void testPackagesNotConsidedInPluginsItself() {
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.jira.not.in.plugins"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.osgi"), is(true));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.remotable.cheese"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.webresource"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.webresource.transformer"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.cache.filecache"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.cache.filecache.impl"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.web"), is(false));
        assertThat(ExportsBuilder.UNDER_PLUGIN_FRAMEWORK.apply("com.atlassian.plugin.web.conditions"), is(false));
    }

    @Test
    public void testJarWithImplicitDirectories() throws Exception {
        // If you want to test that this test is testing the right thing, it should fail if you enable legacy mode:
        // System.setProperty("com.atlassian.plugin.export.legacy.scan.mode", "true");
        // Also ensure that you've not left any detritus from repackaging the associated jar resource around, because
        // the scanner will find that stuff in the classpath and pick it up.

        final ImplicitDirectoriesTestHelper helper = new ImplicitDirectoriesTestHelper();
        final Collection<ExportPackage> exportPackages = helper.getExportPackages(builder);

        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exportPackages, Matchers.<ExportPackage>hasItem(helper.getLeafPackageMatcher()));
        //noinspection RedundantTypeArguments
        assertThat(exportPackages, not(Matchers.<ExportPackage>hasItem(helper.getIntermediatePackageMatcher())));
    }

    @Test
    public void testLegacyScanMode() throws Exception {
        // We want to test that setting the appropriate System property does, in fact, engage legacy mode. Unfortunately,
        // the only functional difference we can pick up on is the difference in bugs: legacy mode doesn't work with jars
        // with implicit directories, and non-legacy mode doesn't work with non-URLClassLoaders. Since we already have code
        // around the former, we go with that and accept the brittle. This has the added benefit of verifying that the test
        // isn't broken.

        System.setProperty(ExportsBuilder.getLegacyScanModeProperty(), "true");

        final ImplicitDirectoriesTestHelper helper = new ImplicitDirectoriesTestHelper();
        final Collection<ExportPackage> exportPackages = helper.getExportPackages(builder);

        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exportPackages, not(Matchers.<ExportPackage>hasItem(helper.getLeafPackageMatcher())));
        //noinspection RedundantTypeArguments
        assertThat(exportPackages, not(Matchers.<ExportPackage>hasItem(helper.getIntermediatePackageMatcher())));
    }

    @Test
    public void productSuppliedServletVersionIsRespected() throws Exception {
        configuration.setServletContext(mockServletContext());
        configuration.setPackageIncludes(Arrays.asList("javax.*", "org.*"));
        configuration.setPackageVersions(ImmutableMap.<String, String>builder().put("javax.servlet*", "4.6").build());

        final Collection<ExportPackage> exports = builder.generateExports(configuration);

        // The type arguments in Matchers.<ExportPackage>hasItem are not redundant in Java 1.6
        //noinspection RedundantTypeArguments
        assertThat(exports, Matchers.<ExportPackage>hasItem(isExportPackage("javax.servlet", "4.6.0")));
        //noinspection RedundantTypeArguments
        assertThat(exports, Matchers.<ExportPackage>hasItem(isExportPackage("javax.servlet.http", "4.6.0")));
    }

    /**
     * Common handling for tests using the implicit directory jar.
     */
    private static class ImplicitDirectoriesTestHelper {
        private static final String JAR_NAME = "implicitDirectories.jar";
        private static final String ROOT_PACKAGE = "com.atlassian.implicit";
        private static final String INTERMEDIATE_PACKAGE = ROOT_PACKAGE + ".intermediate";
        private static final String LEAF_PACKAGE = INTERMEDIATE_PACKAGE + ".leaf";
        private final File jarFile;
        private final ClassLoader classLoader;
        private final PackageScannerConfiguration configuration;

        ImplicitDirectoriesTestHelper() {
            final URL jarUrl = getClass().getClassLoader().getResource(JAR_NAME);
            jarFile = toFile(jarUrl);
            configuration = mock(PackageScannerConfiguration.class);
            when(configuration.getJarIncludes()).thenReturn(ImmutableList.of("*.jar"));
            when(configuration.getPackageIncludes()).thenReturn(ImmutableList.of("org.slf4j", ROOT_PACKAGE + ".*"));
            classLoader = new URLClassLoader(new URL[]{jarUrl});
        }

        Matcher<ExportPackage> getIntermediatePackageMatcher() {
            return isExportPackage(INTERMEDIATE_PACKAGE, null, jarFile);
        }

        Matcher<ExportPackage> getLeafPackageMatcher() {
            return isExportPackage(LEAF_PACKAGE, null, jarFile);
        }

        Collection<ExportPackage> getExportPackages(final ExportsBuilder exportsBuilder) {
            Collection<ExportPackage> exportPackages = null;
            final ClassLoader savedContextClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(classLoader);
                exportPackages = exportsBuilder.generateExports(configuration);
            } finally {
                Thread.currentThread().setContextClassLoader(savedContextClassLoader);
            }
            return exportPackages;
        }
    }

    /**
     * Obtain a matcher for a given package, version but ignore the file.
     */
    private static Matcher<ExportPackage> isExportPackage(final String packageName, final String version) {
        return new ExportPackageMatcher(packageName, version);
    }

    /**
     * Obtain a matcher for a given package, version and file name (ignoring the path).
     *
     * Useful for tests which expect to find a package in an some expected jar in the environment.
     */
    private static Matcher<ExportPackage> isExportPackage(
            final String packageName, final String version, final String locationName) {
        return new ExportPackageMatcher(packageName, version, locationName);
    }

    /**
     * Obtain a matcher for a given package, version and exact absolute file.
     *
     * Useful for tests which expect to find a package in an exact known jar in the test data.
     */
    private static Matcher<ExportPackage> isExportPackage(
            final String packageName, final String version, final File location) {
        return new ExportPackageMatcher(packageName, version, location);
    }

    /**
     * Matcher for an {@link ExportPackage} with given packageName, version, and optionally location.
     * <p/>
     * This is necessary to avoid hoops since {@link ExportPackage#equals} doesn't use location, and {@link
     * ExportPackage#compareTo} doesn't use location or version.
     */
    private static class ExportPackageMatcher extends TypeSafeMatcher<ExportPackage> {
        private final String packageName;
        private final String version;
        private final Matcher<File> locationMatcher;

        public ExportPackageMatcher(final String packageName, final String version) {
            this.packageName = packageName;
            this.version = version;
            this.locationMatcher = any(File.class);
        }

        public ExportPackageMatcher(final String packageName, final String version, final String locationName) {
            this.packageName = packageName;
            this.version = version;
            this.locationMatcher = fileNamed(locationName);
        }


        public ExportPackageMatcher(final String packageName, final String version, final File location) {
            this.packageName = packageName;
            this.version = version;
            this.locationMatcher = equalTo(location);
        }

        public boolean matchesSafely(final ExportPackage exportPackage) {
            return equalTo(packageName).matches(exportPackage.getPackageName())
                    && equalTo(version).matches(exportPackage.getVersion())
                    && locationMatcher.matches(exportPackage.getLocation());
        }

        public void describeTo(final Description description) {
            description.appendText("ExportPackage named ");
            description.appendValue(packageName);
            description.appendText(" with version ");
            description.appendValue(version);
            description.appendText(" and location ");
            locationMatcher.describeTo(description);
        }
    }
}
