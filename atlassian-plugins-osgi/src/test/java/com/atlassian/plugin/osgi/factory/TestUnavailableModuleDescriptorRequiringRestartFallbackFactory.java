package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptorRequiringRestart;
import com.atlassian.plugin.module.ModuleFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUnavailableModuleDescriptorRequiringRestartFallbackFactory {
    @Test
    public void doesNotInterceptForUnrecognisedModule() throws Exception {
        ModuleDescriptorFactory mdf = mock(ModuleDescriptorFactory.class);

        ModuleDescriptorFactory wrapper = new UnavailableModuleDescriptorRequiringRestartFallbackFactory(mdf);

        assertFalse(wrapper.hasModuleDescriptor("test"));
        assertEquals(null, wrapper.getModuleDescriptor("test"));
        assertEquals(null, wrapper.getModuleDescriptorClass("test"));
    }

    @Test
    public void returnsPlaceholderThatRequiresRestartWhenModuleRequiresRestart() throws Exception {
        ModuleDescriptorFactory mdf = mock(ModuleDescriptorFactory.class);
        when(mdf.hasModuleDescriptor("test")).thenReturn(true);
        when(mdf.getModuleDescriptorClass("test")).thenReturn((Class) RequiresRestartModuleDescriptor.class);

        ModuleDescriptorFactory wrapper = new UnavailableModuleDescriptorRequiringRestartFallbackFactory(mdf);

        assertTrue(wrapper.hasModuleDescriptor("test"));
        assertEquals(UnrecognisedModuleDescriptorRequiringRestart.class, wrapper.getModuleDescriptor("test").getClass());
        assertEquals(UnrecognisedModuleDescriptorRequiringRestart.class, wrapper.getModuleDescriptorClass("test"));
    }

    @RequiresRestart
    static class RequiresRestartModuleDescriptor extends AbstractModuleDescriptor<Void> {
        public RequiresRestartModuleDescriptor(ModuleFactory moduleFactory) {
            super(moduleFactory);
        }

        @Override
        public Void getModule() {
            return null;
        }
    }
}
