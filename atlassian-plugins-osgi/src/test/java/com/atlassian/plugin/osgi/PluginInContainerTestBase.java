package com.atlassian.plugin.osgi;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.hostcontainer.SimpleConstructorHostContainer;
import com.atlassian.plugin.loaders.BundledPluginLoader;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.manager.DefaultPluginManager;
import com.atlassian.plugin.manager.EnabledModuleCachingPluginAccessor;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.atlassian.plugin.manager.PluginRegistryImpl;
import com.atlassian.plugin.manager.ProductPluginAccessor;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.module.ClassPrefixModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.module.PrefixModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager;
import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.osgi.factory.OsgiBundleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPluginFactory;
import com.atlassian.plugin.osgi.factory.RemotablePluginFactory;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.hostcomponents.InstanceBuilder;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.atlassian.plugin.test.PluginTestUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Base for in-container unit tests
 */
@RunWith(Parameterized.class)
public abstract class PluginInContainerTestBase {
    protected FelixOsgiContainerManager osgiContainerManager;
    protected File tmpDir;
    protected File cacheDir;
    protected File pluginsDir;
    protected File frameworkBundlesDir;
    protected ModuleDescriptorFactory moduleDescriptorFactory;
    protected PluginController pluginController;
    protected PluginAccessor pluginAccessor;
    protected SplitStartupPluginSystemLifecycle pluginSystemLifecycle;
    protected PluginEventManager pluginEventManager;
    protected ModuleFactory moduleFactory;
    protected SimpleConstructorHostContainer hostContainer;

    private interface PluginManagerProvider {
        class InitialisedPluginManager {
            final PluginController pluginController;
            final PluginAccessor pluginAccessor;
            final SplitStartupPluginSystemLifecycle pluginSystemLifecycle;

            private InitialisedPluginManager(final PluginController pluginController,
                                             final PluginAccessor pluginAccessor,
                                             final SplitStartupPluginSystemLifecycle pluginSystemLifecycle) {
                this.pluginController = pluginController;
                this.pluginAccessor = pluginAccessor;
                this.pluginSystemLifecycle = pluginSystemLifecycle;
            }
        }

        InitialisedPluginManager provide(final ModuleDescriptorFactory moduleDescriptorFactory,
                                         final List<PluginLoader> loader,
                                         final PluginEventManager pluginEventManager,
                                         final File pluginsDir);
    }

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"DefaultPluginManager", new PluginManagerProvider() {
                    @Override
                    public InitialisedPluginManager provide(final ModuleDescriptorFactory moduleDescriptorFactory,
                                                            final List<PluginLoader> loader,
                                                            final PluginEventManager pluginEventManager,
                                                            final File pluginsDir) {
                        DefaultPluginManager pluginManager = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), loader, moduleDescriptorFactory, pluginEventManager);
                        pluginManager.setPluginInstaller(new FilePluginInstaller(pluginsDir));
                        return new InitialisedPluginManager(pluginManager, pluginManager, pluginManager);
                    }
                }},
                {"EnabledModuleCachingPluginAccessor", new PluginManagerProvider() {
                    @Override
                    public InitialisedPluginManager provide(final ModuleDescriptorFactory moduleDescriptorFactory,
                                                            final List<PluginLoader> loader,
                                                            final PluginEventManager pluginEventManager,
                                                            final File pluginsDir) {

                        DefaultPluginManager pluginManager = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), loader, moduleDescriptorFactory, pluginEventManager);
                        pluginManager.setPluginInstaller(new FilePluginInstaller(pluginsDir));
                        return new InitialisedPluginManager(
                                pluginManager,
                                new EnabledModuleCachingPluginAccessor(pluginManager, pluginEventManager, pluginManager),
                                pluginManager);
                    }
                }},
                {"ProductPluginAccessor", new PluginManagerProvider() {
                    @Override
                    public InitialisedPluginManager provide(final ModuleDescriptorFactory moduleDescriptorFactory,
                                                            final List<PluginLoader> loader,
                                                            final PluginEventManager pluginEventManager,
                                                            final File pluginsDir) {
                        PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();
                        PluginPersistentStateStore store = new MemoryPluginPersistentStateStore();

                        PluginAccessor pluginAccessor = new ProductPluginAccessor(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager);

                        DefaultPluginManager pluginManager = DefaultPluginManager.newBuilder()
                                .withPluginRegistry(pluginRegistry)
                                .withStore(store)
                                .withPluginEventManager(pluginEventManager)
                                .withPluginLoaders(loader)
                                .withModuleDescriptorFactory(moduleDescriptorFactory)
                                .withPluginAccessor(pluginAccessor)
                                .build();

                        pluginManager.setPluginInstaller(new FilePluginInstaller(pluginsDir));

                        return new InitialisedPluginManager(pluginManager, pluginAccessor, pluginManager);
                    }
                }},
        });
    }

    @Parameterized.Parameter(value = 0)
    public String testDescription;

    @Parameterized.Parameter(value = 1)
    public PluginManagerProvider pluginManagerProvider;

    @Before
    public void setUp() throws Exception {
        tmpDir = PluginTestUtils.createTempDirectory(this.getClass().getName() + testDescription);
        cacheDir = new File(tmpDir, "cache");
        cacheDir.mkdir();
        pluginsDir = new File(tmpDir, "plugins");
        pluginsDir.mkdir();
        this.pluginEventManager = new DefaultPluginEventManager();
        moduleFactory = new PrefixDelegatingModuleFactory(ImmutableSet.<PrefixModuleFactory>of(
                new ClassPrefixModuleFactory(hostContainer),
                new BeanPrefixModuleFactory()));
        hostContainer = createHostContainer(new HashMap<Class<?>, Object>());

        // dirty hack for running inside IDE, as the current directory is the project root, not the module
        if (new File("atlassian-plugins-osgi").exists()) {
            frameworkBundlesDir = new File(new File("atlassian-plugins-osgi", "target"), "framework-bundles");
        } else {
            frameworkBundlesDir = new File("target", "framework-bundles");
        }
        if (!frameworkBundlesDir.exists()) {
            throw new IllegalStateException(
                    "frameworkBundlesDir does not exist '" + frameworkBundlesDir.getAbsolutePath() + "', " +
                            "ensure that you have run a maven build up to process-resources");
        }
    }

    protected SimpleConstructorHostContainer createHostContainer(Map<Class<?>, Object> originalContext) {
        Map<Class<?>, Object> context = new HashMap<Class<?>, Object>(originalContext);
        context.put(ModuleFactory.class, moduleFactory);
        return new SimpleConstructorHostContainer(context);
    }

    @After
    public void tearDown() throws Exception {
        if (osgiContainerManager != null) {
            osgiContainerManager.stop();
            osgiContainerManager.clearExportCache(); // prevent export cache from being reused across tests
        }
        FileUtils.deleteDirectory(tmpDir);
        osgiContainerManager = null;
        tmpDir = null;
        pluginsDir = null;
        moduleDescriptorFactory = null;
        pluginEventManager = null;
        moduleFactory = null;
        hostContainer = null;
    }

    protected void initPluginManager() throws Exception {
        initPluginManager(null, new DefaultModuleDescriptorFactory(hostContainer));
    }

    protected void initPluginManager(final HostComponentProvider hostComponentProvider) throws Exception {
        initPluginManager(hostComponentProvider, new DefaultModuleDescriptorFactory(hostContainer));
    }

    protected void initPluginManager(final HostComponentProvider hostComponentProvider, final ModuleDescriptorFactory moduleDescriptorFactory, final String version)
            throws Exception {
        final PackageScannerConfiguration scannerConfig = buildScannerConfiguration(version);
        HostComponentProvider requiredWrappingProvider = getWrappingHostComponentProvider(hostComponentProvider);
        OsgiPersistentCache cache = new DefaultOsgiPersistentCache(cacheDir);
        osgiContainerManager = new FelixOsgiContainerManager(frameworkBundlesDir, cache, scannerConfig, requiredWrappingProvider, pluginEventManager);

        final LegacyDynamicPluginFactory legacyFactory = new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME, tmpDir);
        final OsgiPluginFactory osgiPluginDeployer = new OsgiPluginFactory(PluginAccessor.Descriptor.FILENAME, Collections.<Application>emptySet(), cache, osgiContainerManager, pluginEventManager);
        final OsgiBundleFactory osgiBundleFactory = new OsgiBundleFactory(osgiContainerManager);
        final RemotablePluginFactory remotablePluginFactory = new RemotablePluginFactory(PluginAccessor.Descriptor.FILENAME, Collections.<Application>emptySet(), osgiContainerManager, pluginEventManager);

        final DirectoryPluginLoader loader = new DirectoryPluginLoader(pluginsDir,
                ImmutableList.<PluginFactory>of(legacyFactory, osgiPluginDeployer, osgiBundleFactory, remotablePluginFactory),
                new DefaultPluginEventManager());

        initPluginManager(moduleDescriptorFactory, loader);
    }

    protected void initPluginManager(final HostComponentProvider hostComponentProvider, final ModuleDescriptorFactory moduleDescriptorFactory)
            throws Exception {
        initPluginManager(hostComponentProvider, moduleDescriptorFactory, (String) null);
    }

    protected void initPluginManager(final ModuleDescriptorFactory moduleDescriptorFactory, final List<PluginLoader> loader)
            throws Exception {
        this.moduleDescriptorFactory = moduleDescriptorFactory;
        PluginManagerProvider.InitialisedPluginManager initialisedPluginManager = pluginManagerProvider.provide(moduleDescriptorFactory, loader, pluginEventManager, pluginsDir);
        pluginController = initialisedPluginManager.pluginController;
        pluginAccessor = initialisedPluginManager.pluginAccessor;
        pluginSystemLifecycle = initialisedPluginManager.pluginSystemLifecycle;
        pluginSystemLifecycle.init();
    }

    protected void initPluginManager(final ModuleDescriptorFactory moduleDescriptorFactory, PluginLoader loader)
            throws Exception {
        initPluginManager(moduleDescriptorFactory, Arrays.<PluginLoader>asList(loader));
    }

    protected void initBundlingPluginManager(final ModuleDescriptorFactory moduleDescriptorFactory, File... bundledPluginJars) throws Exception {
        this.moduleDescriptorFactory = moduleDescriptorFactory;
        final PackageScannerConfiguration scannerConfig = buildScannerConfiguration("1.0");
        HostComponentProvider requiredWrappingProvider = getWrappingHostComponentProvider(null);
        OsgiPersistentCache cache = new DefaultOsgiPersistentCache(cacheDir);
        osgiContainerManager = new FelixOsgiContainerManager(frameworkBundlesDir, cache, scannerConfig, requiredWrappingProvider, pluginEventManager);

        final OsgiPluginFactory osgiPluginDeployer = new OsgiPluginFactory(PluginAccessor.Descriptor.FILENAME, Collections.<Application>emptySet(), cache, osgiContainerManager, pluginEventManager);

        final DirectoryPluginLoader loader = new DirectoryPluginLoader(pluginsDir, Arrays.<PluginFactory>asList(osgiPluginDeployer),
                new DefaultPluginEventManager());

        File zip = new File(bundledPluginJars[0].getParentFile(), "bundled-plugins.zip");
        for (File bundledPluginJar : bundledPluginJars) {
            ZipOutputStream stream = null;
            InputStream in = null;
            try {
                stream = new ZipOutputStream(new FileOutputStream(zip));
                in = new FileInputStream(bundledPluginJar);
                stream.putNextEntry(new ZipEntry(bundledPluginJar.getName()));
                IOUtils.copy(in, stream);
                stream.closeEntry();
            } catch (IOException ex) {
                IOUtils.closeQuietly(in);
                IOUtils.closeQuietly(stream);
            }
        }
        File bundledDir = new File(bundledPluginJars[0].getParentFile(), "bundled-plugins");
        final BundledPluginLoader bundledLoader = new BundledPluginLoader(zip.toURL(), bundledDir, Arrays.<PluginFactory>asList(osgiPluginDeployer),
                new DefaultPluginEventManager());

        initPluginManager(moduleDescriptorFactory, Arrays.<PluginLoader>asList(bundledLoader, loader));
    }

    private HostComponentProvider getWrappingHostComponentProvider(final HostComponentProvider hostComponentProvider) {
        HostComponentProvider requiredWrappingProvider = new HostComponentProvider() {
            public void provide(final ComponentRegistrar registrar) {

                if (hostComponentProvider != null) {
                    hostComponentProvider.provide(new ComponentRegistrar() {
                        public InstanceBuilder register(Class<?>... mainInterfaces) {
                            if (!Arrays.asList(mainInterfaces).contains(PluginEventManager.class)) {
                                return registrar.register(mainInterfaces);
                            }
                            return null;
                        }
                    });
                }
                registrar.register(ModuleFactory.class).forInstance(moduleFactory);
                registrar.register(PluginEventManager.class).forInstance(pluginEventManager);
                registrar.register(PluginAccessor.class).forInstance(pluginAccessor);
            }
        };
        return requiredWrappingProvider;
    }

    private PackageScannerConfiguration buildScannerConfiguration(String version) {
        final PackageScannerConfiguration scannerConfig = new DefaultPackageScannerConfiguration(version);
        scannerConfig.getPackageIncludes().add("com.atlassian.plugin*");
        scannerConfig.getPackageIncludes().add("javax.servlet*");
        scannerConfig.getPackageIncludes().add("com_cenqua_clover");
        scannerConfig.getPackageExcludes().add("com.atlassian.plugin.osgi.bridge*");
        scannerConfig.getPackageExcludes().add("com.atlassian.plugin.osgi.bridge.external");
        scannerConfig.getPackageExcludes().add("com.atlassian.plugin.osgi.spring.external");
        scannerConfig.getPackageVersions().put("org.apache.commons.logging", "1.1.3");
        return scannerConfig;
    }
}
