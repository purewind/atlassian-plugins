package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.XmlDescriptorParserFactory;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import org.dom4j.Element;
import org.osgi.framework.Constants;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getManifest;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getPluginKey;
import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.addModule;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Plugin deployer that deploys OSGi bundles that don't contain a (spring) context instantiated by the
 * plugin system
 */
public final class OsgiBundleFactory extends AbstractPluginFactory {
    private static final Logger log = LoggerFactory.getLogger(OsgiBundleFactory.class);

    /*
     * OSGi container to install plugin to 
     */
    private final OsgiContainerManager osgiContainerManager;

    /*
     * Atlassian plugin descriptor filename 
     */
    private final String pluginDescriptorFileName;

    /*
     * Build to create plugin specific ModuleFactories needs to parse descriptors (atlassian-plugin.xml)
     */
    private final OsgiChainedModuleDescriptorFactoryCreator osgiChainedModuleDescriptorFactoryCreator;

    public OsgiBundleFactory(final OsgiContainerManager osgi) {
        this(PluginAccessor.Descriptor.FILENAME, osgi);
    }

    public OsgiBundleFactory(final String pluginDescriptorFileName, final OsgiContainerManager osgi) {
        super(new XmlDescriptorParserFactory(), ImmutableSet.<Application>of());
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName);
        this.osgiContainerManager = checkNotNull(osgi, "The osgi container is required");

        this.osgiChainedModuleDescriptorFactoryCreator = new OsgiChainedModuleDescriptorFactoryCreator(new OsgiChainedModuleDescriptorFactoryCreator.ServiceTrackerFactory() {
            public ServiceTracker create(final String className) {
                return osgiContainerManager.getServiceTracker(className);
            }
        });
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact) {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion() {
        return new Predicate<Integer>() {
            @Override
            public boolean apply(final Integer input) {
                return input != null && input >= Plugin.VERSION_2;
            }
        };
    }

    /**
     * Determines if this deployer can handle this artifact by looking for the plugin descriptor
     *
     * <p> OsgiBundlePlugin wraps usual OSGi Bundle into Atlassian Plugin. Factory could create plugin for any
     * bundle that is not Spring powered even for those has not atlassian-plugin.xml. Note that some parts
     * of plugin descriptor, such as <component> or <component-import>, relies on IoC container in order to  work,
     * so without PluginContainerAccessor service exported it won't work and will throw an exception
     * </p>
     *
     * @param pluginArtifact The artifact to test
     * @return The plugin key, null if it cannot load the plugin
     * @throws com.atlassian.plugin.PluginParseException If there are exceptions parsing the plugin configuration
     */
    public String canCreate(final PluginArtifact pluginArtifact) throws PluginParseException {
        checkNotNull(pluginArtifact, "The plugin artifact is required");
        final boolean isPlugin = hasDescriptor(checkNotNull(pluginArtifact));
        final boolean hasSpring = pluginArtifact.containsSpringContext();
        final boolean isTransformless = getPluginKeyFromManifest(checkNotNull(pluginArtifact)) != null;
        final Manifest mf = getManifest(pluginArtifact);

        String key = null;
        if (!isTransformless && !isPlugin && mf != null) {
            final Attributes attrs = mf.getMainAttributes();

            // Only a proper OSGi bundle could be loaded, otherwise it is a simple JAR which should be
            // loaded as P1 (xml classpath) plugin
            if (attrs.containsKey(new Attributes.Name(Constants.BUNDLE_SYMBOLICNAME))) {
                key = getPluginKey(mf);
            }
        }

        if (key == null && (isTransformless && !hasSpring)) {
            key = isPlugin ? getPluginKeyFromDescriptor(checkNotNull(pluginArtifact)) : getPluginKeyFromManifest(pluginArtifact);
        }

        return key;
    }

    /**
     * Create a plugin from the given artifact.
     *
     * @param pluginArtifact          the plugin artifact containing the plugin.
     * @param moduleDescriptorFactory The factory for plugin modules.
     * @return The instantiated and populated plugin, or an {@link UnloadablePlugin} if the plugin cannot be loaded.
     * @since 2.2.0
     */
    public Plugin create(final PluginArtifact pluginArtifact, final ModuleDescriptorFactory moduleDescriptorFactory) {
        checkNotNull(pluginArtifact, "The plugin artifact is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        // Check if plugin could be created and fail fast if not
        final String pluginKey = canCreate(pluginArtifact);
        if (null == pluginKey) {
            log.warn("Unable to load plugin from '{}'", pluginArtifact);
            return new UnloadablePlugin("PluginArtifact has no manifest or is not a bundle: '" + pluginArtifact + "'");
        }

        // If plugin could be loaded then it is:
        // 1. pure OSGi bundle, no atlassian descriptor, no module factory needs
        // 2. atlassian plugin - needs for a module descriptor
        Plugin plugin = null;
        try (InputStream pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName)) {
            plugin = new OsgiBundlePlugin(osgiContainerManager, pluginKey, pluginArtifact);
            if (pluginDescriptor != null) {
                // Plugin specific module factory to allow plugin internal resources usage
                ModuleDescriptorFactory combinedFactory = osgiChainedModuleDescriptorFactoryCreator.create(new OsgiChainedModuleDescriptorFactoryCreator.ResourceLocator() {
                    public boolean doesResourceExist(String name) {
                        return pluginArtifact.doesResourceExist(name);
                    }
                }, moduleDescriptorFactory);


                // Parse descriptor & configure plugin
                DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, applications);
                plugin = parser.configurePlugin(combinedFactory, plugin);
            }
        } catch (IOException ex) {
            log.error("Unable to load plugin: " + pluginArtifact.toFile(), ex);
            plugin = new UnloadablePlugin("Unable to load plugin: " + ex.getMessage());
        }

        return plugin;
    }

    @Override
    public ModuleDescriptor<?> createModule(final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugin instanceof OsgiBundlePlugin) {
            final ModuleDescriptorFactory combinedFactory = osgiChainedModuleDescriptorFactoryCreator.create(new OsgiChainedModuleDescriptorFactoryCreator.ResourceLocator() {
                @Override
                public boolean doesResourceExist(final String name) {
                    // This returns true to indicate that the listable module descriptor class is present in this plugin.
                    // Those module descriptors are skipped when first installing the plugin, however no need for us to skip
                    // here, as the plugin has been loaded/installed.
                    return false;
                }
            }, moduleDescriptorFactory);

            return addModule(combinedFactory, plugin, module);
        } else {
            return null;
        }
    }

    private String getPluginKeyFromManifest(final PluginArtifact pluginArtifact) {
        final Manifest mf = getManifest(pluginArtifact);
        if (mf != null) {
            final String key = mf.getMainAttributes().getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);

            final String version = mf.getMainAttributes().getValue(Constants.BUNDLE_VERSION);
            if (key != null) {
                if (version != null) {
                    return key;
                } else {
                    log.warn("Found plugin key '" + key + "' in the manifest but no bundle version, so it can't be loaded as an OsgiPlugin");
                }
            }
        }
        return null;
    }
}
