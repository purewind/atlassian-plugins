package com.atlassian.plugin.osgi.external;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * A single module descriptor factory for plugins to use when they want to expose just one plugin.  Uses
 * {@link HostContainer} to optionally provide autowiring for new descriptor instances.
 *
 * @since 2.1
 */
public class SingleModuleDescriptorFactory<T extends ModuleDescriptor> implements ListableModuleDescriptorFactory {
    private final String type;
    private final Class<T> moduleDescriptorClass;
    private final HostContainer hostContainer;

    /**
     * Constructs an instance using a specific host container
     *
     * @param hostContainer         The host container to use to create descriptor instances
     * @param type                  The type of module
     * @param moduleDescriptorClass The descriptor class
     * @since 2.2.0
     */
    public SingleModuleDescriptorFactory(final HostContainer hostContainer, final String type, final Class<T> moduleDescriptorClass) {
        this.moduleDescriptorClass = moduleDescriptorClass;
        this.type = type;
        this.hostContainer = hostContainer;
    }

    @Override
    public ModuleDescriptor getModuleDescriptor(final String type) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        T result = null;
        if (this.type.equals(type)) {
            result = hostContainer.create(moduleDescriptorClass);
        }
        return result;
    }

    @Override
    public boolean hasModuleDescriptor(final String type) {
        return (this.type.equals(type));
    }

    @Override
    public Class<? extends ModuleDescriptor> getModuleDescriptorClass(final String type) {
        return (this.type.equals(type) ? moduleDescriptorClass : null);
    }

    /**
     * @since 3.0.0
     */
    @Override
    public Iterable<String> getModuleDescriptorKeys() {
        return ImmutableSet.of(type);
    }

    @Override
    public Set<Class<? extends ModuleDescriptor>> getModuleDescriptorClasses() {
        return ImmutableSet.<Class<? extends ModuleDescriptor>>of(moduleDescriptorClass);
    }

    public HostContainer getHostContainer() {
        return hostContainer;
    }
}
