package com.atlassian.plugin.osgi.factory.transform.model;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.collect.Maps.transformValues;

/**
 * Encapsulates the package exports from the system bundle
 *
 * @since 2.2.0
 */
public class SystemExports {
    private final Map<String, Map<String, String>> exports;

    public static final SystemExports NONE = new SystemExports("");

    /**
     * Constructs an instance by parsing the exports line from the manifest
     *
     * @param exportsLine The Export-Package header value
     */
    public SystemExports(String exportsLine) {
        if (exportsLine == null) {
            exportsLine = "";
        }

        this.exports = internAttributeKeys(OsgiHeaderUtil.parseHeader(exportsLine));
    }

    /**
     * OSGi manifest header keys come from a very small set of possible strings, and are ripe for interning. The other
     * strings in this map are less suitable for interning, and so are left alone.
     */
    private static Map<String, Map<String, String>> internAttributeKeys(final Map<String, Map<String, String>> map) {
        return transformValues(map, new Function<Map<String, String>, Map<String, String>>() {
            @Override
            public Map<String, String> apply(final Map<String, String> innerMap) {
                return internKeys(innerMap);
            }
        });
    }

    private static Map<String, String> internKeys(final Map<String, String> innerMap) {
        // Guava doesn't offer a "transformKeys" method, so we do it the old-fashioned way
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (Map.Entry<String, String> entry : innerMap.entrySet()) {
            builder.put(
                    entry.getKey().intern(),  // this here's the manifest header key
                    entry.getValue()
            );
        }

        return builder.build();
    }

    /**
     * Constructs a package export, taking into account any attributes on the system export, including the version.
     * The version is handled special, in that is added as an exact match, i.e. [1.0,1.0].
     *
     * @param pkg The java package
     * @return The full export line to use for a host component import
     */
    public String getFullExport(String pkg) {
        if (exports.containsKey(pkg)) {
            Map<String, String> attrs = new LinkedHashMap<String, String>(exports.get(pkg));
            if (attrs.containsKey("version")) {
                final String version = attrs.get("version");
                attrs.put("version", "[" + version + "," + version + "]");
            }
            return OsgiHeaderUtil.buildHeader(pkg, attrs);
        }
        return pkg;
    }

    /**
     * @param pkg The package to check
     * @return True if the package is being exported, false otherwise
     */
    public boolean isExported(String pkg) {
        return exports.containsKey(pkg);
    }
}
