package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerFailedEvent;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.event.PluginServiceDependencyWaitEndedEvent;
import com.atlassian.plugin.osgi.event.PluginServiceDependencyWaitStartingEvent;
import com.atlassian.plugin.osgi.event.PluginServiceDependencyWaitTimedOutEvent;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.annotations.VisibleForTesting;
import org.dom4j.Element;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.framework.namespace.PackageNamespace;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Plugin that wraps an OSGi bundle that does contain a plugin descriptor.  The actual bundle is not created until the
 * {@link #install()} method is invoked.  Any attempt to access a method that requires a bundle will throw an
 * {@link com.atlassian.plugin.IllegalPluginStateException}.
 *
 * This class uses a {@link OsgiPluginHelper} to represent different behaviors of key methods in different states.
 * {@link OsgiPluginUninstalledHelper} implements the methods when the plugin hasn't yet been installed into the
 * OSGi container, while {@link OsgiPluginInstalledHelper} implements the methods when the bundle is available.  This
 * leaves this class to manage the {@link PluginState} and interactions with the event system.
 */
//@Threadsafe
public class OsgiPlugin extends AbstractPlugin implements OsgiBackedPlugin, ContainerManagedPlugin, Plugin.Resolvable {
    private static final Logger log = LoggerFactory.getLogger(OsgiPlugin.class);

    /**
     * Manifest key for the Atlassian plugin key entry
     */
    public static final String ATLASSIAN_PLUGIN_KEY = "Atlassian-Plugin-Key";

    /**
     * Manifest key for additional scan folders
     */
    public static final String ATLASSIAN_SCAN_FOLDERS = "Atlassian-Scan-Folders";

    /**
     * Manifest key denoting Atlassian remote plugins
     */
    public static final String REMOTE_PLUGIN_KEY = "Remote-Plugin";

    private final Map<String, Element> moduleElements = new HashMap<>();
    private final PluginEventManager pluginEventManager;
    private final PackageAdmin packageAdmin;
    private final Set<OutstandingDependency> outstandingDependencies = new CopyOnWriteArraySet<>();
    private final BundleListener bundleStartStopListener;

    private volatile boolean treatPluginContainerCreationAsRefresh = false;
    private volatile OsgiPluginHelper helper;

    // Until the framework is actually done starting we want to ignore @RequiresRestart. Where this comes into play
    // is when we have one version of a plugin (e.g. via bundled-plugins.zip) installed but then discover a newer
    // one in installed-plugins. Clearly we can't "require a restart" between those two stages. And since nothing has
    // been published outside of plugins yet (and thus can't be cached by the host app) the @RequiresRestart is
    // meaningless.
    private volatile boolean frameworkStarted = false;

    public OsgiPlugin(final String key, final OsgiContainerManager mgr, final PluginArtifact artifact, final PluginArtifact originalPluginArtifact, final PluginEventManager pluginEventManager) {
        super(checkNotNull(originalPluginArtifact));
        this.pluginEventManager = checkNotNull(pluginEventManager);

        this.helper = new OsgiPluginUninstalledHelper(
                checkNotNull(key, "The plugin key is required"),
                checkNotNull(mgr, "The osgi container is required"),
                checkNotNull(artifact, "The plugin artifact is required"));
        this.packageAdmin = extractPackageAdminFromOsgi(mgr);

        this.bundleStartStopListener = new SynchronousBundleListener() {
            public void bundleChanged(final BundleEvent bundleEvent) {
                if (bundleEvent.getBundle() == getBundle()) {
                    if (bundleEvent.getType() == BundleEvent.STOPPING) {
                        helper.onDisable();
                        setPluginState(PluginState.DISABLED);
                    } else if (bundleEvent.getType() == BundleEvent.STARTED) {
                        final BundleContext ctx = getBundle().getBundleContext();
                        helper.onEnable(createServiceTrackers(ctx));
                        setPluginState(PluginState.ENABLED);
                    }
                }
            }
        };
    }

    /**
     * Only used for testing.
     *
     * @param pluginEventManager The PluginEventManager to use.
     * @param helper             The OsgiPluginHelper to use.
     * @param packageAdmin       the PackageAdmin to use.
     */
    @VisibleForTesting
    OsgiPlugin(final PluginEventManager pluginEventManager, final OsgiPluginHelper helper, final PackageAdmin packageAdmin) {
        super(null);
        this.helper = helper;
        this.pluginEventManager = pluginEventManager;
        this.packageAdmin = packageAdmin;
        this.bundleStartStopListener = null;
    }

    @Override
    public Bundle getBundle() throws IllegalPluginStateException {
        return helper.getBundle();
    }

    @Override
    public InstallationMode getInstallationMode() {
        return helper.isRemotePlugin() ? InstallationMode.REMOTE : InstallationMode.LOCAL;
    }

    /**
     * @return true
     */
    public boolean isUninstallable() {
        return true;
    }

    /**
     * @return true
     */
    public boolean isDynamicallyLoaded() {
        return true;
    }

    /**
     * @return true
     */
    public boolean isDeleteable() {
        return true;
    }


    @Override
    public Date getDateInstalled() {
        long date = getPluginArtifact().toFile().lastModified();
        if (date == 0) {
            date = getDateLoaded().getTime();
        }
        return new Date(date);
    }

    /**
     * @param clazz        The name of the class to be loaded
     * @param callingClass The class calling the loading (used to help find a classloader)
     * @param <T>          The class type
     * @return The class instance, loaded from the OSGi bundle
     * @throws ClassNotFoundException      If the class cannot be found
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException, IllegalPluginStateException {
        return helper.loadClass(clazz, callingClass);
    }

    /**
     * @param name The resource name
     * @return The resource URL, null if not found
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    public URL getResource(final String name) throws IllegalPluginStateException {
        return helper.getResource(name);
    }

    /**
     * @param name The name of the resource to be loaded.
     * @return Null if not found
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    public InputStream getResourceAsStream(final String name) throws IllegalPluginStateException {
        return helper.getResourceAsStream(name);
    }

    /**
     * @return The classloader to load classes and resources from the bundle
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    public ClassLoader getClassLoader() throws IllegalPluginStateException {
        return helper.getClassLoader();
    }

    /**
     * Called when the plugin container for the bundle has failed to be created.  This means the bundle is still
     * active, but the plugin container is not available, so for our purposes, the plugin shouldn't be enabled.
     *
     * @param event The plugin container failed event
     * @throws com.atlassian.plugin.IllegalPluginStateException If the plugin key hasn't been set yet
     */
    @PluginEventListener
    public void onPluginContainerFailed(final PluginContainerFailedEvent event) throws IllegalPluginStateException {
        if (getKey() == null) {
            throw new IllegalPluginStateException("Plugin key must be set");
        }
        if (getKey().equals(event.getPluginKey())) {
            logAndClearOustandingDependencies();
            // TODO: do something with the exception more than logging
            log.error("Unable to start the plugin container for plugin '{}'", getKey(), event.getCause());
            setPluginState(PluginState.DISABLED);
        }
    }

    @PluginEventListener
    public void onPluginFrameworkStartedEvent(final PluginFrameworkStartedEvent event) {
        frameworkStarted = true;
    }

    @PluginEventListener
    public void onPluginFrameworkShutdownEvent(final PluginFrameworkShutdownEvent event) {
        frameworkStarted = false;
    }

    @PluginEventListener
    public void onServiceDependencyWaitStarting(final PluginServiceDependencyWaitStartingEvent event) {
        if (event.getPluginKey() != null && event.getPluginKey().equals(getKey())) {
            final OutstandingDependency dep = new OutstandingDependency(event.getBeanName(), String.valueOf(event.getFilter()));
            outstandingDependencies.add(dep);
            log.info("Plugin '{}' waiting for {}", getKey(), dep);
        }
    }

    @PluginEventListener
    public void onServiceDependencyWaitEnded(final PluginServiceDependencyWaitEndedEvent event) {
        if (event.getPluginKey() != null && event.getPluginKey().equals(getKey())) {
            final OutstandingDependency dep = new OutstandingDependency(event.getBeanName(), String.valueOf(event.getFilter()));
            outstandingDependencies.remove(dep);
            log.info("Plugin '{}' found {}", getKey(), dep);
        }
    }

    @PluginEventListener
    public void onServiceDependencyWaitEnded(final PluginServiceDependencyWaitTimedOutEvent event) {
        if (event.getPluginKey() != null && event.getPluginKey().equals(getKey())) {
            final OutstandingDependency dep = new OutstandingDependency(event.getBeanName(), String.valueOf(event.getFilter()));
            outstandingDependencies.remove(dep);
            log.error("Plugin '{}' timeout waiting for {}", getKey(), dep);
        }
    }

    /**
     * Called when the plugin container for the bundle has been created or refreshed.  If this is the first time the
     * context has been refreshed, then it is a new context.  Otherwise, this means that the bundle has been reloaded,
     * usually due to a dependency upgrade.
     *
     * @param event The event
     * @throws com.atlassian.plugin.IllegalPluginStateException If the plugin key hasn't been set yet
     */
    @PluginEventListener
    public void onPluginContainerRefresh(final PluginContainerRefreshedEvent event) throws IllegalPluginStateException {
        if (getKey() == null) {
            throw new IllegalPluginStateException("Plugin key must be set");
        }
        if (getKey().equals(event.getPluginKey())) {
            outstandingDependencies.clear();
            helper.setPluginContainer(event.getContainer());
            if (!compareAndSetPluginState(PluginState.ENABLING, PluginState.ENABLED) && getPluginState() != PluginState.ENABLED) {
                log.warn("Ignoring the bean container that was just created for plugin " + getKey() + ".  The plugin " +
                        "is in an invalid state, " + getPluginState() + ", that doesn't support a transition to " +
                        "enabled.  Most likely, it was disabled due to a timeout.");
                helper.setPluginContainer(null);
                return;
            }

            // Only send refresh event on second creation
            if (treatPluginContainerCreationAsRefresh) {
                pluginEventManager.broadcast(new PluginRefreshedEvent(this));
            } else {
                treatPluginContainerCreationAsRefresh = true;
            }
        }
    }

    /**
     * Determines which plugin keys are dependencies based on tracing the wires, categorising them as mandatory,
     * optional or dynamic.
     *
     * {@link PackageNamespace#RESOLUTION_OPTIONAL} and {@link PackageNamespace#RESOLUTION_DYNAMIC} are mapped to
     * optional and dynamic, respectively. Any others are mapped to mandatory.
     *
     * @return not null, possibly empty
     * @see Plugin#getDependencies()
     * @since 4.0
     */
    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return helper.getDependencies();
    }

    @Override
    public String toString() {
        return getKey();
    }

    /**
     * Installs the plugin artifact into OSGi
     *
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    @Override
    protected void installInternal() throws IllegalPluginStateException {
        log.debug("Installing OSGi plugin '{}'", getKey());
        final Bundle bundle = helper.install();
        helper = new OsgiPluginInstalledHelper(bundle, packageAdmin);
    }

    /**
     * Enables the plugin by setting the OSGi bundle state to enabled.
     *
     * @return {@link PluginState#ENABLED}if the container is being refreshed or {@link PluginState#ENABLING} if we are waiting
     * on a plugin container (first time being activated, as all subsequent times are considered refreshes)
     * @throws OsgiContainerException      If the underlying OSGi system threw an exception or we tried to enable the bundle
     *                                     when it was in an invalid state
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    @Override
    protected synchronized PluginState enableInternal() throws OsgiContainerException, IllegalPluginStateException {
        log.debug("Enabling OSGi plugin '{}'", getKey());
        try {
            final PluginState stateResult;
            if (getBundle().getState() == Bundle.ACTIVE) {
                log.debug("Plugin '{}' bundle is already active, not doing anything", getKey());
                stateResult = PluginState.ENABLED;
            } else if ((getBundle().getState() == Bundle.RESOLVED) || (getBundle().getState() == Bundle.INSTALLED)) {
                pluginEventManager.register(this);
                if (!treatPluginContainerCreationAsRefresh) {
                    // Move to ENABLING before starting the bundle which triggers the plugin container refresh event
                    setPluginState(PluginState.ENABLING);
                    // Since we've taken over management of state change, tell our caller not to bother
                    stateResult = PluginState.PENDING;
                } else {
                    stateResult = PluginState.ENABLED;
                }
                log.debug("Plugin '{}' bundle is resolved or installed, starting.", getKey());
                getBundle().start();
                final BundleContext ctx = getBundle().getBundleContext();
                helper.onEnable(createServiceTrackers(ctx));

                // ensure the bean factory is removed when the bundle is stopped
                ctx.addBundleListener(bundleStartStopListener);
            } else {
                throw new OsgiContainerException("Cannot enable the plugin '" + getKey() + "' when the bundle is not in the resolved or installed state: "
                        + getBundle().getState() + "(" + getBundle().getBundleId() + ")");
            }

            // Return our desired state, which can be PENDING if the container refresh will complete the enable
            return stateResult;
        } catch (final BundleException e) {
            log.error("Detected an error (BundleException) enabling the plugin '" + getKey() + "' : " + e.getMessage() + ". " +
                    " This error usually occurs when your plugin imports a package from another bundle with a specific version constraint " +
                    "and either the bundle providing that package doesn't meet those version constraints, or there is no bundle " +
                    "available that provides the specified package. For more details on how to fix this, see " +
                    "https://developer.atlassian.com/x/mQAN");
            throw new OsgiContainerException("Cannot start plugin: " + getKey(), e);
        }
    }

    private ServiceTracker[] createServiceTrackers(final BundleContext ctx) {
        return new ServiceTracker[]{
                new ServiceTracker(ctx, ModuleDescriptor.class.getName(),
                        new ModuleDescriptorServiceTrackerCustomizer(this, pluginEventManager)),
                new ServiceTracker(ctx, ListableModuleDescriptorFactory.class.getName(),
                        new UnrecognizedModuleDescriptorServiceTrackerCustomizer(this, pluginEventManager))
        };
    }

    /**
     * Disables the plugin by changing the bundle state back to resolved
     *
     * @throws OsgiContainerException      If the OSGi system threw an exception
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    @Override
    protected synchronized void disableInternal() throws OsgiContainerException, IllegalPluginStateException {
        // Only disable underlying bundle if this is a truly dynamic plugin
        if (!requiresRestart()) {
            try {
                if (getPluginState() == PluginState.DISABLING) {
                    logAndClearOustandingDependencies();
                }
                helper.onDisable();
                pluginEventManager.unregister(this);
                getBundle().stop();
                treatPluginContainerCreationAsRefresh = false;
            } catch (final BundleException e) {
                log.error("Detected an error (BundleException) disabling the plugin '" + getKey() + "' : " + e.getMessage() + ".");
                throw new OsgiContainerException("Cannot stop plugin: " + getKey(), e);
            }
        }
    }

    private boolean requiresRestart() {
        return frameworkStarted && PluginUtils.doesPluginRequireRestart(this);
    }

    private void logAndClearOustandingDependencies() {
        for (final OutstandingDependency dep : outstandingDependencies) {
            log.error("Plugin '{}' never resolved {}", getKey(), dep);
        }
        outstandingDependencies.clear();
    }

    /**
     * Uninstalls the bundle from the OSGi container
     *
     * @throws OsgiContainerException      If the underlying OSGi system threw an exception
     * @throws IllegalPluginStateException if the bundle hasn't been created yet
     */
    @Override
    protected void uninstallInternal() throws OsgiContainerException, IllegalPluginStateException {
        final String key = getKey();

        int retryCount = 0;
        Exception rootCause = null;
        final long sleepTime = 500L;

        while (true) {
            try {
                // We unregister irrespective of whether the plugin is installed. Since it's idempotent it's safe,
                // and if we're trying to uninstall uninstalled things, then stopping more events won't hurt.
                pluginEventManager.unregister(this);
                final Bundle bundle = getBundleIfInstalled();
                if (null != bundle) {
                    // We need this state to make the new helper below, but it uses bundle headers, so we want to do it
                    // prior to uninstallation to avoid possible issues with felix (see PLUGDEV-215 / PIPE-836).
                    final boolean remotePlugin = helper.isRemotePlugin();
                    // This code is being updated not just for better memory performance, but also in response to improving
                    // diagnostics around some plugin update issues (see PLUG-1084), so it is very defensive.
                    if (bundle.getState() != Bundle.UNINSTALLED) {
                        bundle.uninstall();
                    } else {
                        log.warn("Bundle for '{}' already UNINSTALLED, but still held by helper '{}'", key, helper);
                    }
                    // We want to fire the helper event late as we're being defensive
                    final OsgiPluginHelper oldHelper = helper;
                    helper = new OsgiPluginDeinstalledHelper(key, remotePlugin);
                    setPluginState(PluginState.UNINSTALLED);
                    oldHelper.onUninstall();
                } else {
                    log.debug("Trying to uninstall '{}', but it is not installed (helper '{}')", key, helper);
                }
                break;

            } catch (final BundleException e) {
                rootCause = retryCount == 0 ? e : rootCause;
                retryCount++;

                if (retryCount < 3) {
                    log.debug("Possible transient fail on try {} to uninstall '{}', retrying in {} mSecs",
                            new Object[]{retryCount, key, sleepTime});
                    log.debug(e.getMessage(), e);
                    try {
                        Thread.sleep(sleepTime);
                    } catch (final InterruptedException ei) {
                        throw new OsgiContainerException(
                                "Cannot uninstall '" + key + "', retry sleep was interrupted: " + ei.getMessage());
                    }
                } else {
                    log.error("Detected an error (BundleException) disabling the plugin '{}'.", key);
                    log.error(rootCause.getMessage(), rootCause);
                    throw new OsgiContainerException("Cannot uninstall '" + key + "'");
                }
            }
        }
    }

    /**
     * Obtain the bundle for this plugin if it is installed.
     * <p/>
     * This method really belongs on {@link OsgiPluginHelper}, but i don't want to change it's interface. It is intended to
     * be called in a context where we expect this OsgiPlugin to be installed.
     *
     * @return the bundle for this plugin, or null if the plugin is not installed.
     */
    private Bundle getBundleIfInstalled() {
        // This implementation is ugly on account of this really belonging elsewhere. Since the intended usage is to be
        // called only when this OsgiPlugin is installed, we don't expect to take the exception path.
        try {
            return getBundle();
        } catch (final IllegalPluginStateException eips) {
            return null;
        }
    }

    /**
     * Adds a module descriptor XML element for later processing, needed for dynamic module support
     *
     * @param key     The module key
     * @param element The module element
     */
    void addModuleDescriptorElement(final String key, final Element element) {
        moduleElements.put(key, element);
    }

    /**
     * Exposes {@link #removeModuleDescriptor(String)} for package-protected classes
     *
     * @param key The module descriptor key
     */
    void clearModuleDescriptor(final String key) {
        removeModuleDescriptor(key);
    }

    /**
     * Gets the module elements for dynamic module descriptor handling.  Doesn't need to return a copy or anything
     * immutable because it is only accessed by package-private helper classes
     *
     * @return The map of module keys to module XML elements
     */
    Map<String, Element> getModuleElements() {
        return moduleElements;
    }

    /**
     * Extracts the {@link PackageAdmin} instance from the OSGi container
     *
     * @param mgr The OSGi container manager
     * @return The package admin instance, should never be null
     */
    private PackageAdmin extractPackageAdminFromOsgi(final OsgiContainerManager mgr) {
        // Get the system bundle (always bundle 0)
        final Bundle bundle = mgr.getBundles()[0];

        // We assume the package admin will always be available
        final ServiceReference ref = bundle.getBundleContext()
                .getServiceReference(PackageAdmin.class.getName());
        return (PackageAdmin) bundle.getBundleContext()
                .getService(ref);
    }

    public ContainerAccessor getContainerAccessor() {
        return helper.getContainerAccessor();
    }

    @Override
    public void resolve() {
        // It would be nicer to do this more globally, but i can't see a convenient place to hook into the OSGi interface
        // layer in DefaultPluginManager or similar. We just resolve unconditionally for simplicity. In fact Felix grabs
        // a lock internally before it checks the state to find there's no work, but a little artisnal data suggests the
        // effect is neglible on jira statup (25 ms).
        packageAdmin.resolveBundles(new Bundle[]{getBundle()});
    }

    private static class OutstandingDependency {
        private final String beanName;
        private final String filter;

        public OutstandingDependency(final String beanName, final String filter) {
            this.beanName = beanName;
            this.filter = filter;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final OutstandingDependency that = (OutstandingDependency) o;

            if (beanName != null ? !beanName.equals(that.beanName) : that.beanName != null) {
                return false;
            }
            if (!filter.equals(that.filter)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = beanName != null ? beanName.hashCode() : 0;
            result = 31 * result + filter.hashCode();
            return result;
        }

        public String toString() {
            return "service '" + beanName + "' with filter '" + filter + "'";
        }
    }
}
